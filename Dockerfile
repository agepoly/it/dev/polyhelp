FROM debian:buster
MAINTAINER informatique@agepoly.ch
ARG version=0.2
ARG project_name="polyhelp"

RUN echo "locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8" | debconf-set-selections

RUN apt-get update
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8

ENV DEBIAN_FRONTEND noninteractive
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US
ENV LC_ALL en_US.UTF-8

RUN apt-get update
RUN apt-get install -y \
    apache2 \ 
    git \
    libpq-dev \
    libapache2-mod-php \
    php \
    php-pdo-mysql \
    php-mysql \
    php-mysqli \
    php-pdo-pgsql \
    php-curl \
    s6

## Clone project

ARG git_branch="master"
ARG gitlab_token_name="gitlab+deploy-token-131069"
ARG gitlab_token_password="iwjzjenrFHyKJSmC9SS7"
ARG git_repo_path="agepoly/it/dev/polyhelp"
ARG git_temp_path="/tmp/polyhelp"
ARG pip_reqs=""
ARG www_root_path="/var/www/html"

RUN git clone  --progress --verbose --single-branch  --branch $git_branch https://$gitlab_token_name:$gitlab_token_password@gitlab.com/$git_repo_path $git_temp_path

WORKDIR $git_temp_path

#RUN mkdir $www_root_path && 
RUN cp -r $git_temp_path/polyhelp-code/* -t $www_root_path/.

RUN find  "$www_root_path"
RUN ls -l "$git_temp_path"

WORKDIR "$www_root_path"

RUN rm -fr $git_temp_path

#COPY polyhelp-code/ /var/www/html
#RUN chown -R www-data: /var/www/

########

RUN a2dissite 000-default 
COPY polyhelp.apache /etc/apache2/sites-available/$project_name.conf
RUN a2ensite $project_name

RUN a2enmod rewrite

VOLUME [/var/www/polyhelp/files,/var/www/polyhelp/support,/var/www/polyhelp/support_TODO]

COPY ./s6 /lib/s6

EXPOSE 80

CMD "/lib/s6/init"

