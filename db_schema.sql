--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: course_prof; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE course_prof (
    course_id integer NOT NULL,
    user_id integer NOT NULL,
    type smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.course_prof OWNER TO postgres;

--
-- Name: courses; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE courses (
    id integer NOT NULL,
    name character varying(128),
    postgroup_id integer NOT NULL,
    isa_key character varying(32) NOT NULL,
    isa_id integer NOT NULL
);


ALTER TABLE public.courses OWNER TO postgres;

--
-- Name: courses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE courses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courses_id_seq OWNER TO postgres;

--
-- Name: courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE courses_id_seq OWNED BY courses.id;


--
-- Name: courses_sections; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE courses_sections (
    course_id integer NOT NULL,
    section_id integer NOT NULL,
    degree smallint NOT NULL,
    semester boolean NOT NULL,
    mandatory boolean
);


ALTER TABLE public.courses_sections OWNER TO postgres;

--
-- Name: faculties; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE faculties (
    id integer NOT NULL,
    name_en character varying(64),
    name_fr character varying(64),
    short character varying(4)
);


ALTER TABLE public.faculties OWNER TO postgres;

--
-- Name: faculties_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE faculties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.faculties_id_seq OWNER TO postgres;

--
-- Name: faculties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE faculties_id_seq OWNED BY faculties.id;


--
-- Name: filegroup_weeks; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filegroup_weeks (
    filegroup_id integer NOT NULL,
    week integer
);


ALTER TABLE public.filegroup_weeks OWNER TO postgres;

--
-- Name: filegroups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filegroups (
    id integer NOT NULL,
    course_id integer NOT NULL,
    name character varying(96),
    category smallint,
    "time" timestamp without time zone DEFAULT now(),
    post_id integer NOT NULL,
    author_id integer NOT NULL,
    reviewed boolean DEFAULT false NOT NULL,
    uploader_id integer NOT NULL,
    section_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.filegroups OWNER TO postgres;

--
-- Name: filegroups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE filegroups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filegroups_id_seq OWNER TO postgres;

--
-- Name: filegroups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE filegroups_id_seq OWNED BY filegroups.id;


--
-- Name: filerates; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE filerates (
    file_id integer NOT NULL,
    user_id integer NOT NULL,
    rate smallint
);


ALTER TABLE public.filerates OWNER TO postgres;

--
-- Name: files; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE files (
    id integer NOT NULL,
    filegroup_id integer NOT NULL,
    subtype integer NOT NULL,
    extension character varying(4),
    filepath character varying(96),
    uploader_id integer NOT NULL,
    rating_base integer DEFAULT 0,
    creation_time timestamp without time zone,
    upload_time timestamp without time zone DEFAULT now(),
    description text,
    md5 character(32) DEFAULT NULL::character(1),
    rating_cache integer DEFAULT 0,
    origin smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.files OWNER TO postgres;

--
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.files_id_seq OWNER TO postgres;

--
-- Name: files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE files_id_seq OWNED BY files.id;


--
-- Name: postgroups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE postgroups (
    id integer NOT NULL,
    name character varying(128),
    reltype smallint DEFAULT 0,
    relid integer
);


ALTER TABLE public.postgroups OWNER TO postgres;

--
-- Name: postgroups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE postgroups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.postgroups_id_seq OWNER TO postgres;

--
-- Name: postgroups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE postgroups_id_seq OWNED BY postgroups.id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE posts (
    id integer NOT NULL,
    user_id integer NOT NULL,
    postgroup_id integer NOT NULL,
    post_id integer,
    title character varying(96) NOT NULL,
    content text,
    creation_time timestamp without time zone DEFAULT now(),
    file_related boolean DEFAULT false NOT NULL
);


ALTER TABLE public.posts OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE posts_id_seq OWNED BY posts.id;


--
-- Name: problems; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE problems (
    id integer NOT NULL,
    user_id integer NOT NULL,
    reltype smallint,
    relid integer NOT NULL,
    relcategory smallint,
    description text,
    "time" timestamp without time zone DEFAULT now()
);


ALTER TABLE public.problems OWNER TO postgres;

--
-- Name: problems_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE problems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.problems_id_seq OWNER TO postgres;

--
-- Name: problems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE problems_id_seq OWNED BY problems.id;


--
-- Name: sections; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sections (
    id integer NOT NULL,
    faculty_id integer NOT NULL,
    name_en character varying(96),
    name_fr character varying(96),
    short character varying(5),
    ldap_short character varying(5) NOT NULL,
    isa_id integer NOT NULL
);


ALTER TABLE public.sections OWNER TO postgres;

--
-- Name: sections_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sections_id_seq OWNER TO postgres;

--
-- Name: sections_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sections_id_seq OWNED BY sections.id;


--
-- Name: telemetry_data; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE telemetry_data (
    id bigint NOT NULL,
    event_id bigint NOT NULL,
    attr character varying(255) NOT NULL,
    val text
);


ALTER TABLE public.telemetry_data OWNER TO postgres;

--
-- Name: telemetry_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE telemetry_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.telemetry_data_id_seq OWNER TO postgres;

--
-- Name: telemetry_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE telemetry_data_id_seq OWNED BY telemetry_data.id;


--
-- Name: telemetry_events; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE telemetry_events (
    id bigint NOT NULL,
    session_id bigint NOT NULL,
    "time" timestamp without time zone DEFAULT now(),
    type character varying(255)
);


ALTER TABLE public.telemetry_events OWNER TO postgres;

--
-- Name: telemetry_events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE telemetry_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.telemetry_events_id_seq OWNER TO postgres;

--
-- Name: telemetry_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE telemetry_events_id_seq OWNED BY telemetry_events.id;


--
-- Name: telemetry_sessions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE telemetry_sessions (
    id bigint NOT NULL,
    cookie character varying(32),
    unit character varying(32),
    browser character varying(64),
    platform character varying(64),
    device character varying(32)
);


ALTER TABLE public.telemetry_sessions OWNER TO postgres;

--
-- Name: telemetry_sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE telemetry_sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.telemetry_sessions_id_seq OWNER TO postgres;

--
-- Name: telemetry_sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE telemetry_sessions_id_seq OWNED BY telemetry_sessions.id;


--
-- Name: user_addresses; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_addresses (
    id integer NOT NULL,
    user_id integer NOT NULL,
    name character varying(64),
    value character varying(64),
    description text
);


ALTER TABLE public.user_addresses OWNER TO postgres;

--
-- Name: user_addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_addresses_id_seq OWNER TO postgres;

--
-- Name: user_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_addresses_id_seq OWNED BY user_addresses.id;


--
-- Name: user_favcourses; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_favcourses (
    user_id integer NOT NULL,
    course_id integer NOT NULL,
    priority smallint
);


ALTER TABLE public.user_favcourses OWNER TO postgres;

--
-- Name: user_rights; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_rights (
    id integer NOT NULL,
    user_id integer NOT NULL,
    reltype smallint,
    relid integer
);


ALTER TABLE public.user_rights OWNER TO postgres;

--
-- Name: user_rights_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_rights_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_rights_id_seq OWNER TO postgres;

--
-- Name: user_rights_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_rights_id_seq OWNED BY user_rights.id;


--
-- Name: user_sessions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_sessions (
    id character(64) NOT NULL,
    user_id integer NOT NULL,
    "time" timestamp without time zone DEFAULT now(),
    expiration timestamp without time zone DEFAULT (now() + '7 days'::interval)
);


ALTER TABLE public.user_sessions OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    section_id integer DEFAULT 0 NOT NULL,
    faculty_id integer DEFAULT 0 NOT NULL,
    degree smallint DEFAULT 0,
    avatar character varying(96),
    first_name character varying(64),
    last_name character varying(64),
    creation_time timestamp without time zone DEFAULT now(),
    professor boolean DEFAULT false,
    superadmin boolean DEFAULT false,
    score integer DEFAULT 0 NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    connected timestamp without time zone DEFAULT to_timestamp((0)::double precision) NOT NULL,
    sciper integer,
    gaspar character varying(64) DEFAULT NULL::character varying,
    email character varying(64),
    beta boolean DEFAULT false NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY courses ALTER COLUMN id SET DEFAULT nextval('courses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY faculties ALTER COLUMN id SET DEFAULT nextval('faculties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filegroups ALTER COLUMN id SET DEFAULT nextval('filegroups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY files ALTER COLUMN id SET DEFAULT nextval('files_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY postgroups ALTER COLUMN id SET DEFAULT nextval('postgroups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY problems ALTER COLUMN id SET DEFAULT nextval('problems_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sections ALTER COLUMN id SET DEFAULT nextval('sections_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telemetry_data ALTER COLUMN id SET DEFAULT nextval('telemetry_data_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telemetry_events ALTER COLUMN id SET DEFAULT nextval('telemetry_events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telemetry_sessions ALTER COLUMN id SET DEFAULT nextval('telemetry_sessions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_addresses ALTER COLUMN id SET DEFAULT nextval('user_addresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_rights ALTER COLUMN id SET DEFAULT nextval('user_rights_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: course_prof_course_id_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY course_prof
    ADD CONSTRAINT course_prof_course_id_user_id_key UNIQUE (course_id, user_id);


--
-- Name: courses_isa_key_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY courses
    ADD CONSTRAINT courses_isa_key_key UNIQUE (isa_key);


--
-- Name: courses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (id);


--
-- Name: courses_sections_course_id_section_id_degree_semester_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY courses_sections
    ADD CONSTRAINT courses_sections_course_id_section_id_degree_semester_key UNIQUE (course_id, section_id, degree, semester);


--
-- Name: faculties_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY faculties
    ADD CONSTRAINT faculties_pkey PRIMARY KEY (id);


--
-- Name: filegroup_weeks_filegroup_id_week_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filegroup_weeks
    ADD CONSTRAINT filegroup_weeks_filegroup_id_week_key UNIQUE (filegroup_id, week);


--
-- Name: filegroups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filegroups
    ADD CONSTRAINT filegroups_pkey PRIMARY KEY (id);


--
-- Name: filerates_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY filerates
    ADD CONSTRAINT filerates_unique UNIQUE (file_id, user_id);


--
-- Name: files_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_pkey PRIMARY KEY (id);


--
-- Name: postgroups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY postgroups
    ADD CONSTRAINT postgroups_pkey PRIMARY KEY (id);


--
-- Name: posts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: problems_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY problems
    ADD CONSTRAINT problems_pkey PRIMARY KEY (id);


--
-- Name: sections_isa_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sections
    ADD CONSTRAINT sections_isa_id_key UNIQUE (isa_id);


--
-- Name: sections_ldap_short_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sections
    ADD CONSTRAINT sections_ldap_short_key UNIQUE (ldap_short);


--
-- Name: sections_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sections
    ADD CONSTRAINT sections_pkey PRIMARY KEY (id);


--
-- Name: short_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sections
    ADD CONSTRAINT short_unique UNIQUE (short);


--
-- Name: telemetry_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY telemetry_data
    ADD CONSTRAINT telemetry_data_pkey PRIMARY KEY (id);


--
-- Name: telemetry_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY telemetry_events
    ADD CONSTRAINT telemetry_events_pkey PRIMARY KEY (id);


--
-- Name: telemetry_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY telemetry_sessions
    ADD CONSTRAINT telemetry_sessions_pkey PRIMARY KEY (id);


--
-- Name: user_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_addresses
    ADD CONSTRAINT user_addresses_pkey PRIMARY KEY (id);


--
-- Name: user_favcourses_unique; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_favcourses
    ADD CONSTRAINT user_favcourses_unique UNIQUE (user_id, course_id);


--
-- Name: user_rights_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_rights
    ADD CONSTRAINT user_rights_pkey PRIMARY KEY (id);


--
-- Name: user_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_sessions
    ADD CONSTRAINT user_sessions_pkey PRIMARY KEY (id);


--
-- Name: users_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users_gaspar_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_gaspar_key UNIQUE (gaspar);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_sciper_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_sciper_key UNIQUE (sciper);


--
-- Name: course_prof_course_id_user_id_type_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX course_prof_course_id_user_id_type_idx ON course_prof USING btree (course_id, user_id, type);


--
-- Name: courses_isa_key_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX courses_isa_key_idx ON courses USING btree (isa_key);


--
-- Name: courses_sections_course_id_section_id_degree_semester_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX courses_sections_course_id_section_id_degree_semester_idx ON courses_sections USING btree (course_id, section_id, degree, semester);


--
-- Name: filerates_file_id_user_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX filerates_file_id_user_id_idx ON filerates USING btree (file_id, user_id);


--
-- Name: telemetry_sessions_cookie_index; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX telemetry_sessions_cookie_index ON telemetry_sessions USING btree (cookie);


--
-- Name: user_favcourses_user_id_course_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_favcourses_user_id_course_id_idx ON user_favcourses USING btree (user_id, course_id);


--
-- Name: user_sessions_id_user_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX user_sessions_id_user_id_idx ON user_sessions USING btree (id, user_id);


--
-- Name: course_prof_course_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY course_prof
    ADD CONSTRAINT course_prof_course_id_fkey FOREIGN KEY (course_id) REFERENCES courses(id) DEFERRABLE;


--
-- Name: course_prof_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY course_prof
    ADD CONSTRAINT course_prof_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: courses_postgroup_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY courses
    ADD CONSTRAINT courses_postgroup_id_fkey FOREIGN KEY (postgroup_id) REFERENCES postgroups(id) DEFERRABLE;


--
-- Name: courses_sections_course_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY courses_sections
    ADD CONSTRAINT courses_sections_course_id_fkey FOREIGN KEY (course_id) REFERENCES courses(id) DEFERRABLE;


--
-- Name: courses_sections_section_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY courses_sections
    ADD CONSTRAINT courses_sections_section_id_fkey FOREIGN KEY (section_id) REFERENCES sections(id) DEFERRABLE;


--
-- Name: filegroup_weeks_filegroup_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filegroup_weeks
    ADD CONSTRAINT filegroup_weeks_filegroup_id_fkey FOREIGN KEY (filegroup_id) REFERENCES filegroups(id) DEFERRABLE;


--
-- Name: filegroups_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filegroups
    ADD CONSTRAINT filegroups_author_id_fkey FOREIGN KEY (author_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: filegroups_course_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filegroups
    ADD CONSTRAINT filegroups_course_id_fkey FOREIGN KEY (course_id) REFERENCES courses(id) DEFERRABLE;


--
-- Name: filegroups_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filegroups
    ADD CONSTRAINT filegroups_post_id_fkey FOREIGN KEY (post_id) REFERENCES posts(id) DEFERRABLE;


--
-- Name: filegroups_section_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filegroups
    ADD CONSTRAINT filegroups_section_id_fkey FOREIGN KEY (section_id) REFERENCES sections(id) DEFERRABLE;


--
-- Name: filegroups_uploader_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filegroups
    ADD CONSTRAINT filegroups_uploader_id_fkey FOREIGN KEY (uploader_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: filerates_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filerates
    ADD CONSTRAINT filerates_file_id_fkey FOREIGN KEY (file_id) REFERENCES files(id) DEFERRABLE;


--
-- Name: filerates_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY filerates
    ADD CONSTRAINT filerates_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: files_filegroup_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_filegroup_id_fkey FOREIGN KEY (filegroup_id) REFERENCES filegroups(id) DEFERRABLE;


--
-- Name: files_uploader_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_uploader_id_fkey FOREIGN KEY (uploader_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: posts_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_post_id_fkey FOREIGN KEY (post_id) REFERENCES posts(id) DEFERRABLE;


--
-- Name: posts_postgroup_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_postgroup_id_fkey FOREIGN KEY (postgroup_id) REFERENCES postgroups(id) DEFERRABLE;


--
-- Name: posts_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: problems_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY problems
    ADD CONSTRAINT problems_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: sections_faculty_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sections
    ADD CONSTRAINT sections_faculty_id_fkey FOREIGN KEY (faculty_id) REFERENCES faculties(id) DEFERRABLE;


--
-- Name: telemetry_data_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telemetry_data
    ADD CONSTRAINT telemetry_data_event_id_fkey FOREIGN KEY (event_id) REFERENCES telemetry_events(id);


--
-- Name: telemetry_events_session_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY telemetry_events
    ADD CONSTRAINT telemetry_events_session_id_fkey FOREIGN KEY (session_id) REFERENCES telemetry_sessions(id);


--
-- Name: user_addresses_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_addresses
    ADD CONSTRAINT user_addresses_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: user_favcourses_course_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_favcourses
    ADD CONSTRAINT user_favcourses_course_id_fkey FOREIGN KEY (course_id) REFERENCES courses(id) DEFERRABLE;


--
-- Name: user_favcourses_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_favcourses
    ADD CONSTRAINT user_favcourses_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: user_rights_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_rights
    ADD CONSTRAINT user_rights_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: user_sessions_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_sessions
    ADD CONSTRAINT user_sessions_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) DEFERRABLE;


--
-- Name: users_faculty_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_faculty_id_fkey FOREIGN KEY (faculty_id) REFERENCES faculties(id) DEFERRABLE;


--
-- Name: users_section_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_section_id_fkey FOREIGN KEY (section_id) REFERENCES sections(id) DEFERRABLE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


