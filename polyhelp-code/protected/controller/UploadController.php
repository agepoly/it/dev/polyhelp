<?php

/**
 * Controller for the upload section.
 */

class UploadController extends EPFHelpParentController {

    static private $data_defaults = Array(
        'section' => 'Upload',
        'css' => Array('upload'),
        'js' => Array(),
        'errors' => Array(),
        'warnings' => Array(),
        'successes' => Array(),
    );

    /**
     * Get the info of the filegroup in DB from a file id.
     */
    private function retrieve_filegroup_from_fid($file_id) {
        /* get the filegroup info */
        $q =
            'SELECT fg.id, fg.course_id, fg.name, fg.category, fg.author_id,
                    fg.uploader_id, fg.post_id, fg.section_id AS section
            FROM filegroups fg
            JOIN files f ON f.filegroup_id = fg.id
            WHERE f.id = :id';

        $r = Doo::db()->query($q, Array(':id' => $file_id));
        $info = $r->fetch();

        if ($r->rowCount() < 1)
            return false;

        /* get the file weeks */
        $filegroup_id = $info['id'];
        $q =
            'SELECT week
            FROM filegroup_weeks
            WHERE filegroup_id = :id';

        $r = Doo::db()->query($q, Array(':id' => $filegroup_id));
        $info['weeks'] = Array();
        while ($row = $r->fetch()) {
            $info['weeks'][] = intval($row['week']);
        }

        return $info;
    }

    /**
     * Get the info of the file from the database from its id.
     *
     * the filegroup_id argument is used to filter out files that are not in
     * the same group (we want one group at a time).
     */
    private function retrieve_file($id, $fg_id) {
        $q =
            'SELECT f.subtype, f.filepath, f.uploader_id,
                    extract(epoch FROM f.creation_time) AS creation_time,
                    f.description, f.filepath, f.extension, f.origin
            FROM files f
            JOIN filegroups fg ON fg.id = f.filegroup_id
            WHERE f.id = :id AND f.filegroup_id = :fgid';

        $r = Doo::db()->query($q, Array(':id' => $id, ':fgid' => $fg_id));

        return $r->fetch();
    }

    /**
     * Get from the author, professors and search it in the DB, search for
     * the associated course too and fill it in the array.
     */
    private function get_suggestions(&$filegroup) {
        $fsa = Array();
        $fsc = Array();

        $search_author = (isset($filegroup['author'])
            && strlen($filegroup['author']) > 3);
        $search_select = count($filegroup['sample_select']) > 0;

        if ($search_author || $search_select) {
            if ($search_author) {
                /* get the last part of the author and use that */
                $ar = explode(' ', $filegroup['author']);
                $author = end($ar);
            }

            /* get the professors with their courses */
            $q =
                'SELECT u.id AS uid, u.first_name AS fname,
                        u.last_name AS lname, c.id AS cid, c.name AS cname
                FROM users u
                JOIN course_prof cf ON u.id = cf.user_id
                JOIN courses c ON c.id = cf.course_id
                JOIN courses_sections cs ON cs.section_id = :section
                     AND cs.course_id = c.id
                WHERE ';

            $params = Array(':section' => $this->user_section());

            /* add the author suggestion */
            if ($search_author) {
                $q .= '(first_name || last_name ~* :name)';
                array_append($params, Array(':name' => $author));
            }

            /* add the filegroup sample suggestions */
            if ($search_select) {
                /* try by author name */
                foreach ($filegroup['sample_select'] as $k => $v) {
                    $v = preg_replace('/\s+/', '%', $v);
                    if ($search_author || $k > 0) $q .= ' OR ';
                    $q .= ' (first_name || last_name ILIKE :selecta'. $k .')';
                    $params[':selecta'. $k] = '%'. $v .'%';
                }

                /* try by course name too */
                foreach ($filegroup['sample_select'] as $k => $v) {
                    $v = preg_replace('/e/i', '(é|è|e)', $v);
                    $v = preg_replace('/\s+/', '.*', $v);
                    $q .= ' OR (c.name ~* :selectb'. $k .')';
                    $params[':selectb'. $k] = $v;
                }
            }

            $q .=
                'ORDER BY last_name, first_name
                LIMIT 5';

            $r = Doo::db()->query($q, $params);

            while ($row = $r->fetch()) {
                /* detect perfect match and only keep it */
                $score = 0;
                $fullname = strtolower($row['fname'] .' '. $row['lname']);
                $coursename = strtolower($row['cname']);

                if ($search_author) {
                    if (strtolower($filegroup['author']) == $fullname) {
                        $score += 1;
                    }
                }

                if ($search_select) {
                    foreach ($filegroup['sample_select'] as $k => $v) {
                        $v = strtolower($v);
                        if ($v == $fullname) {
                            $score += 1;
                        }
                        if ($v == $coursename) {
                            $score += 1;
                        }
                    }
                }

                /* in the other case, we keep the suggestions */
                $fsa[] = Array(
                    'id'     =>  $row['uid'],
                    'fname'  =>  $row['fname'],
                    'lname'  =>  $row['lname'],
                    'score'  =>  $score,
                );
                $fsc[] = Array(
                    'id'    =>  $row['cid'],
                    'name'  =>  $row['cname'],
                    'score'  =>  $score,
                );
            }

            /* remove duplicates professors */
            array_rem_duplicates($fsa, function ($a, $b) {
                return $a['id'] == $b['id'];
            });

            if (count($fsa) == 0) {
                /* get the students that match too */
                $q =
                    'SELECT u.id AS uid, u.first_name AS fname,
                            u.last_name AS lname
                    FROM users u
                    WHERE professor = false AND (';

                $params = Array();

                if ($search_author) {
                    $q .= 'first_name || last_name ~* :name';
                    array_append($params, Array(':name' => $author));
                }

                if ($search_select) {
                    foreach ($filegroup['sample_select'] as $k => $v) {
                        $v = preg_replace('/\s+/', '%', $v);
                        if ($search_author || $k > 0) $q .= ' OR ';
                        $q .= ' (first_name || last_name ILIKE :select'. $k .')';
                        $params[':select'. $k] = '%'. $v .'%';
                    }
                }

                $q .=
                    ')
                    ORDER BY last_name, first_name
                    LIMIT 5';

                $r = Doo::db()->query($q, $params);

                while ($row = $r->fetch()) {
                    $fsa[] = Array(
                        'id'     =>  $row['uid'],
                        'fname'  =>  $row['fname'],
                        'lname'  =>  $row['lname'],
                        'score'  =>  0,
                    );
                }
            }
        }

        usort($fsa, function ($a, $b) {
            return -intcmp($a['score'], $b['score']);
        });
        usort($fsc, function ($a, $b) {
            return -intcmp($a['score'], $b['score']);
        });

        /* if there isn't any suggested course add 'aucune' and other stuff */
        if (count($fsc) == 0) {
            $fsc[] = Array(
                'id'    =>  0,
                'name'  =>  'Aucune (veuillez choisir)',
                'score' =>  -1,
            );

            /* we add the courses that were favorited by the user */
            $q =
                'SELECT c.id AS cid, c.name AS cname, u.first_name AS fname,
                        u.last_name AS lname, u.id AS uid
                FROM user_favcourses uf
                JOIN courses c ON uf.course_id = c.id
                JOIN course_prof cp ON cp.course_id = uf.course_id
                JOIN users u ON u.id = cp.user_id
                WHERE uf.user_id = :uid';

            $r = Doo::db()->query($q, Array(':uid' => $this->user_id()));

            while ($row = $r->fetch()) {
                $fsa[] = Array(
                    'id'     =>  $row['uid'],
                    'fname'  =>  $row['fname'],
                    'lname'  =>  $row['lname'],
                    'score'  =>  -1,
                );
                $fsc[] = Array(
                    'id'    =>  $row['cid'],
                    'name'  =>  $row['cname'],
                    'score'  =>  -1,
                );
            }
        }

        /* merge results */
        array_append($filegroup['suggest_authors'], $fsa);
        array_append($filegroup['suggest_courses'], $fsc);
    }

    /**
     * Get the authors that makes courses in the same section; then we add the
     * students too.
     */
    private function get_authors_courses(&$authors, &$courses, $section) {
        /* get the professors list along its courses */
        $q =
            'SELECT u.id AS uid, u.first_name AS fname,
                u.last_name AS lname, c.id AS cid, c.name AS cname, cs.degree
            FROM users u
            JOIN course_prof cf ON u.id = cf.user_id
            JOIN courses c ON c.id = cf.course_id
            JOIN courses_sections cs ON cs.section_id = :section
                 AND cs.course_id = c.id
            WHERE u.id > 1 AND u.professor = true
            ORDER BY u.id';

        $r = Doo::db()->query($q, Array(':section' => $section));
        if ($r->rowCount() < 1) return false;

        while ($row = $r->fetch()) {
            $authors[0][] = Array( # professors
                'id'     =>  $row['uid'],
                'fname'  =>  $row['fname'],
                'lname'  =>  $row['lname'],
            );
            $courses[$row['degree']][] = Array(
                'id'    =>  $row['cid'],
                'name'  =>  $row['cname'],
            );
        }

        /* get the students list */
        $q =
            'SELECT id, first_name, last_name
            FROM users
            WHERE id > 1 AND professor = false
            ORDER BY last_name, first_name';

        $r = Doo::db()->query($q);

        while ($row = $r->fetch()) {
            $authors[1][] = Array(# students
                'id'     =>  $row['id'],
                'fname'  =>  $row['first_name'],
                'lname'  =>  $row['last_name'],
            );
        }

        /* sort by year (array key) and by authors type */
        ksort($courses); # order: 1st, 2nd, 3rd, etc years
        ksort($authors); # order: professors then students

        /* sort each sub array in alphabetic order */
        foreach (array_keys($authors) as $key) {
            usort($authors[$key], function ($a, $b) {
                return strcmp($a['lname'].$a['fname'], $b['lname'].$b['fname']);
            });
        }
        foreach (array_keys($courses) as $key) {
            usort($courses[$key], function ($a, $b) {
                return strcmp($a['name'], $b['name']);
            });
        }

        /* remove duplicates professors (because one teaches lots of courses) */
        array_rem_duplicates($authors[0], function ($a, $b) {
            return $a['id'] == $b['id'];
        });
    }

    /**
     * Returns the list of semester weeks given a timestamp.
     */
    private function get_semester_weeks($timestamp) {
        $year = intval(date('Y', $timestamp));
        $cur_semester = Globals::$semesters[Globals::get_semester($timestamp)];
        $first_week = $cur_semester['first_week'];
        $last_week = $cur_semester['last_week'];
        $weeks = Array();

        /* generate the semester weeks for the week checkboxes */
        for ($i = $first_week; $i <= $last_week; ++$i) {
            $week_time = Globals::time_year_week($year, $i);
            $weeks[] = Array(
                'id'    => $i,
                'text'  => Globals::format_date_duration($week_time,
                                                         $week_time+3600*24*5),
            );
        }

        return $weeks;
    }

    /**
     * Returns the user id of the teacher for the given course.
     */
    private function get_course_profs($section_id, $course_id) {
        $q =
            'SELECT u.id, u.first_name AS fname, u.last_name AS lname
            FROM users u
            JOIN course_prof cp ON cp.user_id = u.id
            JOIN courses_sections cs ON cs.course_id = cp.course_id
                 AND cs.course_id = :cid AND cs.section_id = :sid
            ORDER BY lname, fname';

        $r = Doo::db()->query($q, Array(':sid' => $section_id,
                                        ':cid' => $course_id));

        $ar = $r->fetchAll();
        array_rem_duplicates($ar, function ($a, $b) {
            return $a['id'] == $b['id'];
        });

        return $ar;
    }

    /**
     * Update the files and filegroup given in argument.
     *
     * Update the filegroup only if $fg_edit is true; $fg and $pfg contains the
     * database and user filegroup info; $fe and $pfe contains the database and
     * user files info.
     */
    private function update_files($fg_edit, $fg, $pfg, $fe, $pfe) {
        $filegroup_id = $fg['id'];
        $post_id = $fg['post_id'];
        $course_id_new = $pfg['course_default'];

        $filegroup_path_new = Globals::upload_file_dir($course_id_new,
            $filegroup_id);

        Doo::db()->beginTransaction();

        try {
            /* if the filegroup needs to be updated */
            if ($fg_edit) {
                /* move the post of the filegroup */
                $q =
                    'UPDATE posts
                    SET postgroup_id = (
                        SELECT postgroup_id FROM courses WHERE id = :cid
                        ), title = :title
                    WHERE id = :pid';

                $r = Doo::db()->query($q, Array(
                    ':title'  =>  'Fichier: '. $pfg['title'],
                    ':cid'    =>  $course_id_new,
                    ':pid'    =>  $post_id,
                ));

                /* update the filegroup */
                $q =
                    'UPDATE filegroups
                    SET course_id = :cid, name = :name, category = :category,
                        author_id = :aid, reviewed = :reviewed,
                        section_id = :section
                    WHERE id = :fid';

                $r = Doo::db()->query($q, Array(
                    ':cid'        =>  $course_id_new,
                    ':name'       =>  $pfg['title'],
                    ':category'   =>  $pfg['category'],
                    ':aid'        =>  $pfg['author_default'],
                    ':reviewed'   =>  true,
                    ':fid'        =>  $filegroup_id,
                    ':section'    =>  $pfg['preferred_section'],
                ));

                /* update the weeks (first remove them) */
                $q =
                    'DELETE FROM filegroup_weeks
                    WHERE filegroup_id = :fid';

                $params = Array(':fid' => $filegroup_id);
                $r = Doo::db()->query($q, $params);

                $q =
                    'INSERT INTO filegroup_weeks (filegroup_id, week)
                    VALUES ';

                foreach ($pfg['weeks_defaults'] as $k => $week) {
                    if ($k > 0) $q .= ', ';

                    $q .= '(:fid, :week'. $k .')';
                    $params[':week'. $k] = $pfg['weeks_defaults'][$k];
                }

                $r = Doo::db()->query($q, $params);
            }

            /* create the new filegroup directory if needed */
            if (!file_exists($filegroup_path_new)) {
                if (!mkdir($filegroup_path_new, 0777, true)) {
                    throw new Exception("Cannot create the directory: "
                        .$filegroup_path_new);
                }
            }

            /* update the files info */
            foreach ($pfe as $k => $file_info_new) {
                $file_info = $fe[$k];

                $file_path = $file_info['filepath'];
                $title_new = Globals::upload_shorttitle($pfg, $file_info_new);
                $file_path_new = Globals::upload_file_name($file_info['id'],
                    $title_new, $file_info['extension']);
                $file_fullpath_new = $filegroup_path_new .'/'. $file_path_new;

                if ($file_info['filepath'] != $file_fullpath_new) {
                    /* if a rename failed we commit so the DB and the files
                     * stays consistent. */
                    if (!rename($file_info['filepath'], $file_fullpath_new)) {
                        Doo::db()->commit();
                        return false;
                    }
                }

                /* calculate the new rating_base */
                $rating_base = file_base_score($file_info_new);

                $q =
                    'UPDATE files
                    SET subtype = :subtype, filepath = :filepath,
                        creation_time = to_timestamp(:ctime),
                        description = :desc, origin = :orig,
                        rating_base = :rbase
                    WHERE id = :fid';

                $r = Doo::db()->query($q, Array(
                    ':subtype'   =>  $file_info_new['corrections'],
                    ':orig'      =>  $file_info_new['origin'],
                    ':filepath'  =>  $file_fullpath_new,
                    ':ctime'     =>  $file_info_new['timestamp'],
                    ':desc'      =>  $file_info_new['comments'],
                    ':fid'       =>  $file_info['id'],
                    ':rbase'     =>  $rating_base,
                ));
            }
        }
        catch (Exception $e) {
            throw $e;
            Doo::db()->rollback();
            return false;
        }

        Doo::db()->commit();
        return true;
    }

    /**
     * Upload page, the first page shown.
     */
    public function start() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "upload";
        $data['js'][] = "upload";

        if (isset($this->params['sid']) && isset($this->params['cid'])) {
            $data['preferred_course']  = $this->params['cid'];
            $data['preferred_section'] = $this->params['sid'];
        }

        if (isset($this->params['fgid'])) {
            $data['preferred_filegroup'] = $this->params['fgid'];
        }

        $this->render('template', $data, $this);
    }

    /**
     * Upload page with information to fill, second step.
     */
    public function new_post() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "upload-post";
        $fg0 = Array();
        $data['fg0_edit'] = true;
        $log_errors = array();

        $preferred_course  = array_get($_POST, 'preferred_course', null);
        $preferred_section = array_get($_POST, 'preferred_section',
            $this->user_section());
        $preferred_filegroup = array_get($_POST, 'preferred_filegroup', null);

        /* control teacher access */
        if ($this->user_is_professor()
            && ($preferred_course === null
                || !$this->user_rights_teacher_course($preferred_course)))
        {
            $data['errors'][] = Array(
                'text' => "Vous n'avez pas le droit d'uploader pour ce cours "
                ."(vous ne pouvez uploader sans sélectionner un de vos cours)",
            );
            $log_errors[] = 'no-right-prof';
            goto error;
        }

        /* define which mode: the adding into an existing filegroup mode OR
         * the new filegroup mode */
        $mode = ($preferred_filegroup != null) ? 'add' : 'new';

        /* Support only one file group at once for upload yet */
        $files = Globals::files_array($_FILES);

        //TODO dans certain cas cette variable est nul et tout plante :(
        //un simple if ne suffit pas a éviter le problème :(

            $cur_group = $files['files-0'];
            $file_count = count($cur_group);

            /* complete the category of files */
            $data['degrees']          = Globals::$degrees;

            /* foreach file in this group, manage it */
            for ($i = 0; $i < $file_count; ++$i) {
                $cur_file = $cur_group[$i];

                if ($cur_file['name'] == "") {
                    $data['errors'][] = Array(
                        'text' => "Un fichier n'a pas de nom, avez-vous oublié "
                        ."de spécifier un fichier ?",
                    );
                    $log_errors[] = 'no-name';
                    goto error;
                }

                if ($cur_file['error'] != 0 || !file_exists($cur_file['tmp_name']))
                {
                    $data['errors'][] = Array(
                        'text' => "Erreur lors de l'upload du fichier nommé: "
                                  . $cur_file['name'],
                    );
                    $log_errors[] = 'uploading-error';
                    goto error;
                }

                $file_mime = file_get_mime($cur_file['tmp_name']);
                if ($file_mime === false
                 || !array_key_exists($file_mime, Globals::$upload_accept_mimes))
                {
                    if ($file_mime === false) $file_mime = 'inconnu';
                    $data['errors'][] = Array(
                        'text' => "Le fichier {$cur_file['name']} "
                        ."({$file_mime}) n'est pas à un format autorisé",
                    );
                    $log_errors[] = 'format-not-accepted';
                    goto error;
                }

                /* extract informations, black magick! */
                $file_info = file_extract_info($cur_file, $file_mime);

                /* fill the info into the filegroup fields if necessary */
                foreach (Array('title', 'course', 'category', 'author', 'date',
                          'timestamp') as $key)
                {
                    if (isset($file_info[$key]) && !isset($fg0[$key])) {
                        $fg0[$key] = $file_info[$key];
                    }
                }

                if ($mode == 'new') {
                    /* merge suggestions extracted from the file samples */
                    array_append($fg0['sample_select'],
                        array_get($file_info, 'sample_select', Array()));
                }

                /* fill the info into the file fields */
                $fe0[] = $data['files-entries-0'][] = array_merge(Array(
                    'date'        =>  date(Globals::$date_day_format),
                    'timestamp'   =>  time(),
                    'comments'    =>  '',
                    'filename'    =>  $cur_file['name'],
                    'corrections' =>  1,
                    'origin'      =>  0,
                    'extension'   =>  Globals::$upload_accept_mimes[$file_mime],
                    'tmp_name'    =>  $cur_file['tmp_name'],
                ), $file_info);
            }

        /* get suggestions/list of authors/courses/weeks and all that only if
         * it is a new fle which has a new filegroup */
        if ($mode == 'new') {
            $fg0['suggest_authors'] = Array();
            $fg0['suggest_courses'] = Array();

            /* if the user has come from a course, suggest it here */
            if ($preferred_course != null) {
                $fg0['course_default'] = $preferred_course;
                $fg0['suggest_authors'] =
                    $this->get_course_profs($preferred_section,
                        $preferred_course);
            }
            else {
                /* we can search the author in the DB and find the course too */
                $this->get_suggestions($fg0);
            }

            /* get the first one of each as default ones */
            if (count($fg0['suggest_courses']) > 0) {
                $fg0['course_default'] = $fg0['suggest_courses'][0]['id'];
            }
            if (count($fg0['suggest_authors']) > 0) {
                $fg0['author_default'] = $fg0['suggest_authors'][0]['id'];
            }

            /* prepare the authors and courses list */
            $ret = $this->get_authors_courses($data['authors'],
                $data['courses'], $preferred_section);

            if ($ret === false) {
                $data['errors'][] = Array(
                    'text' => "La section visitée ne contient aucun cours ou cela "
                    ."veut probablement dire que la section choisie n'existe pas",
                );
                $log_errors[] = 'section-not-valid';
                goto error;
            }

            /* default values for the filegroup info if empty */
            array_prepend($fg0, Array(
                'title'             =>  $fe0[0]['filename'],
                'course'            =>  'inconnu',
                'course_default'    =>  0,
                'category'          =>  3,
                'author'            =>  'inconnu',
                'author_default'    =>  1,
                'date'              =>  date(Globals::$date_day_format),
                'timestamp'         =>  time(),
                'preferred_section' => $preferred_section,
            ));

            /* prepare the weeks checkboxes from the file date */
            $fg_date = array_get($fg0, 'timestamp', time());
            $data['fg0-semester_weeks'] = $this->get_semester_weeks($fg_date);

            /* set default weeks depending on categories */
            $file_week = intval(date('W', $fg0['timestamp']));
            $fg0['weeks_defaults'] = Array();
            if (in_array($fg0['category'], Array(1, 3))) {
                array_push($fg0['weeks_defaults'],
                    $file_week-1, $file_week, $file_week+1);
            }
            else if ($fg0['category'] == 4) {
                array_append($fg0['weeks_defaults'], array_map(function ($v) {
                    return $v['id'];
                }, $data['fg0-semester_weeks']));
            }
            else {
                $fg0['weeks_defaults'][] = $file_week;
            }

            $data['filegroups-0'] = $fg0;
        }
        /* in the other cases we just hide the form part that allows the
         * edition of the fg */
        else {
            $data['fg0_edit'] = false; 

            /* get the course for later uses */
            $q =
                'SELECT c.id AS cid, fg.author_id, fg.category, fg.name
                FROM courses c
                JOIN filegroups fg ON fg.course_id = c.id
                WHERE fg.id = :fgid
                LIMIT 1';

            $r = Doo::db()->query($q, Array(':fgid' => $preferred_filegroup));
            if (!$row = $r->fetch()) {
                $data['errors'][] = Array(
                    'text' => "Le groupe de fichiers n'existe pas ou alors "
                    ."il n'est pas associé à un cours",
                );
                $log_errors[] = 'filegroup-not-valid';
                goto error;
            }

            $fg0 = Array(
                'title'             =>  $row['name'],
                'course_default'    =>  $row['cid'],
                'category'          =>  $row['category'],
                'author_default'    =>  $row['author_id'],
            );
        }

        /* insert the current data in the database so we can edit them
         * longer afterward or in the case the user stop here. */
        Doo::db()->beginTransaction();

        $user_id = $this->user_id();

        try {
            /* add a new filegroup with its associated posts */
            if ($mode == 'new') {
                /* insert the file post */
                $q =
                    'INSERT INTO posts (user_id, postgroup_id, post_id, title,
                        content, file_related)
                    VALUES (:uid, (
                        SELECT postgroup_id FROM courses WHERE id = :cid
                        ), NULL, :title, :content, :frelated)';

                $post_content =
                    "Ceci est un post automatique ajouté lors de l'ajout de "
                    ."ce nouveau fichier.\n\nPostez ici vos commentaires, "
                    ."questions ou autres remarques";

                $r = Doo::db()->query($q, Array(
                    ':uid'       =>  $user_id,
                    ':cid'       =>  $fg0['course_default'],
                    ':title'     =>  'Fichier: '. $fg0['title'],
                    ':content'   =>  $post_content,
                    ':frelated'  =>  'true'
                ));
                $post_id = Doo::db()->lastInsertId('posts_id_seq');

                /* insert the filegroup */
                $q =
                    'INSERT INTO filegroups (course_id, name, category, post_id,
                        author_id, uploader_id, reviewed, section_id)
                    VALUES (:cid, :name, :cat, :pid, :aid, :uid, :rev, :sid)';

                $r = Doo::db()->query($q, Array(
                    ':cid' => $fg0['course_default'], ':name' => $fg0['title'],
                    ':cat' => $fg0['category'],       ':pid' => $post_id,
                    ':aid' => $fg0['author_default'], ':rev' => 'false',
                    ':uid' => $user_id, ':sid' => $fg0['preferred_section'],
                ));
                $filegroup_id = Doo::db()->lastInsertId('filegroups_id_seq');
                $course_default = $fg0['course_default'];
            }
            /* in the case of adding a file to a filegroup we get from POST */
            else {
                $filegroup_id = $preferred_filegroup;
            }

            /* create the directory for the file group */
            $fg_dir = Globals::upload_file_dir($fg0['course_default'],
                $filegroup_id);
            if (!file_exists($fg_dir) && !mkdir($fg_dir, 0777, true)) {
                $data['errors'][] = Array(
                    'text' => 'impossible de créer le dossier des fichiers',
                );
                $log_errors[] = 'could-not-mkdir';
                goto error;
            }

            /* insert the files in the filegroup */
            foreach ($fe0 as $i => $cur_file) {
                /* calculate the file base score */
                $rating_base = file_base_score($cur_file);

                $q =
                    'INSERT INTO files (filegroup_id, subtype, extension,
                        filepath, uploader_id, rating_base,
                        creation_time, description, origin)
                    VALUES (:fg, :subtype, :ext, :filepath, :uid, :rbase,
                        to_timestamp(:ctime), :desc, :orig)';

                /* this is the file information, note that the path includes
                 * the very ID of it, so we need to do it in two steps and
                 * update the filepath in the next step, for the moment we set
                 * it to null */
                $r = Doo::db()->query($q, Array(
                    ':fg'        =>  $filegroup_id,
                    ':subtype'   =>  $cur_file['corrections'],
                    ':orig'      =>  $cur_file['origin'],
                    ':ext'       =>  $cur_file['extension'],
                    ':filepath'  =>  NULL,
                    ':uid'       =>  $user_id,
                    ':rbase'     =>  $rating_base,
                    ':ctime'     =>  $cur_file['timestamp'],
                    ':desc'      =>  $cur_file['comments'],
                ));
                $file_id = Doo::db()->lastInsertId('files_id_seq');
                $file_id_list[] = $file_id;

                /* choose a name and move the file */
                $short_title = Globals::upload_shorttitle($fg0, $cur_file);
                $filepath = $fg_dir . '/' .
                    Globals::upload_file_name($file_id, $short_title,
                        $cur_file['extension']);

                if (!move_uploaded_file($cur_file['tmp_name'],
                    $filepath))
                {
                    $data['errors'][] = Array(
                        'text' => 'impossible de déplacer un fichier',
                    );
                    $log_errors[] = 'could-not-move';
                    goto error;
                }

                /* we don't forget to update our file path in the DB */
                $q = 'UPDATE files SET filepath = :fp WHERE id = :fid';
                $r = Doo::db()->query($q, Array(
                    ':fp'  => $filepath,
                    ':fid' => $file_id
                ));
            }

            /* add the weeks to new filegroups only */
            if ($mode == 'new') {
                /* we add the weeks concerned by the files */
                $q = 'INSERT INTO filegroup_weeks (filegroup_id, week) VALUES';
                foreach ($fg0['weeks_defaults'] as $k => $v) {
                    if ($k != 0) $q .= ', ';
                    $q .= " ({$filegroup_id}, {$v})";
                }
                $r = Doo::db()->query($q);
            }

            /* we add scores to the user, thank him for his upload */
            $q = 'UPDATE users SET score = score + :ds WHERE id = :uid';
            Doo::db()->query($q, Array(':uid' => $user_id,
                                       ':ds' => Globals::$score_file ));

            Doo::db()->commit();
            $ready = true;
        }
        catch (PDOException $e) {
            Doo::db()->rollback();
            $data['errors'][] = Array(
                'text' => "Erreur d'insertion des fichiers dans la base de "
                ."donnée, une exception s'est produite",
            );
            $log_errors[] = 'could-not-insert';
            goto error;
        }

        $data['edit_list'] = implode(',', $file_id_list);
        $data['degrees'] = Globals::$degrees;
        $data['filegroup_id'] = $filegroup_id;
        $data['block_links'][] = Array(
            'text' => "Voir la page de support pour ces fichiers",
            'link' => "/support/files/{$data['filegroup_id']}",
        );
        $data['sections'] = $this->sections();
        $data['preferred_section'] = $preferred_section;
        
        //if($_POST){
        //    return '/support/files/'.$data['filegroup_id'];
        //}
        
        
        error:

        Doo::loadModel('Telemetry');
        Telemetry::log('upload', array(
            'count' => $file_count,
            'filenames' => $_FILES['files-0']['name'],
            'errors' => $log_errors
        ));

        $this->render('template', $data, $this);
    }

    /**
     * File form to modify the file informations.
     */
    public function edit() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "upload-post";
        $data['edit_list'] = $this->params['ids'];
        $user_id = $this->user_id();
        $log_errors = array();

        /* get the list of files id to edit */
        $files_id = explode(',', $this->params['ids']);

        $fg0 = $this->retrieve_filegroup_from_fid($files_id[0]);

        /* if the user is changing the concerned section */
        $preferred_section = array_get($_POST, 'preferred_section',
            $fg0['section']);
        /* in case the preferred_section is not valid, reset to user section */
        $sections = $this->sections();
        $sections_id = array_map(function ($v) { return $v['id']; }, $sections);
        if (!in_array($preferred_section, $sections_id)) {
            $preferred_section = $this->user_section();
        }

        /* if the user hasn't the right to edit the filegroup, hide it */
        $data['filegroups-0'] = Array();
        $data['fg0_edit'] = $this->user_rights_filegroup($fg0)
            && !array_get($_POST, 'fg0_hide', false);

        /* check if the file exists */
        if ($fg0 === false) {
            $data['errors'][] = Array(
                'text' => "Le fichier n'existe pas ou alors il n'est "
                ."associé à aucun groupe",
            );
            $log_errors[] = 'file-not-valid';
            goto error;
        }

        /* retrieve each file info and check user rights */
        $files_info = Array();
        foreach ($files_id as $k => $file_id) {
            $files_info[$k] = $this->retrieve_file($file_id, $fg0['id']);
            $files_info[$k]['id'] = $file_id;

            /* check if the file exists */
            if ($files_info === false) {
                $data['errors'][] = Array(
                    'text' => "Le fichier N°{$file_id} n'a pas été trouvé",
                );
                $log_errors[] = 'file-not-there';
                goto error;
            }

            /* check edition rights of the user */
            if (!$this->user_rights_file($files_info[$k], $fg0)) {
                $data['errors'][] = Array(
                    'text' => "Vous n'avez pas les droits suffisants pour "
                    ."éditer le fichier N°{$file_id}",
                );
                $log_errors[] = 'no-right';
                goto error;
            }

            /* check the file actually exists in the right place, avoiding
             * future errors */
            if (!file_exists($files_info[$k]['filepath'])) {
                $data['errors'][] = Array(
                    'text' => "Le fichier N°{$file_id} n'a pas été trouvé à "
                    ."son emplacement (erreur interne)",
                );
                $log_errors[] = 'file-not-at-filepath';
                goto error;
            }
        }

        /* if the user is POSTing his change and not only changing the
         * concerned/preferred section */
        if ($_POST && !isset($_POST['section_switcher'])) {
            $errors = 0;
            $pfg0 = Array();
            $pfe0 = Array();

            /* parse post variables */
            foreach ($_POST as $k => $v) {
                if (preg_match('/^fg0-(\d)+-(.*)$/', $k, $matches) > 0) {
                    $pfe0[intval($matches[1])][$matches[2]] = $v;
                }
                elseif (preg_match('/fg0-(.*)$/', $k, $matches) > 0) {
                    $pfg0[$matches[1]] = $v;
                }
            }

            /* if the user has the rights, update the filegroup */
            if ($data['fg0_edit']) {
                $fields_check = Array(
                    'course' => Array('name' => 'Cours',  'type' => 'numeric'),
                    'title'  => Array('name' => 'Titre',  'type' => 'string',
                                      'maxlen' => 87, 'minlen' => 5,
                                      'clean' => true),
                    'type'   => Array('name' => 'Type',   'type' => 'numeric',
                                      'in_array' => 
                                        array_keys(Globals::$file_categories)),
                    'author' => Array('name' => 'Auteur', 'type' => 'numeric'),
                    'weeks'  => Array('name' => 'Semaines concernées',
                                  'type' => 'array', 'minlen' => 1,
                                  'maxlen' => false),
                );

                $errors += check_form($fields_check, $pfg0, $data['warnings'],
                    $results);


                /* control teacher access */
                if ($this->user_is_professor()
                 && !$this->user_rights_teacher_course($pfg0['course']))
                {
                    $data['warnings'][] = Array(
                        'text' => "Vous ne pouvez pas uploader pour ce cours",
                    );
                    $log_errors[] = 'no-right-prof';
                    $errors += 1;
                }

                /* fill the filegroup info based on the POST */
                $data['filegroups-0'] = Array(
                    'course_default' => array_get($pfg0, 'course', null),
                    'title'          => array_get($results, 'title', null),
                    'category'       => array_get($pfg0, 'type',   null),
                    'author_default' => array_get($pfg0, 'author', null),
                    'weeks_defaults' => array_get($pfg0, 'weeks',  null),
                    'preferred_section' => $preferred_section,
                );
                array_remove_null($data['filegroups-0']);
            }
            /* if the fg0 is not edited, get current information about it */
            else {
                $data['filegroups-0'] = array_merge($fg0, Array(
                    'title'          => $fg0['name'],
                    'course_default' => $fg0['course_id'],
                    'author_default' => $fg0['author_id'],
                ));
            }

            $file_fields_check = Array(
                'corrections' => Array(
                                   'name' => 'Corrections',
                                   'type' => 'numeric',
                                   'in_array' =>
                                     array_keys(Globals::$file_corrections)),
                'date'        => Array('name' => 'Date', 'type' => 'date'),
                'comments'    => Array( 
                                   'name' => 'Commentaires',
                                   'type' => 'string', 'maxlen' => false,
                                   'minlen' => false, 'clean' => true),
                'origin'      => Array(
                                   'name' => 'Provenance',
                                   'type' => 'numeric',
                                   'in_array' =>
                                     array_keys(Globals::$file_origins)
                ),
            );

            /* update the file entry using the form fields */
            foreach ($files_id as $k => $file_id) {
                /* we specify which file the field is describing */
                $check = $file_fields_check;
                foreach (array_keys($check) as $field) {
                    $check[$field]['name'] .= " (fichier ". ($k+1) .")";
                }

                $errors += check_form($check, $pfe0[$k], $data['warnings'],
                    $form_parsed);

                /* fill the file entry info based on the POST */
                if ($errors == 0) {
                    /* parse and check the date */
                    if (!isset($fg_date) && $form_parsed['date']) {
                        $fg_date = $form_parsed['date'];
                    }

                    $data['files-entries-0'][$k] = Array(
                        'corrections'  => $pfe0[$k]['corrections'],
                        'origin'       => $pfe0[$k]['origin'],
                        'date'         => $pfe0[$k]['date'],
                        'timestamp'    => $form_parsed['date'],
                        'comments'     => $form_parsed['comments'],
                        'filename'     => basename($files_info[$k]['filepath']),
                        'extension'    => $files_info[$k]['extension'],
                    );
                }
            }

            /* update the files and filegroup at once */
            if ($errors == 0) {
                $ret = $this->update_files($data['fg0_edit'], $fg0,
                    $data['filegroups-0'], $files_info,
                    $data['files-entries-0']);

                if (!$ret) {
                    $data['errors'][] = Array(
                        'text' => "Impossible de mettre à jour les données "
                        ."des fichiers, c'est probablement une erreur de la "
                        ."base de données",
                    );
                    $log_errors[] = 'could-not-edit';
                    goto error;
                }
                else {
                    $data['successes'][] = Array(
                        'text' => "L'enregistement des données de tous les "
                        ."fichiers a bien été effectué",
                    );
                }
            }
        }

        /* retrieve and fill files informations in get mode (fills blank fields
         * in POST mode) */
        foreach ($files_id as $k => $file_id) {
            /* fill the form fields on this file */
            $creation_time = intval($files_info[$k]['creation_time']);
            array_prepend($data['files-entries-0'][$k], Array(
                'corrections'  => $files_info[$k]['subtype'],
                'origin'       => $files_info[$k]['origin'],
                'date'         => date(Globals::$date_day_format,
                    $creation_time),
                'timestamp'    => $creation_time,
                'comments'     => $files_info[$k]['description'],
                'filename'     => basename($files_info[$k]['filepath']),
            ));

            if (!isset($fg_date)) {
                $fg_date = $creation_time;
            }
        }

        /* fill the form fields on the filegroup (fill blank files on POST) */
        array_prepend($data['filegroups-0'], Array(
            'course_default' => $fg0['course_id'],
            'title'          => $fg0['name'],
            'category'       => $fg0['category'],
            'author_default' => $fg0['author_id'],
            'weeks_defaults' => $fg0['weeks'],
        ));

        if (!isset($fg_date)) {
            $fg_date = time();
        }

        $data['authors'] = Array();
        $data['courses'] = Array();

        /* fill the form fields */
        $this->get_authors_courses($data['authors'], $data['courses'],
            $preferred_section);
        $data['degrees']              = Globals::$degrees;
        $data['fg0-semester_weeks']   = $this->get_semester_weeks($fg_date);
        $data['filegroup_id']         = $fg0['id'];
        $data['block_links'][] = Array(
            'text' => "Voir la page de support pour ces fichiers",
            'link' => "/support/files/{$data['filegroup_id']}",
        );

        $data['preferred_section'] = $preferred_section;
        $data['degrees'] = Globals::$degrees;
        $data['sections'] = $this->sections();

        $filegroup_id = $fg0['id'];
        if($_POST && !isset($_POST['section_switcher'])){
            Doo::loadModel('Telemetry');
            Telemetry::log('file/edited', array(
                'file-ids' => $this->params['ids'],
                'errors' => $log_errors
            ));

            // If there is no error nor warning, we redirect to the file view 
            if(empty($data['errors']) && empty($data['warnings'])){
                return '/support/files/'.$data['filegroup_id'];
            }
        }
        
        error:
        

        Doo::loadModel('Telemetry');
        Telemetry::log('file/editing', array(
            'file-ids' => $this->params['ids'],
            'errors' => $log_errors
        ));

        $this->render('template', $data, $this);
    }
}

?>
