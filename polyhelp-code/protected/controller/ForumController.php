<?php

/**
 * Controller for the forum page.
 */

class ForumController extends EPFHelpParentController {

    static private $data_defaults = array(
        'section' => 'Forum',
        'css' => array('forum'),
        'js' => array('jquery.dataTables.min', 'tables_custom'),
        'errors' => Array(),
    );

    /**
     * Page displayed when viewing, creating or appending a new post.
     */
    public function post() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "forum-post";
        $data['js'][] = "forum-post";
        $data['js'][] = "form_hint";
        $data['js'][] = "block_report";
        $action = array_get($_POST, 'action', 'view');
        $parent_id = array_get($_POST, 'parent', $this->params['pid']);

        /* check that the parent_id is not missing */
        if (!isset($parent_id)) {
            $data['errors'][] = array(
                'text' => "Certains paramètres sont manquants",
                );
            goto error;
        }

        /* add a new post to an existing parent post */
        if ($action == 'add' || $action == 'view') {
            $post_id = $parent_id;
            /* get the minimal parent post information and check it exists */
            $q =
                'SELECT id, postgroup_id, file_related
                FROM posts
                WHERE id = :pid AND post_id IS NULL';

            $r = Doo::db()->query($q, array(':pid' => $post_id ));
            if (!$parent_info = $r->fetch()) {
                $data['errors'][] = array(
                    'text' => "Le post choisi ne semble pas exister",
                );
                goto error;
            }
        }
        /* create a new post in a postgroup without parent post */
        else if ($action == 'new') {
            $parent_postgroup_id = $parent_id;

            $parent_info = array(
                'postgroup_id' => $parent_postgroup_id,
                'id' => null,
                'file_related' => false,
            );
        }
        else {
            $data['errors'][] = array(
                'text' => "Action invalide demandée, ceci est certainement un bug",
            );
            goto error;
        }

        if ($action != 'view') {
            /* check the form */
            $warns = check_form(Globals::$post_fields_check, $_POST,
                $data['warnings'], $fields_cleaned);


            /* if there is some warnings, don't display the post */
            if ($action == 'new' && count($data['warnings']) > 0) {
                $data['hide_posts'] = true;
                $data['parent_id'] = $parent_id;
                $data['action'] = 'new';
            }
        }

        /* if the post is correct then we can post the message */
        if ($action != 'view' && $warns == 0) {
            $q =
                'INSERT INTO posts (user_id, postgroup_id, post_id, title,
                                    content, file_related)
                VALUES (:uid, :pgid, :pid, :title, :content, :frel)';

            try {
                Doo::db()->beginTransaction();

                $r = Doo::db()->query($q, array(
                    ':uid'      =>  $this->user_id(),
                    ':pgid'     =>  $parent_info['postgroup_id'],
                    ':pid'      =>  $parent_info['id'],
                    ':title'    =>  $fields_cleaned['title'],
                    ':content'  =>  $fields_cleaned['content'],
                    ':frel'     =>  $parent_info['file_related'] + '',
                ));

                /* add points to the user because he is active */
                $q = 'UPDATE users SET score = score + :score WHERE id = :uid';
                $r = Doo::db()->query($q, array(':uid' => $this->user_id(),
                                                ':score' => Globals::$score_post));
            }
            catch (PDOException $e) {
                $data['errors'][] = array(
                    'text' => "Impossible de créer le post demandé au forum",
                );
                Doo::db()->rollback();
                goto error;
            }

            Doo::db()->commit();

            /* on success clean the POST fields */
            unset($_POST['title']);
            unset($_POST['content']);

            /* the new post is the parent, set according */
            if ($action == 'new') {
                $post_id = Doo::db()->lastInsertId('posts_id_seq');
            }

            $data['successes'][] = array(
                'text' => "Le post a bien été sauvé et ajouté",
            );
        }

        /* if all went well */
        if (!array_get($data, 'hide_posts', false)) {
            /* retrieve the post messages */
            $this->retrieve_post_data($post_id, $data);

            $data['section'] = "Topic: ". $data['title'];
            $data['parent_id'] = $post_id;
            $data['action'] = 'add';
            $data['all_rights'] = false;


            if (count($data['errors']) > 0) goto error;
        }
        /* if we don't have any post to display yet (new post failed case) */
        else {
            $data['section'] = "Nouveau fil";
            $data['parent_id'] = $parent_id;
            $data['action'] = 'new';
        }

        /* display an report form block for the main post */
        $data['block_report'] = array(
            'relid' => $parent_id,
            'reltype' => 2, /* posts */
        );

        error:
        $this->render('template', $data, $this);
    }

    /**
     * Displays all posts in a postgroup (may use ajax in the future!).
     */
    public function view_postgroup() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "forum-postgroup";
        $data['css'][] = "support";
        $data['css'][] = 'datatables';
        $data['js'][] = "jquery.dataTables.min";
        $data['js'][] = "tables_custom";
        $data['js'][] = 'jquery.balloon.min';
        $data['js'][] = 'balloon_extended';
        $data['js'][] = "forum-postgroup";
        $postgroup_id = $this->params['pgid'];

        /* check the postgroup exists and get its name */
        $q =
            'SELECT name
            FROM postgroups
            WHERE id = :pgid';

        $r = Doo::db()->query($q, array(':pgid' => $postgroup_id));
        if (!$post = $r->fetch()) {
            $data['errors'][] = array(
                'text' => "Ce forum n'existe pas",
            );
            goto error;
        }
        $data['title'] = $post['name'];
        $data['section'] = "Forum {$post['name']}";

        /* get all the posts in the current postgroup */
        $q =
            'SELECT p.id, p.title, p.user_id, p.file_related,
                    u.first_name, u.last_name, fg.id AS fgid,
                    extract(\'epoch\' from p.creation_time) as ctime
            FROM posts p
            JOIN users u ON u.id = user_id
            LEFT JOIN filegroups fg ON fg.post_id = p.id
            WHERE p.postgroup_id = :pgid AND p.post_id IS NULL
            ORDER BY p.creation_time DESC';

        $r = Doo::db()->query($q, array(':pgid' => $postgroup_id));

        $data['posts'] = array(':pgid' => $postgroup_id);
        while ($row = $r->fetch()) {
            $row['cdate'] = date(Globals::$date_day_format, $row['ctime']);
            $data['posts'][] = $row;
        }

        $data['postgroup_id'] = $postgroup_id;

        error:
        $this->render('template', $data, $this);
    }

    /**
     * Displays when the user wants to create a new post in a postgroup.
     */
    public function new_post() {
        array_append($data, self::$data_defaults);
        $data['section'] = "Nouveau fil";
        $data['pagename'] = "forum-postform";
        $data['js'][] = "form_hint";
        $postgroup_id = $this->params['pgid'];

        /* check the postgroup exists and get its name */
        $q = 'SELECT name, reltype FROM postgroups WHERE id = :pgid LIMIT 1';
        $r = Doo::db()->query($q, array(':pgid' => $postgroup_id));
        if (!$pg = $r->fetch()) {
            $data['errors'][] = array(
                'text' => "Ce forum n'existe pas",
            );
            goto error;
        }

        $data['reltype_desc'] = Globals::$postgroups_reltype[$pg['reltype']];
        $data['postgroup_name'] = $pg['name'];
        $data['parent_id'] = $postgroup_id;
        $data['action'] = 'new';

        error:
        $this->render('template', $data, $this);
    }

    /**
     * Displays when the user edits a post.
     */
    public function edit_post() {
        array_append($data, self::$data_defaults);
        $data['section'] = "Édition";
        $data['pagename'] = "forum-postform";
        $data['js'][] = "form_hint";
        $data['action'] = 'edit';
        $post_id = $this->params['pid'];
        $data['post_id'] = $post_id;

        /* get the post information */
        $q =
            'SELECT p.title, p.content, p.user_id, pg.reltype, pg.relid,
                    pp.id AS pid, pp.title AS ptitle
            FROM posts p
            JOIN posts pp ON pp.id = p.post_id OR pp.id = p.id
            JOIN postgroups pg ON pg.id = p.postgroup_id
            WHERE p.id = :pid';

        $r = Doo::db()->query($q, array(':pid' => $post_id));
        if (!$info = $r->fetch()) {
            $data['errors'][] = array(
                'text' => "Le post à éditer n'a pas été trouvé",
            );
            goto error;
        }

        /* check the user rights */
        $post_info = array(
            'user_id' => $info['user_id'],
        );
        $pg_info = array(
            'reltype' => $info['reltype'],
            'relid'   => $info['relid'],
        );
        if (!$this->user_rights_post($post_info, $pg_info)) {
            $data['errors'][] = array(
                'text' => "Vous n'avez pas les droits requis sur ce post",
            );
            goto error;
        }

        /* if the user has sent his form then validate and update the post */
        if ($_POST) {
            $warns = check_form(Globals::$post_fields_check, $_POST,
                $data['warnings'], $fields_cleaned);

            if ($warns > 0) goto error;

            $q = 'UPDATE posts SET title = :title, content = :content
                WHERE id = :pid';

            try {
                $r = Doo::db()->query($q, array(
                    ':title' => $fields_cleaned['title'],
                    ':content' => $fields_cleaned['content'],
                    ':pid' => $post_id,
                ));
            }
            catch (PDOException $e) {
                $data['errors'][] = array(
                    'text' => "Impossible de mettre à jour le post",
                    );
                goto error;
            }

            $data['successes'][] = array(
                'text' => "Le post a bien été mis à jour",
            );
            $data['block_links'][] = array(
                'text' => "Retour vers le topic: ". $info['ptitle'],
                'link' => '/support/post/view/'. $info['pid'],
            );
        }
        /* fills the form when we are in the GET mode */
        else {
            $_POST = array(
                'title' => $info['title'],
                'content' => $info['content'],
            );
        }

        error:
        $this->render('template', $data, $this);
    }

}

?>
