<?php

/**
 * Controller for the admin pages.
 */

class AdminController extends EPFHelpParentController {

    static private $data_defaults = array(
        'section' => 'Admin',
        'css' => array('admin', 'datatables' ),
        'js' => array('jquery.dataTables.min', 'tables_custom',
                  'jquery.balloon.min', 'balloon_extended', 'admin'),
    );

    /** We check that the user has at least one right, if not we deny the
     * access to the admin console. */
    public function beforeRun($ressource, $action) {
        parent::beforeRun($ressource, $action);

        /* most of the user won't ever have any right, so deny them */
        if (!$this->user_display_admin()) {
            $data = array(
                'js' => array(),
                'css' => array(),
                'pagename' => 'error',
                'section' => 'Erreur',
                'errors' => array(array('text' => "Accès refusé" )),
            );
            $this->render('template', $data, $this);
            exit();
        }
    }

    /** Returns a description of dataTable for files. */
    private function table_factory_files($rows) {
        return array(
            'html_id' => 'files',
            'headers' => array("Cours", "Groupe", "Uploader", "Date d'envoi",
                           "Note", "Validé"),
            'rows' => array_map(function ($v) {
                return array(
                    array('value' => $v['cname'],
                      'link' => "/support/course/{$v['cid']}"),
                    array( 'value' => $v['name'],
                      'link' => "/support/files/{$v['fgid']}/{$v['fid']}" ),
                    array('value' => "{$v['last_name']} {$v['first_name']}",
                      'link' => "/profile/edit/{$v['uploader_id']}"),
                    array('value' => time_ago($v['time']), 'raw' => $v['time']),
                    array('value' => $v['rate'] ),
                    array('value' => ($v['reviewed']) ? "Oui" : "Non"),
                );
            }, $rows),
        );
    }

    /** Returns a description of dataTable for posts. */
    private function table_factory_posts($rows) {
        return array(
            'html_id' => 'posts',
            'headers' => array("Forum", "Titre", "Auteur", "Date" ),
            'rows' => array_map(function ($v) {
                $parent_id = ($v['ppid']) ? $v['ppid'] : $v['id'];
                return array(
                    array('value' => $v['pgname'],
                      'link' => "/support/postgroup/{$v['pgid']}"),
                    array('value' => $v['title'],
                      'link' => "/support/post/view/{$parent_id}#{$v['id']}"),
                    array('value' => "{$v['last_name']} {$v['first_name']}",
                      'link' => "/profile/edit/{$v['user_id']}"),
                    array('value' => time_ago($v['time']), 'raw' => $v['time']),
                );
            }, $rows),
        );
    }

    /** Returns a description of a dataTable for users. */
    private function table_factory_users($rows) {
        return array(
            'html_id' => 'users',
            'headers' => array("Nom", "Prénom", "Score", "Professeur", "Activé?",
                           "Posts", "Fichiers", "Superadmin", "Autorisations"),
            'rows' => array_map(function ($v) {
                return array(
                    array('value' => $v['last_name'] ),
                    array('value' => $v['first_name'],
                      'link' => "/profile/edit/{$v['id']}"),
                    array('value' => $v['score'] ),
                    array('value' => $v['professor'] ? 'Oui' : 'Non'),
                    array('value' => $v['enabled'] ? 'Oui' : 'Non'),
                    array('value' => $v['posts']),
                    array('value' => $v['files']),
                    array('value' => $v['superadmin'] ? 'OUI' : 'n'),
                    array('value' => $v['rights']),
                );
            }, $rows),
        );
    }

    /** Returns a description of a dataTable for repots. */
    private function table_factory_reports($rows) {
        return array(
            'html_id' => 'reports',
            'headers' => array("Objet", "Type", "Category", "Rapporteur",
                           "Date", "Description", "Action"),
            'rows' => array_map(function ($v) {
                return array(
                    array('value' => $v['title'], 'link' => $v['link']),
                    array('value' => $v['type']),
                    array('value' => $v['cat']),
                    array('value' => "{$v['last_name']} {$v['first_name']}",
                      'link' => "/profile/edit/{$v['user_id']}"),
                    array('value' => time_ago($v['time']), 'raw' => $v['time']),
                    array('value' => $v['description'] ),
                    array('value' => '',
                      'buttons' => array(
                          array( 'icon' => 'remove', 'class' => 'remove',
                            'link' => '/ajax/report/delete/'. $v['id']),
                      ),
                    ),
                );
            }, $rows),
        );
    }

    /**
     * Retrieve a list of the $limit last files.
     *
     * Don't retrieve files without relation to the filter_courses list.
     */
    private function retrieve_files($filter_courses, $limit=0) {
        $rows = array();
        $params = array();

        $q =
            'SELECT f.id AS fid, f.uploader_id, fg.id AS fgid, fg.course_id,
                    fg.name, f.rating_base + f.rating_cache AS rate,
                    extract(\'epoch\' from f.upload_time) as time,
                    u.last_name, u.first_name, fg.reviewed,
                    c.id AS cid, c.name AS cname
            FROM files f
            JOIN filegroups fg ON fg.id = f.filegroup_id
            JOIN users u ON u.id = f.uploader_id
            JOIN courses c ON c.id = fg.course_id ';

        if ($filter_courses != null) {
            $q .= 'WHERE fg.course_id IN ('. implode(',', $filter_courses) .')';
        }

        $q .= 'ORDER BY f.id DESC ';

        if ($limit > 0) {
            $q .= 'LIMIT :limit ';
            $params[':limit'] = $limit;
        }

        $r = Doo::db()->query($q, $params);

        return $r->fetchall();
    }

    /**
     * Retrieve the list of the last posts.
     *
     * Don't retrieve files without relation to the filter_courses list.
     */
    private function retrieve_posts($filter_courses, $limit=0) {
        $rows = array();
        $params = array();

        $q =
            'SELECT p.id, p.user_id, p.title, p.content, pg.id AS pgid,
                    pg.name AS pgname, u.last_name, u.first_name,
                    extract(\'epoch\' from p.creation_time) as time,
                    pp.id AS ppid
            FROM posts p
            JOIN postgroups pg ON pg.id = p.postgroup_id
            JOIN users u ON u.id = p.user_id
            LEFT JOIN posts pp ON pp.id = p.post_id ';

        if ($filter_courses != null) {
            $q .= 'WHERE pg.reltype = 3
                AND pg.relid IN ('. implode(',', $filter_courses) .')';
        }

        $q .= 'ORDER BY p.id DESC ';

        if ($limit > 0) {
            $q .= 'LIMIT :limit ';
            $params[':limit'] = $limit;
        }

        $r = Doo::db()->query($q, $params);

        return $r->fetchall();
    }

    /**
     * Retrieve the list of the users by rights.
     */
    public function retrieve_users() {
        /* FIXME: WARNING: This query may be very slow with a lot of users */
        $q =
            'SELECT id, last_name, first_name, score, professor, enabled,
                    superadmin,
                (SELECT COUNT(*) FROM user_rights r WHERE r.user_id = u.id)
                    AS rights,
                (SELECT COUNT(*) FROM posts p WHERE p.user_id = u.id)
                    AS posts,
                (SELECT COUNT(*) FROM files f WHERE f.uploader_id = u.id)
                    AS files
            FROM USERS u
            ORDER BY superadmin DESC, rights DESC, score DESC';

        $r = Doo::db()->query($q);

        return $r->fetchall();
    }

    /**
     * Retrieve the list of reports done by users.
     */
    public function retrieve_reports() {
        $q =
            'SELECT p.id, p.user_id, p.reltype, p.relid, p.relcategory,
                    p.description, extract(\'epoch\' from time) as time,
                    u.last_name, u.first_name
            FROM problems p
            JOIN users u ON u.id = p.user_id
            ORDER BY id DESC';

        $r = Doo::db()->query($q);

        $rows = array();
        while ($row = $r->fetch()) {
            /* FIXME: WARNING: it queries the DB for each row, may be slow! */
            $info = $this->problems_object_info($row);
            array_append($row, $info);

            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * Returns the list of granted courses, or null if superadmin.
     */
    private function courses_grants() {
        /* if the user is superadmin, then he has all rights */
        if ($this->user_is_superadmin()) {
            return null;
        }
        else {
            return $this->user_granted_courses();
        }
    }

    /**
     * The page is an overview of the important objects.
     */
    public function start() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = 'admin';
        $data['section'] .= ' Overview';
        $user_granted_courses = $this->courses_grants();

        /* if the user has no rights, we hide these tables */
        if ($user_granted_courses === null
         || count($user_granted_courses) > 0) {
            $data['table_files'] = $this->table_factory_files(
                $this->retrieve_files($user_granted_courses, 100));

            $data['table_posts'] = $this->table_factory_posts(
                $this->retrieve_posts($user_granted_courses, 100));
        }

        /* fetch the list of reports done by users */
        $data['table_reports'] = $this->table_factory_reports(
            $this->retrieve_reports());

        if ($this->user_rights_users()) {
            /* fetch the list of users ordered by rights */
            $data['table_users'] = $this->table_factory_users(
                $this->retrieve_users());
        }

        $this->render('template', $data, $this);
    }

    /**
     * The page for a full view on files.
     */
    public function full_files() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = 'admin-files';
        $data['section'] .= ' files';
        $user_granted_courses = $this->courses_grants();

        if ($user_granted_courses === null
         || count($user_granted_courses) > 0)
        {
            $data['table_files'] = $this->table_factory_files(
                $this->retrieve_files($user_granted_courses));
        }

        $this->render('template', $data, $this);
    }

    /**
     * The page for a full view on posts.
     */
    public function full_posts() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = 'admin-posts';
        $data['section'] .= ' posts';
        $user_granted_courses = $this->courses_grants();

        if ($user_granted_courses === null
         || count($user_granted_courses) > 0)
        {
            $data['table_posts'] = $this->table_factory_posts(
                $this->retrieve_posts($user_granted_courses));
        }

        $this->render('template', $data, $this);
    }

}

?>
