<?php

/**
 * Used for all ajax requests done in javascript, there is no view for these
 * page because we only need to display some minimal value in plain text.
 */
class AjaxController extends EPFHelpParentController {

    static private $data_defaults = array(
        'css' => array(),
        'js' => array(),
    );

    /**
     * Function that display an error if not in ajax (noJS).
     */
    private function die_view($code, $descs, $opts = array()) {
        $ajax = array_get($opts, 'ajax', false);
        $dontdie = array_get($opts, 'dontdie', false);

        if (array_get_chain($ajax, 'useajax', $_GET, $_POST)) {
            /* if useajax is enabled, we return a JSON object */
            echo json_encode(array(
                'code' => $code,
                'errors' => $descs,
            ));
        }
        else {
            /* if no ajax, then show a pretty page with the messages */
            if ($code == 0) {
                $data['section'] = "Succès";
                $data['pagename'] = 'block-success';
                foreach ($descs as $v) {
                    $data['successes'][] = array(
                        'text' => $v,
                    );
                }
            }
            else {
                $data['section'] = "Échec";
                $data['pagename'] = 'error';
                foreach ($descs as $v) {
                    $data['errors'][] = array(
                        'text' => $v,
                    );
                }
            }

            $this->render('template',
                array_merge($data, self::$data_defaults), $this);
        }
        if (!$dontdie) {
            exit();
        }
    }

    /**
     * Add the course in the user favorite courses.
     */
    public function favcourses_add() {
        $course_id = $this->params['cid'];

        /* remove if the course was favorited */
        $q =
            'DELETE FROM user_favcourses WHERE user_id = :uid
                AND course_id = :cid AND priority < 0';
        $r = Doo::db()->query($q, array(':uid' => $this->user_id(),
                                        ':cid' => $course_id));

        /* then add the course to the favorite list */
        $q =
            'INSERT INTO user_favcourses (user_id, course_id, priority)
            VALUES (:uid, :cid, :pri)';

        try {
            $r = Doo::db()->query($q, array(':uid' => $this->user_id(),
                                        ':cid' => $course_id, ':pri' => 1));
        } catch (PDOException $e) {
            $r = false;
        }

        if ($r) {
            $this->die_view(0, array("Ajouté aux favoris"));
        }
        elseif (strpos($e->getMessage(), 'Unique violation') !== false) {
            $this->die_view(1, array("Déjà en favori"));
        }
        else {
            $this->die_view(2, array("Erreur inconnue"));
        }
    }

    /**
     * Remove the course in the user favorite courses.
     */
    public function favcourses_rem() {
        $course_id = $this->params['cid'];

        $q =
            'DELETE FROM user_favcourses
            WHERE user_id = :uid AND course_id = :cid';

        try {
            $r = Doo::db()->query($q, array(':uid' => $this->user_id(),
                                        ':cid' => $course_id ));
        } catch (PDOException $e) {
            $r = false;
        }

        if ($r && $r->rowCount() < 1) {
            $this->die_view(1, array("Pas encore en favori"));
        }
        else if ($r) {
            $this->die_view(0, array("Favori supprimé"));
        }
        else {
            $this->die_view(2, array("Erreur inconnue"));
        }
    }

    /**
     * Allow the user to remove a course from the left menu courses list
     */
    public function favcourses_black() {
        $course_id = $this->params['cid'];

        /* remove if the course was favorited */
        $q =
            'DELETE FROM user_favcourses WHERE user_id = :uid
                AND course_id = :cid AND priority > 0';
        $r = Doo::db()->query($q, array(':uid' => $this->user_id(),
                                    ':cid' => $course_id));

        /* then we add to the blacklist */
        $q =
            'INSERT INTO user_favcourses (user_id, course_id, priority)
            VALUES (:uid, :cid, :pri)';

        try {
            /* we black list the course by setting a negative priority */
            $r = Doo::db()->query($q, array(':uid' => $this->user_id(),
                                        ':cid' => $course_id, ':pri' => -1));
        } catch (PDOException $e) {
            $r = false;
        }

        if ($r) {
            $this->die_view(0, array("Enlevé des cours"));
        }
        elseif (strpos($e->getMessage(), 'Unique violation') !== false) {
            $this->die_view(1, array("Cours enlevé de la liste"));
        }
        else {
            $this->die_view(2, array( "Erreur inconnue"));
        }
    }

    /**
     * Allow the user to upvote or downfile a particular file.
     */
    public function file_vote() {
        $file_id = $this->params['fid'];
        $rate = $this->params['rate'];
        $user_id = $this->user_id();
        $votes = array('upvote' => +1, 'dwvote' => -1);

        $rate = array_get($votes, $rate, false);
        if ($rate === false) {
            $this->die_view(1, array("Valeur de vote invalide"));
        }

        // Log feature use
        Doo::loadModel('Telemetry');
        Telemetry::log('file/vote-attempt', array('file-id' => $file_id));

        Doo::db()->beginTransaction();

        try {
            /* insert the rating of the user in the list */
            $q =
                'INSERT INTO filerates (user_id, file_id, rate)
                VALUES (:uid, :fid, :rate)';

            $r = Doo::db()->query($q, array(':uid'  => $user_id,
                                        ':fid'  => $file_id,
                                        ':rate' => $rate));

            if ($r->rowCount() < 1) {
                goto error;
            }

            /* add the rate to the file */
            $q = 'UPDATE files SET rating_cache = rating_cache + :rate
                WHERE id = :fid';

            $r = Doo::db()->query($q, array(':rate' => $rate, ':fid' => $file_id));
            if ($r->rowCount() < 1) {
                goto error;
            }

            /* add a bonus for the user (because he is active) */
            $q = 'UPDATE users SET score = score + :score WHERE id = :uid';

            $r = Doo::db()->query($q, array(':uid' => $user_id,
                                        ':score' => Globals::$score_vote));
            if ($r->rowCount() < 1) {
                $data['errors'][] = array(
                    'text' => "Impossible de mettre à jour le score de "
                    ."l'utilisateur courant",
                );
                goto error;
            }

            /* add bonus to the uploader only if positive vote */
            if ($rate > 0) {
                $q = 'UPDATE users u
                    SET score = score + :score
                    FROM files f
                    WHERE u.id = f.uploader_id AND f.id = :fid';

                $r = Doo::db()->query($q, array(':score' => $rate,
                                            ':fid' => $file_id));
                if ($r->rowCount() < 1) {
                    $data['errors'][] = array(
                        'text' => "Impossible de mettre à jour le score de "
                        ."l'uploader",
                    );
                    goto error;
                }
            }
        }
        catch (PDOException $e) {
            if (strpos($e->getMessage(), 'Unique violation') !== false) {
                $this->die_view(2, array("Vous avez déjà voté"),
                    array('dontdie' => true));
            }
            else {
                $this->die_view(3, array("Une erreur inconnu s'est produite".$e->getMessage()),
                    array('dontdie' => true));
            }
            goto error;
        }

        Doo::db()->commit();
        $this->die_view(0, array("Vote enregistré"));

        error:
        Doo::db()->rollback();
        $this->die_view(PHP_INT_MAX, array());
    }

    /**
     * Used when a person report a problem with any object on the site.
     */
    public function report() {
        /* check the type of report is specified */
        $type_index = array_get($_POST, 'reltype', null);
        if ($type_index === null) {
            $this->die_view(1, array("Le type de problème est manquant"));
        }

        /* check the report type is correct */
        $type_entry = array_get(Globals::$problems_types, $type_index, null);
        if ($type_entry === null) {
            $this->die_view(4, array("Le type de rapport est incorrect"));
        }

        /* check the form */
        $fields_check = array(
            'reltype' => array(
                'name' => "Type d'objet", 'type' => 'numeric', 'min' => 0
            ),
            'relid' => array('name' => "Objet", 'type' => 'numeric', 'min' => 0),
            'cat' => array(
                'name' => "Type de problème", 'type' => 'numeric',
                'in_array' => array_keys(
                    array_get($type_entry, 'categories', array()))
            ),
            'description' => array(
                       'name' => "Description",
                       'type' => 'string', 'clean' => true
            ),
        );

        $errors_txt = array();
        check_form($fields_check, $_POST, $errors_txt, $values);

        if (count($errors_txt) > 0) {
            $this->die_view(3, array_map(function ($v) {
                return $v['text'];
            }, $errors_txt));
        }

        $relid = array_get($_POST, 'relid', null);
        $cat = array_get($_POST, 'cat', null);

        /* verify the report object exists with this id */
        $object_info = $this->problems_object_info(array(
            'reltype'     => $type_index,
            'relcategory' => $cat,
            'relid'       => $relid,
        ));
        if (!$object_info['found']) {
            $this->die_view(4, array("L'objet du rapport courant n'existe pas"));
        }

        Doo::db()->beginTransaction();

        try {
            $q =
                'INSERT INTO problems (user_id, reltype, relid, relcategory,
                    description)
                VALUES (:uid, :reltype, :relid, :pcat, :desc)';

            Doo::db()->query($q, array(
                    ':uid' => $this->user_id(),
                    ':reltype' => $type_index,
                    ':relid' => $relid,
                    ':pcat' => $cat,
                    ':desc' => $values['description']
                )
            );
        }
        catch (Exception $e) {
            Doo::db()->rollback();
            $this->die_view(2,
                array("Une erreur s'est produite dans la base de donnée "));
        }

        Doo::db()->commit();
        $this->die_view(0,
            array("Toutes les opérations se sont passées avec succès"));
    }

    /**
     * Used when an user wants to delete a post.
     */
    public function delete_post() {
        $post_id = $this->params['pid'];

        $q =
            'SELECT p.user_id, pg.reltype, pg.relid
            FROM posts p
            JOIN postgroups pg ON pg.id = p.postgroup_id
            WHERE p.id = :pid';

        $r = Doo::db()->query($q, array(':pid' => $post_id));
        if (!$info = $r->fetch()) {
            $this->die_view(1, array("Aucun post correspondant trouvé"));
        }

        $post_info = array(
            'user_id' => $info['user_id'],
        );
        $pg_info = array(
            'relid' => $info['relid'],
            'reltype' => $info['reltype'],
        );

        if (!$this->user_rights_post($post_info, $pg_info)) {
            $this->die_view(2, array("Vous n'avez pas les droits nécessaires"));
        }

        try {
            /* delete the post */
            $q = 'DELETE FROM posts WHERE id = :pid';
            Doo::db()->query($q, array(':pid' => $post_id));

            /* update the user score (remove the points due to the post) */
            $q = 'UPDATE users SET score = score - :score WHERE id = :uid';
            Doo::db()->query($q, array(':uid' => $this->user_id(),
                                   ':score' => Globals::$score_post));
        }
        catch (PDOException $e) {
            $this->die_view(3, array("Impossible de supprimer ce post"));
        }

        $this->die_view(0, array("Le post a bien été supprimé"));
    }

    /**
     * Used when an admin wants to delete a problem/report.
     */
    public function delete_problem() {
        $report_id = $this->params['rid'];

        $q =
            'SELECT id, user_id, relid, reltype, relcategory
            FROM problems WHERE id = :rid';

        $r = Doo::db()->query($q, array(':rid' => $report_id));
        if (!$report = $r->fetch()) {
            $this->die_view(1, array("Le rappport n'a pas été trouvé"));
        }

        if (!$this->user_rights_problems($report)) {
            $this->die_view(2, array("Vous n'avez pas les droits requis"));
        }

        $q = 'DELETE FROM problems WHERE id = :rid';
        try {
            Doo::db()->query($q, array(':rid' => $report_id));
        }
        catch (PDOException $e) {
            $this->die_view(3, array("Erreur interne à la base de donnée"));
        }

        $this->die_view(0, array("Rapport supprimé avec succès"));
    }

    /** Used when an admin wants to delete a right. */
    public function delete_right() {
        $right_id = $this->params['rid'];

        /* only superadmin can remove rights */
        if (!$this->user_is_superadmin()) {
            $this->die_view(1, array("Vous n'avez pas les droits requis"));
        }

        $q = 'DELETE FROM user_rights WHERE id = :rid';
        try {
            $r = Doo::db()->query($q, array(':rid' => $right_id));
        }
        catch (PDOException $e) {
            $this->die_view(3, array( "Erreur interne à la base de donnée"));
        }

        if ($r->rowCount() < 1) {
            $this->die_view(4, array("Le droit de supprimer n'a pas été trouvé"));
        }

        $this->die_view(0, array("Droit supprimé avec succès"));
    }

    /** Used for superadmins to delete a file. */
    public function delete_file() {
        $fid = $this->params['fid'];
        $warn = "";

        /* only superadmins can delete a file */
        if (!$this->user_is_superadmin()) {
            $this->die_view(2, array("Vous n'avez pas les droits requis"));
        }

        /* verify file existence and get file path */
        $q = 'SELECT uploader_id, filepath FROM files WHERE id = :fid';
        $r = Doo::db()->query($q, array(':fid' => $fid));
        if (!$file_info = $r->fetch()) {
            $this->die_view(1, array("Le fichier à supprimer est introuvable"));
        }

        Doo::db()->beginTransaction();
        Doo::db()->deferAll();

        /* proceed with the deletion */
        try {
            /* delete the file entry in the DB */
            $q = 'DELETE FROM files WHERE id = :fid';
            $r = Doo::db()->query($q, array(':fid' => $fid));

            /* remove user points due to file deletion */
            $q = 'UPDATE users SET score = score - :ds WHERE id = :uid';
            $r = Doo::db()->query($q, array(':uid' => $file_info['uploader_id'],
                                        ':ds' => Globals::$score_file));

            /* remove file ratings */
            $q = 'DELETE FROM filerates WHERE file_id = :fid';
            $r = Doo::db()->query($q , array(':fid' => $fid));
        }
        catch (PDOException $e) {
            Doo::db()->rollback();
            $this->die_view(2, array("La suppression dans la base de donnée a échoué"));
        }

        /* FIXME: If the file is alone in his filegroup, should we delete it? */
        Doo::db()->commit();

        /* delete the file on the hard drive, if it exists */
        $file_path = $file_info['filepath'];
        if (!file_exists($file_path)) {
            $warn .= " (Le fichier n'était pas présent sur le disque) ";
        }
        else {
            if (!unlink($file_path)) {
                $this->die_view(3, array("Le fichier a été supprimé dans la base "
                    ."de donnée mais pas sur le disque ({$file_path})"));
            }
        }

        $this->die_view(0, array("La suppression du fichier s'est bien déroulée"
            . $warn));
    }

    // Returns a JSON-formatted list of folders
    public function search_results() {
        $term = strtolower(rawurldecode($this->params['term']));

        $q = 'SELECT files.id AS id, filegroups.id AS fid, files.subtype AS subtype, filegroups.category AS category, filegroups.name AS name, courses.name AS course, EXTRACT(YEAR FROM files.creation_time) AS year
                FROM courses, filegroups, files
                WHERE filegroups.id = files.filegroup_id
                 AND courses.id = filegroups.course_id
                ORDER BY filegroups.name || courses.name || EXTRACT(YEAR from files.creation_time) <-> :term
                LIMIT 30';
        $r = Doo::db()->query($q, array(':term' => $term));

        $a = array();
        $item = array();
        while ($row = $r->fetch()) {
            $item = array();
            $item['name'] = $row['name'];
            $item['course'] = $row['course'];
            $item['category'] = Globals::$file_categories[$row['category']];
            if ($row['category'] == 2) $item['year'] = $row['year'];    // Show year for exams
            $item['subtype'] = ($row['category'] != 3 && $row['category'] != 4) ?
                Globals::$file_corrections[$row['subtype']] : '';       // Do not show Donnee/Corrige for Cours or Resume
            $item['href'] = sprintf('/support/files/%d/%d', $row['fid'], $row['id']);
            $a[] = $item;
        }

        $result = array();
        $result['term'] = $term;
        $result['results'] = $a;

        echo json_encode($result);
    }

    public function telemetry_search() {
        // Log search usage
        $term = strtolower(rawurldecode($this->params['term']));

        Doo::loadModel('Telemetry');
        Telemetry::log('search', array('term' => $term));
    }

    public function telemetry_search_access() {
        // Log search result access (index is zero-based position of accessed result in the list)
        $index = $this->params['index'];

        Doo::loadModel('Telemetry');
        Telemetry::log('search/access', array('index' => $index));
    }

    public function explorer_default_path() {
        $result = new stdClass();
        $user_id = $this->user_id();

        $q = 'SELECT section_id, degree FROM users WHERE id = :uid;';
        $r = Doo::db()->query($q, array(':uid' => $user_id));
        $row = $r->fetch();

        // FIXME Put semester names into Globals instead of here as a magical array
        // FIXME Change to spring semester after this semester's end
        //$names = array('', '-BA1', '-BA3', '-BA5', '-MA1', '-MA3');
	$names = array('', '-BA2', '-BA4', '-BA6', '-MA2', '-MA4');

        // (Restore '-' . $names[$row['degree']] after info is updated with Tequila)
        $result->path = 'f-' . $row['section_id'] . $names[$row['degree']];

        echo json_encode($result);
    }

    // or empty response on error (not even "null" or "{}")
    public function explorer_list() {

        // How long should this folder's file list be cached
        $cache_max_age = 5;

        // Let the browser cache this JSON response for some time
        header("Cache-Control: max-age=" . $cache_max_age);

        // Cache it on the server too
        Doo::cache('front')->get($cache_max_age);

        Doo::cache('front')->start();
        
        $path = $this->params['path'];
        
        // Names of CURRENT FOLDERS
        $levels = array('section', 'semester', 'course', 'category', 'filegroup', 'file');

        // Turn $raw_parts into nicely-indexed $parts array.
        $raw_parts = explode('-', $path);
        $parts = array();
        for ($i = 1; $i < count($raw_parts); ++$i) {
            if (!$raw_parts[$i]) {
                return;
            }
            $parts[$levels[$i-1]] = $raw_parts[$i];
        }

        // Add in some extra info
        $level = count($parts);
        $parts['path'] = $path;

        $result = new stdClass();
        $result->parts = $parts;

        // $result->list expects associative array with keys id, name, href, [group]
        switch ($level) {
            case 0 :
                $result->level = 0;
                $result->title = $this->explorer_title_sections($parts);
                $result->list  = $this->explorer_list_sections($parts);
                break;
            case 1 :
                $result->level = 1;
                $result->title = $this->explorer_title_semesters($parts);
                $result->list  = $this->explorer_list_semesters($parts);
                break;
            case 2 :
                $result->level = 2;
                $result->title = $this->explorer_title_courses($parts);
                $result->list  = $this->explorer_list_courses($parts);
                break;
            case 3 :
                $result->level = 3;
                $result->title = $this->explorer_title_categories($parts);
                $result->list  = $this->explorer_list_categories($parts);
                break;
            case 4 :
                $result->level = 4;
                $result->title = $this->explorer_title_filegroups($parts);
                $result->list  = $this->explorer_list_filegroups($parts);
                break;
            case 5 :
                $result->level = 5;
                $result->title = $this->explorer_title_files($parts);
                $result->list  = $this->explorer_list_files($parts);
                break;
            default :
                return;
                break;
        }

        echo json_encode($result);

        Doo::cache('front')->end();
    }

    private function explorer_title_sections($parts) {
        return 'PolyHelp';
    }

    private function explorer_list_sections($parts) {
        $q =   'SELECT faculties.short AS group, sections.id AS id, sections.name_fr AS name
                 FROM sections
                 INNER JOIN faculties
                  ON sections.faculty_id = faculties.id
                 WHERE sections.id > 0
                  AND faculties.id > 0
                 ORDER BY faculties.short DESC, sections.name_fr';
        $r = Doo::db()->query($q);

        // Count files
        $qc =  'SELECT COUNT(files.id) AS count, courses_sections.section_id AS id
                 FROM files, filegroups, courses_sections
                 WHERE filegroups.id = files.filegroup_id
                  AND courses_sections.course_id = filegroups.course_id
                 GROUP BY courses_sections.section_id';
        $rc = Doo::db()->query($qc);
        $ac = array();
        while ($row = $rc->fetch()) {
            $ac[$row['id']] = $row['count'];
        }
        
        $a = array();
        while ($row = $r->fetch()) {
            $row['href'] = '#f-'.$row['id'];
            $row['count'] = isset($ac[$row['id']]) ? $ac[$row['id']] : 0;
            $a[] = $row;
        }
        return $a;
    }

    private function explorer_title_semesters($parts) {
        $id = $parts['section'];
        $q = 'SELECT name_fr FROM sections WHERE id = :id';
        $r = Doo::db()->query($q, array(':id' => $id));

        $row = $r->fetch();
        return $row['name_fr'];
    }

    private function explorer_list_semesters($parts) {
        $names = array('BA1', 'BA2', 'BA3', 'BA4', 'BA5', 'BA6', 'MA1', 'MA2', 'MA3', 'MA4');
        $db_names = array('1', '11', '2', '21', '3', '31', '4', '41', '5', '51');
        
        $id = $parts['section'];
        $qc =  'SELECT COUNT(files.id) AS count, courses_sections.degree AS degree, courses_sections.semester AS semester
                 FROM files, filegroups, courses_sections
                 WHERE filegroups.id = files.filegroup_id
                  AND courses_sections.course_id = filegroups.course_id
                  AND courses_sections.section_id = :id
                 GROUP BY courses_sections.degree, courses_sections.semester';
        $rc = Doo::db()->query($qc, array(':id' => $id));

        // Store file counts per semester
        $ac = array();
        while ($row = $rc->fetch()) {
            $ac[$row['degree'] . $row['semester']] = $row['count'];
        }

        $a = array();
        for ($i = 0; $i < count($names); ++$i) {
            $a[] = array(
                'id' => $names[$i], 
                'name' => $names[$i], 
                'href' => '#' . $parts['path'] . '-' . $names[$i], 
                'count' => isset($ac[$db_names[$i]]) ? $ac[$db_names[$i]] : 0
            );
        }
        return $a;
    }

    private function explorer_title_courses($parts) {
        $id = $parts['section'];
        $q = 'SELECT short FROM sections WHERE id = :id';
        $r = Doo::db()->query($q, array(':id' => $id));

        $row = $r->fetch();
        return $row['short'] . '-' . $parts['semester'];
    }

    private function explorer_list_courses($parts) {
        $human = $parts['semester'];
        $degree = substr($human, 2, 1);
        $semester = ($degree % 2 == 1) ? 'f' : 't';
        $degree = ceil($degree / 2);
        if (substr($human, 0, 2) == 'MA') {
            $degree += 3;
        }
        $section = $parts['section'];

        $q =   'SELECT courses.id AS id, courses.name AS name
                 FROM courses
                 INNER JOIN courses_sections
                  ON courses.id = courses_sections.course_id
                 WHERE degree = :degree
                  AND semester = :semester
                  AND section_id = :section
                 ORDER BY courses.name';
        $r = Doo::db()->query($q, array(':degree' => $degree, ':semester' => $semester, ':section' => $section));

        $qc =  'SELECT COUNT(files.id) AS count, filegroups.course_id AS id
                 FROM files, filegroups, courses_sections
                 WHERE filegroups.id = files.filegroup_id
                  AND courses_sections.course_id = filegroups.course_id
                  AND courses_sections.degree = :degree
                  AND courses_sections.semester = :semester
                  AND courses_sections.section_id = :section
                 GROUP BY filegroups.course_id';
        $rc = Doo::db()->query($qc, array(':degree' => $degree, ':semester' => $semester, ':section' => $section));

        $ac = array();
        while ($row = $rc->fetch()) {
            $ac[$row['id']] = $row['count'];
        }

        $a = array();
        while ($row = $r->fetch()) {
            $row['href'] = '#' . $parts['path'] . '-' . $row['id'];
            $row['count'] = isset($ac[$row['id']]) ? $ac[$row['id']] : 0;
            $a[] = $row;
        }
        return $a;
    }

    private function explorer_title_categories($parts) {
        $id = $parts['course'];
        $q = 'SELECT name FROM courses WHERE id = :id';
        $r = Doo::db()->query($q, array(':id' => $id));

        $row = $r->fetch();
        return $row['name'];
    }

    private function explorer_list_categories($parts) {
        $ids = array('', 3, 2, 1, 4, 6, 7, 5);
        $names = array('', 'Cours', 'Examens', 'Exercices', 'Résumés', 'Travaux pratiques', 'Projets', 'Autres');
        $groups = array('', 'cours', 'examens', 'exercices', 'resumes', 'tps', 'projets', 'autres');

        $course = $parts['course'];
        $human = $parts['semester'];
        $degree = substr($human, 2, 1);
        $semester = ($degree % 2 == 1) ? 'f' : 't';
        $degree = ceil($degree / 2);
        if (substr($human, 0, 2) == 'MA') {
            $degree += 3;
        }
        $section = $parts['section'];
        $qc =  'SELECT COUNT(files.id) AS count, filegroups.category AS category
                 FROM files, filegroups, courses_sections
                 WHERE filegroups.id = files.filegroup_id
                  AND courses_sections.course_id = filegroups.course_id
                  AND courses_sections.course_id = :course
                  AND courses_sections.degree = :degree
                  AND courses_sections.semester = :semester
                  AND courses_sections.section_id = :section
                 GROUP BY filegroups.category';
        $rc = Doo::db()->query($qc, array(':course' => $course, ':degree' => $degree, ':semester' => $semester, ':section' => $section));

        $ac = array();
        while ($row = $rc->fetch()) {
            $ac[$row['category']] = $row['count'];
        }

        $a = array();
        for ($i = 1; $i < count($names); ++$i) {
            $a[] = array(
                'id' => $ids[$i], 
                'name' => $names[$i], 
                'href' => '#' . $parts['path'] . '-' . $ids[$i], 
                'group' => $groups[$i], 
                'count' => isset($ac[$ids[$i]]) ? $ac[$ids[$i]] : 0
            );
        }
        return $a;
    }

    private function explorer_title_filegroups($parts) {
        $names = array('', 'Exercices', 'Examens', 'Cours', 'Résumés', 'Autres', 'Travaux Pratiques', 'Projets');
        return $names[$parts['category']];
    }

    private function explorer_list_filegroups($parts) {
        $id = $parts['course'];
        $category = $parts['category'];
        $q =   'SELECT filegroups.id, filegroups.name, MIN(EXTRACT(YEAR FROM files.creation_time)) as year
                 FROM filegroups
                INNER JOIN files
                 ON filegroups.id = files.filegroup_id
                WHERE course_id = :id
                 AND category = :category
                GROUP BY filegroups.id
                 ORDER BY year DESC, name ASC';
        $r = Doo::db()->query($q, array(':id' => $id, ':category' => $category));
             
        $a = array();
        while ($row = $r->fetch()) {
            $row['href'] = '#' . $parts['path'] . '-' . $row['id'];
            $a[] = $row;
        }
        
        return $a;
    }

    private function explorer_title_files($parts) {
        $id = $parts['filegroup'];
        $q = 'SELECT name FROM filegroups WHERE id = :id';
        $r = Doo::db()->query($q, array(':id' => $id));

        $row = $r->fetch();
        return $row['name'];
    }

    private function explorer_list_files($parts) {
        $course = $parts['course'];
        $category = $parts['category'];
        $filegroup = $parts['filegroup'];
        $q =   'SELECT files.id AS id, filegroups.id as fid, files.subtype AS group
                 FROM files
                 INNER JOIN filegroups
                  ON files.filegroup_id = filegroups.id
                 WHERE filegroups.course_id = :course
                  AND filegroups.category = :category
                  AND filegroups.id = :filegroup
                 ORDER BY files.subtype';
        $r = Doo::db()->query($q, array(':course' => $course, ':category' => $category, ':filegroup' => $filegroup));

        $a = array();
        while ($row = $r->fetch()) {
            $row['name'] = Globals::$file_corrections[$row['group']];
            $row['group'] = 'subtype-' . $row['group'];
            $row['href'] = sprintf('/support/files/%d/%d', $row['fid'], $row['id']);
            $a[] = $row;
        }
        return $a;
    }

    public function telemetry_explorer_navigate() {
        // Log explorer navigation usage
        $path = $this->params['path'];

        Doo::loadModel('Telemetry');
        Telemetry::log('explorer/navigate', array('path' => $path));
    }



    public function telemetry_upload_panel() {
        Doo::loadModel('Telemetry');
        if (isset($this->params['fgid'])) {
            Telemetry::log('upload/panel', array('filegroup-id' => $this->params['fgid']));
        } else {
            Telemetry::log('upload/panel');
        }
    }


    
    
    public function user_info(){
        $user_id = isset($this->params['uid']) ? $this->params['uid'] : $this->user_id();
        /* check he exists and gets the user informations */
        $q =
            'SELECT u.section_id, u.faculty_id, avatar, first_name, last_name,
                    creation_time, professor, score, degree, superadmin,
                    f.name_fr AS fname, s.name_fr AS sname, u.id
            FROM users u
            JOIN faculties f ON f.id = u.faculty_id
            JOIN sections s ON s.id = u.section_id
            WHERE u.id = :uid';
        
        $r = Doo::db()->query($q, array( ':uid' => $user_id ));
        if (!$user_info = $r->fetch()) {
            $data['errors'][] = array(
                'text' => "Cet utilisateur n'a pas été trouvé",
            );
        }
        
        /* default avatar if necessary */
        if (strlen($user_info['avatar']) == 0) {
            $user_info['avatar'] = Globals::$user_avatar_default;
        }
        
        $data['user_info'] = $user_info;
        
        /* get the user contact informations */
        $q = 'SELECT name, value, description FROM user_addresses
            WHERE user_id = :uid';
        $r = Doo::db()->query($q, array( ':uid' => $user_id ));
        $data['addresses'] = $r->fetchall();
        
        /* add floating edit if the user has users edition rights */
        $data['edit'] = $this->user_rights_users($user_info);
        $data['user_id'] = $user_id;
        /* display the report block for the current file */
        $data['block_report'] = array(
            'relid' => $user_id,
            'reltype' => 3, /* users */
        );
        $data['score_stars'] = user_score($user_info['score']);

        echo json_encode($data);
    }
}
