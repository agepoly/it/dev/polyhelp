<?php

class BetaController extends EPFHelpParentController {

	static private $data_defaults = array(
        'section' => 'Beta Management',
        'css' => array('beta.default'),
        'js' => array('beta.default'),
        'errors' => array(),
    );

	public function showIndex(){
		
        array_append($data, self::$data_defaults);
        $data['pagename'] = "BetaDefaultView";

		if(!$this->hasAccess()){
			$data['error'] = $this->get_defaultError();
			goto error;
		}

		error:
        $this->render('template', $data, $this);
	}

	public function searchBeta(){
        array_append($data, self::$data_defaults);

		// if(!$this->hasAccess()){
		// 	$data['error'] = $this->get_defaultError();
		// 	goto error;
		// }

		$name = $this->params['name'];
		Doo::loadModel('Beta');
		$beta = new Beta();
		$data['name'] = $name;
		$req = array(
			'where'=>'name=?',
			'param'=>array($name));

		$data['betaList'] = $beta->find($req);

		error:
		$this->toJSON($data, true);
	}

	public function searchBetaUser(){
        array_append($data, self::$data_defaults);

		// if(!$this->hasAccess()){
		// 	$data['error'] = $this->get_defaultError();
		// 	goto error;
		// }

		$params = $this->params['beta'];

		Doo::loadModel('Beta');
		$beta = new Beta($params);

		$data['betaUserList'] = $beta->find_users();

		error:
		$this->toJSON($data, true);
	}

	public function showBetaEditorView(){
		array_append($data, self::$data_defaults);
        if(!$this->hasAccess()){
			$data['error'] = $this->get_defaultError();
			goto error;
		}

		$params = $this->param['element'];
		Doo::loadModel('Beta');
		$beta = new Beta($params);
		$data['element'] = $beta;

		error:
		$this->render('BetaEditor', $data, true);
	}

	public function insertBeta(){
		if(!$this->hasAccess()){
			$data['error'] = $this->get_defaultError();
			goto error;
		}

		$beta_params = $this->params['beta'];
		Doo::loadModel('Beta');
		$beta = new Beta($beta_params);

		$id = $beta->insert();
		$data['last_insert_beta_id'] = $id;

		error:
		$this->toJSON($data);
	}

	public function showBetaUserEditorView(){
		array_append($data, self::$data_defaults);
        if(!$this->hasAccess()){
			$data['error'] = $this->get_defaultError();
			goto error;
		}

		$data['beta-id'] = $this->params['beta-id'];
		$params = $this->params['element'];
		Doo::loadModel('UserBeta');
		$betaUser = new UserBeta(params);

		$data['element'] = $betaUser;

		error:
		$this->render('BetaUserEditor', $data, true);
	}

	public function insertBetaUser(){
		if(!$this->hasAccess()){
			$data['error'] = $this->get_defaultError();
			goto error;
		}

		$betaUser_params = $this->params['betaUser'];
		Doo::loadModel('UserBeta');
		$betaUser = new UserBeta($betaUser_params);

		$id = $betaUser->insert();
		$data['last_insert_beta_user_id'] = $id;

		error:
		$this->toJSON($data);
	}

	private function has_access(){
		return $this->user_is_superadmin();
	}

	private function get_defaultError(){
		$error = array(
				'text'=>
					'Vous n\'avez pas les accréditations nécessaire pour accéder à cette page.'
			);

		return $error;
	}

	private function getMenuArray(){

		$menu = array(
			'Edit beta List'=>'/admin/beta/edit',
			'Edit beta-users List'=>'/admin/betaUser/edit'
			);

		return $menu;
	}
}

?>