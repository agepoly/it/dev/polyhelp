<?php

/**
 * This class overload some default functions, ensure that the user is logged,
 * we load the requiered informations for menus before our custom Controller are
 * executed.
 *
 * The template view can rely on these informations to be available.
 */
class EPFHelpParentController extends DooController {

    /** The current user id (filled by ensure_login) */
    protected $_user_id = null;

    /** The user favorite courses in the user_favcourses DB. */
    public $user_favcourses = Array();

    /** Boolean hash map of favorite courses (check). */
    public $user_favcourses_hash = Array();

    /** List of the students courses (according to his degree and semester */
    public $user_courses = Array();

    /** The user non-favorite (blacklist) of courses as a hashmap */
    public $user_antifavcouses_hash = Array();

    /** The user informations in the users DB. */
    public $user_info = Array();

    /** The user rights. */
    public $user_rights = Array();

    /** The courses that this user teaches. */
    private $_user_teaching_courses = Array();
    private $_user_teaching_courses_fetched = false;

    /** The list of sections (not including the null section). */
    private $_sections = null;

    /** All the courses given for the sections. */
    private $_section_courses = Array();
    private $_section_courses_fetched = Array();

    /** The connected users. */
    public $users_connected = Array();

    /**
     * Executed before the child Controller routed method.
     *
     * Entry point of the site: ensure some security, access require a login or
     * a form is displayed, check the validity of the user, retrieve mandatory
     * information about him for the pages.
     */
    public function beforeRun($ressource, $action) {
        $this->ensure_login();

        $this->retrieve_user_info();
        $this->check_validity();
        $this->update_presence();

        $this->retrieve_user_connected();
        $this->retrieve_user_favcourses();
        $this->retrieve_user_courses();
        $this->retrieve_user_rights();
    }


    /** Ensure that users are logged or are loggin now. */
    // In the end, sets $this->_user_id to the current user's ID
    private function ensure_login() {
        $data = Array();

        Doo::loadModel('Auth');

        // If we're already logged in with a valid session_hash, we're done
        if ($this->_user_id = Auth::check()) {
            return;
        }

        // We need to do some internal "routing" to do special things on /login pages.
        // Get which route we're on right now
        Doo::loadCore('uri/DooUriRouter');
        $current_route_array = (new DooUriRouter)->execute(Doo::app()->route, Doo::conf()->SUBFOLDER);
        $current_route = $current_route_array[0] . '/' . $current_route_array[1];

        if ($current_route == 'EPFHelpParentController/login') {
            $this->login();
        } else if ($current_route == 'EPFHelpParentController/login_fetch') {
            $this->login_fetch();
        } else if ($current_route == 'EPFHelpParentController/logout') {
            $this->logout();
        } else {
            // Not logged in

            // $this->render('login_form', $data, $this);
            $this->login();
            // Our sticker logo isn't pretty enough, so skip the splash screen
            // (go to tequila right away)
        }
        exit;
    }

    /** 
     *  Login page: send off to Tequila.
     */
    public function login(){

        $tequila_url = Auth::to_tequila();

        if ($tequila_url) {
            header('Location: ' . $tequila_url);
        } else {
            // FIXME Better errors (throw exception?)
            die('Error: Could not connect to Tequila.');
        }
        exit;
    }

    /**
     *  Just came back from Tequila login; fetch user info and done
     */
    public function login_fetch() {

        $info = Auth::from_tequila();
        if (is_null($info)) {
            // FIXME Better error message
            die('Error: you could not be authenticated after Tequila.');
        }
        
        // Do some logging
        Doo::loadModel('Telemetry');
        $t0 = microtime(true);
        Telemetry::start($info['gaspar'], $info['affi']);
        Telemetry::log('login');
        $t1 = microtime(true);
        Telemetry::log('login/telemetry', array('benchmark' => ($t1 - $t0)));
        unset($t0, $t1);

        // Redirect to post-login page (that is, homepage)
        header('Location: /');
        exit;
    }
    
    /**
     *  Log the user out.
     */
    public function logout(){
        setcookie(Globals::$cookie_name, "", time() - 3600);

        Doo::loadModel('Telemetry');
        Telemetry::log('logout');
        Telemetry::stop();

        header('Location: /');
        exit;
    }

    /** Fill $this->user_favcourses and $this->user_favcourses_hash. */
    private function retrieve_user_favcourses() {
        $q =
            'SELECT uf.course_id, uf.priority, c.name
            FROM user_favcourses uf
            JOIN courses c ON c.id = uf.course_id
            WHERE user_id = :uid
            ORDER BY priority DESC';

        $r = Doo::db()->query($q, Array(':uid' => $this->user_id()));

        while ($row = $r->fetch()) {
            if ($row['priority'] < 0) {
                /* blacklisted courses */
                $this->user_antifavcouses_hash[$row['course_id']] = true;
            }
            else {
                /* white/favorited courses */
                $this->user_favcourses[] = $row;
                $this->user_favcourses_hash[$row['course_id']] = true;
            }
        }
    }

    /** Fill $this->user_courses */
    private function retrieve_user_courses() {
        $q =
            'SELECT c.id, c.name
            FROM courses c
            JOIN courses_sections cs ON cs.course_id = c.id
            WHERE cs.section_id = :sid AND cs.degree = :udegree
                AND cs.semester = :sem
            ORDER BY c.name';

        $r = Doo::db()->query($q, Array(':udegree' => $this->user_info['degree'],
            ':sid' => $this->user_info['section_id'],
            ':sem' => Globals::current_semester()
        ));

        while ($row = $r->fetch()) {
            /* Add the course if it is *not* in the blacklist and not as fav */
            if (!array_get($this->user_antifavcouses_hash, $row['id'], false)
                && !array_get($this->user_favcourses_hash, $row['id'], false))
            {
                $this->user_courses[] = $row;
            }
        }
    }

    /** Fill the $this->user_rights. */
    private function retrieve_user_rights() {
        $q = 'SELECT reltype, relid FROM user_rights WHERE user_id = :uid';
        $r = Doo::db()->query($q, Array(':uid' => $this->user_id()));
        $this->user_rights = $r->fetchall();
    }

    /** Fill $this->user_info. */
    private function retrieve_user_info() {
        $q =
            'SELECT section_id, faculty_id, degree, first_name, last_name, 
                    professor, superadmin, avatar, enabled
            FROM users
            WHERE id = :uid';

        $r = Doo::db()->query($q, Array(':uid' => $this->user_id()));

        $this->user_info = $r->fetch();
    }

    /** Check the user is enabled and all that. */
    private function check_validity() {
        if (!$this->user_info['enabled']) {
            /* display the login form with an error */
            $data['errors'][] = Array(
                'text' => "Votre compte n'est pas encore activé, veuillez "
                ."réessayer plus tard",
            );
            $this->render('login_form', $data, $this);
            exit();
        }
    }

    /**
     * Retrieve the posts in the data variable (common code).
     */
    protected function retrieve_post_data($post_id, &$data) {
        $q =
            'SELECT p.user_id, p.postgroup_id, p.title, p.content, p.post_id,
                    u.first_name, u.last_name, u.avatar, u.score, p.id,
                    extract(\'epoch\' from p.creation_time) as ctime
            FROM posts p
            JOIN users u ON u.id = p.user_id
            WHERE p.id = :pid OR p.post_id = :pid
            ORDER BY p.creation_time ASC';

        $r = Doo::db()->query($q, Array(':pid' => $post_id));

        while ($row = $r->fetch()) {
            $row['cdate'] = date(Globals::$date_seconds_format, $row['ctime']);
            if ($row['avatar'] == "") {
                $row['avatar'] = Globals::$user_avatar_default;
            }
            $data['posts'][] = $row;

            if ($row['post_id'] === null) {
                $data['title'] = $row['title'];
                $parent_post = $row;
            }
        }
        if ($r->rowCount() < 1) {
            $data['errors'][] = Array('text' => "Ce post n'a pas été trouvé", );
            return;
        }
        if (!isset($parent_post)) {
            $data['errors'][] = Array('text' => "Ce post semble buggé, il n'a pas de parent", );
            return;
        }

        /* retrieve the parent forum, course or section to make useful links */
        $q = 'SELECT name, reltype, relid FROM postgroups WHERE id = :pgid';
        $r = Doo::db()->query($q, Array(':pgid' => $parent_post['postgroup_id']));
        $pg = $r->fetch();
        $data['postgroup_id'] = $parent_post['postgroup_id'];
        $data['postgroup_name'] = $pg['name'];

        /* if the forum is associated with a course/section, retrieve it */
        if ($pg['reltype'] == 3) {
            $q = 'SELECT id, name FROM courses WHERE id = :cid';
            $r = Doo::db()->query($q, Array(':cid' => $pg['relid']));

            if ($course = $r->fetch()) { # FIXME: database may be inconsistent
                $data['course_id'] = $course['id'];
                $data['course_name'] = $course['name'];
            }
        }
        elseif ($pg['reltype'] == 2) {
            $q = 'SELECT id, name_fr FROM sections WHERE id = :sid';
            $r = Doo::db()->query($q, Array(':sid' => $pg['relid']));

            if ($section = $r->fetch()) { # FIXME: database may be inconsistent
                $data['section_id'] = $section['id'];
                $data['section_name'] = $section['name_fr'];
            }
        }

        /* add edit/delete rights flag if rights are granted */
        foreach ($data['posts'] as &$p) {
            $p['rights'] = $this->user_rights_post($p, $pg);
        }
    }

    /**
     * Get the user id (the variable is filled by ensure_login).
     */
    protected function user_id() {
        if ($this->_user_id === null)
            die("Une erreur est survenue, vous n'êtes pas logué");

        return $this->_user_id;
    }

    /**
     * Returns whether the user is a superadmin.
     */
    protected function user_is_superadmin() {
        return $this->user_info['superadmin'];
    }

    /**
     * Returns whether the user is a teacher.
     */
    protected function user_is_professor() {
        return $this->user_info['professor'];
    }

    /**
     * Returns the user's section.
     */
    protected function user_section() {
        return $this->user_info['section_id'];
    }

    /**
     * Returns the user's degree (year).
     */
    protected function user_degree() {
        return $this->user_info['degree'];
    }

    /**
     * Returns and retrieve if necessary $_user_teaching_courses.
     */
    protected function user_teaching_courses() {
        if (!$this->_user_teaching_courses_fetched) {
            $q =
                'SELECT c.id AS course_id, c.name, c.postgroup_id,
                        string_agg(s.short, \'||\') AS sections_raw
                FROM course_prof cp
                JOIN courses c ON c.id = cp.course_id
                JOIN courses_sections cs ON cs.course_id = cp.course_id
                JOIN sections s ON s.id = cs.section_id
                WHERE user_id = :uid
                GROUP BY c.id';

            $r = Doo::db()->query($q, Array(':uid' => $this->user_id()));

            $courses = Array();
            while ($row = $r->fetch()) {
                $row['sections'] = explode('||', $row['sections_raw']);
                $courses[] = $row;
            }
            $this->_user_teaching_courses = $courses;
            $this->_user_teaching_courses_fetched = true;
        }

        return $this->_user_teaching_courses;
    }

    /**
     * Returns the ids of the courses in $_user_teaching_courses.
     */
    protected function user_teaching_courses_ids() {
        return array_map(function ($v) {
            return $v['course_id'];
        }, $this->user_teaching_courses());
    }

    /**
     * Returns and retrieve if necessary $_sections.
     *
     * if $invalids is true then returns the null/stub sections too.
     */
    protected function sections($invalids=false) {
        if ($this->_sections === null) {
            /* we skip the section id 0 because it is none */
            $q = 'SELECT id, name_fr AS name FROM sections';
            $r = Doo::db()->query($q);

            $this->_sections = $r->fetchall();
        }

        if (!$invalids) {
            return array_filter($this->_sections, function ($v) {
                return $v['id'] > 0;
            });
        } else {
            return $this->_sections;
        }
    }

    /**
     * Returns and retrieve if necessary $_section_courses.
     */
    protected function section_courses($section) {
        if (!array_get($this->_section_courses_fetched, $section, false)) {
            $q = 'SELECT c.id FROM courses c
                JOIN courses_sections cs ON cs.course_id = c.id
                AND cs.section_id = :sid';
            $r = Doo::db()->query($q, Array(':sid' => $section));

            $this->_section_courses[$section] = array_map(function ($v) {
                return $v['id'];
            }, $r->fetchall());

            $this->_section_courses_fetched[$section] = true;
        }

        return $this->_section_courses[$section];
    }

    /**
     * Returns the list of courses which are granted to the user.
     */
    protected function user_granted_courses() {
        return array_flatmap(
            array_filter($this->user_rights,
                function ($v) {
                    return $v['reltype'] == 0;
                }),
            function ($v) {
                return $this->section_courses($v['relid']);
            }
        );
    }

    /**
     * Whether the user has rights on the given section.
     */
    private function user_rights_section($section_id) {
        if (array_exists($this->user_rights,
            function ($v) use($section_id) {
                return $v['reltype'] == 0 && $v['relid'] == $section_id;
            }))
        {
            return true;
        }

        return false;
    }

    /**
     * Whether the user has rights on the given course.
     */
    private function user_rights_course($course_id) {
        /* grants if the user has rights on the course of this filegroup */
        if (in_array($course_id, array_map(
                function ($v) {
                    return $v['relid'];
                },
                array_filter($this->user_rights,
                function ($v) {
                    return $v['reltype'] == 1;
                }))))
        {
            return true;
        }

        /* grants if the user has rights on his own courses and teaches the
         * course of this filegroup */
        if (array_exists($this->user_rights,
            function ($v) {
                return $v['reltype'] == 3;
            }) && in_array($course_id, $this->user_teaching_courses_ids()))
        {
            return true;
        }

        /* grants if the user has rights on the given section */
        if (in_array($course_id, $this->user_granted_courses())) {
            return true;
        }

        return false;
    }

    /**
     * Whether the user has rights to edit the filegroup.
     */
    protected function user_rights_filegroup($filegroup_info) {
        $user_id = $this->user_id();

        if ($this->user_is_superadmin())                return true;
        if ($filegroup_info['uploader_id'] == $user_id) return true;
        if ($this->user_rights_course($filegroup_info['course_id'])) {
            return true;
        }

        return false;
    }

    /**
     * Whether the user has rights to edit the file.
     */
    protected function user_rights_file($file_info, $filegroup_info) {
        $user_id = $this->user_id();

        if ($this->user_is_superadmin())              return true;
        if ($file_info['uploader_id'] == $user_id)    return true;
        if ($this->user_rights_course($filegroup_info['course_id'])) {
            return true;
        }

        return false;
    }

    /**
     * Whether the user has rights to edit/delete the post.
     */
    protected function user_rights_post($post_info, $pg_info) {
        /* grants if it's a superadmin */
        if ($this->user_is_superadmin()) {
            return true;
        }

        /* the author has rights on his posts */
        if ($post_info['user_id'] == $this->user_id()) {
            return true;
        }

        /* if the postgroup is a course forum then grants the user rights if he
         * has rights on the course itself. */
        if ($pg_info['reltype'] == 3) {
            if ($this->user_rights_course($pg_info['relid'])) {
                return true;
            }
        }
        /* if the postgroup is a section forum then grants the user rights if he
         * has rights on the section itself. */
        else if ($pg_info['reltype'] == 2) {
            if ($this->user_rights_section($pg_info['relid'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Whether the user has rights to edit/delete a user.
     *
     * if target_user_info is null, rights on view of the user list.
     */
    protected function user_rights_users($target_user_info=null) {
        /* grants if it's a superadmin */
        if ($this->user_is_superadmin()) {
            return true;
        }

        if ($target_user_info != null) {
            /* if the user is itself grants */
            if ($target_user_info['id'] == $this->user_id()) {
                return true;
            }

            /* if the target user is a superadmin, deny expect if the current
                * user is himself a superadmin! */
            if ($target_user_info['superadmin']) {
                return false;
            }
        }

        /* access of type 'users' (reltype = 2) */
        if (array_exists($this->user_rights,
            function ($v) {
                return $v['reltype'] == 2;
            }))
        {
            return true;
        }

        return false;
    }

    /**
     * Whether the user has rights to delete this problem/report.
     */
    protected function user_rights_problems($report_info) {
        /* superadmins can delete reports */
        if ($this->user_is_superadmin()) {
            return true;
        }

        return false;
    }

    /**
     * Returns info on an object based on a right.
     */
    protected function rights_object_info($right_info) {
        $id = $right_info['relid'];
        $type = Globals::$user_rights_type[$right_info['reltype']];
        $ret = Array(
            'type' => $type['name'],
            'require_id' => $type['require_id'],
            'link' => null,
            'title' => '(erreur)',
            'found' => false,
        );

        # section
        if ($right_info['reltype'] == 0) {
            $q = 'SELECT id, name_fr AS name FROM sections WHERE id = :sid';
            $r = Doo::db()->query($q, Array(':sid' => $id));

            if ($row = $r->fetch()) {
                array_append($ret, Array(
                    'link' => sprintf($type['link'], $row['id']),
                    'title' => $row['name'],
                    'found' => true,
                ));
            }
        }
        # course
       else if ($right_info['reltype'] == 1) {
           $q = 'SELECT id, name FROM courses WHERE id = :cid';
           $r = Doo::db()->query($q, Array(':cid' => $id));

           if ($row = $r->fetch()) {
               array_append($ret, Array(
                   'link' => sprintf($type['link'], $row['id']),
                   'title' => $row['name'],
                   'found' => true,
               ));
           }
        }
        # users
        else if ($right_info['reltype'] == 2) {
            array_append($ret, Array(
                'title' => '(tous)',
                'found' => true,
            ));
        }
        # owncourse
        else if ($right_info['reltype'] == 3) {
            array_append($ret, Array(
                'title' => '(tous)',
                'found' => true,
            ));
        }

        return $ret;
    }

    /**
     * Returns the list of possible objects for each right type.
     */
    protected function rights_objects_list() {
        /* fetch for sections */
        $sections = $this->sections();

        /* fetch for courses */
        $q =
            'SELECT c.id, c.name, string_agg(s.short, \'||\') AS sections
            FROM courses c
            JOIN courses_sections cs ON cs.course_id = c.id
            JOIN sections s ON s.id = cs.section_id
            WHERE c.id > 0
            GROUP BY c.id';

        $r = Doo::db()->query($q);
        $courses = Array();
        while ($row = $r->fetch()) {
            $course_sections = implode(' ', explode('||', $row['sections']));
            $row['raw_name'] = $row['name'];
            $row['name'] = "{$row['name']} ({$course_sections})";

            $courses[] = $row;
        }

        /* we will sort according to the name */
        function sorter(&$t) {
            uasort($t, function ($a, $b) {
                return strcmp($a['name'], $b['name']);
            });
            return $t;
        };

        /* no fetch needed for users and owncourse, so that's all */
        return Array(
            0 => sorter($sections),
            1 => sorter($courses),
            2 => null,
            3 => null,
        );
    }

    /**
     * Returns info on an object based on a report/problem.
     */
    protected function problems_object_info($report_info) {
        $id = $report_info['relid'];
        $type = Globals::$problems_types[$report_info['reltype']];
        $ret = Array(
            'type' => $type['name'],
            'cat'  => $type['categories'][$report_info['relcategory']],
            'link' => null,
            'title' => '(erreur)',
            'found' => false,
        );

        # file
        if ($report_info['reltype'] == 0) {
            $q =
                'SELECT fg.id AS fgid, fg.name,
                        f.rating_base + f.rating_cache AS rating
                FROM files f
                JOIN filegroups fg ON fg.id = f.filegroup_id
                WHERE f.id = :fid';

            $r = Doo::db()->query($q, Array(':fid' => $id));
            if ($row = $r->fetch()) {
                array_append($ret, Array(
                    'link' => sprintf($type['link'], $row['fgid'], $id),
                    'title' => "{$row['name']} ({$row['rating']})",
                    'found' => true,
                ));
            }
        }

        # filegroup
        else if ($report_info['reltype'] == 1) {
            $q = 'SELECT name FROM filegroups WHERE id = :fgid';

            $r = Doo::db()->query($q, Array(':fgid' => $id));
            if ($row = $r->fetch()) {
                array_append($ret, Array(
                    'link' => sprintf($type['link'], $id),
                    'title' => $row['name'],
                    'found' => true,
                ));
            }
        }

        # post
        else if ($report_info['reltype'] == 2) {
            $q = 'SELECT p.title, pp.id AS ppid
                FROM posts p
                LEFT JOIN posts pp ON pp.id = p.post_id
                WHERE p.id = :pid';

            $r = Doo::db()->query($q, Array(':pid' => $id));
            if ($row = $r->fetch()) {
                array_append($ret, Array(
                    'link' => sprintf($type['link'],
                        array_get($row, 'ppid', $id), $id),
                    'title' => $row['title'],
                    'found' => true,
                ));
            }
        }

        # users
        else if ($report_info['reltype'] == 3) {
            $q = 'SELECT last_name, first_name, score
                FROM users WHERE id = :uid';

            $r = Doo::db()->query($q, Array(':uid' => $id));
            if ($row = $r->fetch()) {
                array_append($ret, Array(
                    'link' => sprintf($type['link'], $id),
                    'title' => "{$row['last_name']} {$row['first_name']} "
                        ."({$row['score']})",
                    'found' => true,
                ));
            }
        }

        return $ret;
    }


    /** Returns whether we display the admin section or not. */
    public function user_display_admin() {
        return count($this->user_rights()) > 0 || $this->user_is_superadmin();
    }

    /** Check the teacher has the right to access the given course. */
    protected function user_rights_teacher_course($course_id) {
        return in_array($course_id, $this->user_teaching_courses_ids())
            || in_array($course_id, $this->user_granted_courses());
    }

    /** Update the current user presence (he is online). */
    private function update_presence() {
        $q = 'UPDATE users SET connected = now() WHERE id = :uid';
        Doo::db()->query($q, Array(':uid' => $this->user_id()));
    }

    /** Get the list of connected users. */
    protected function retrieve_user_connected() {
        $q = 'SELECT id, last_name, first_name
            FROM users
            WHERE connected >= now() + :delta';

        $r = Doo::db()->query($q, Array(
            ':delta' => Globals::$presence_timeout . ' seconds ago',
            ));
        $this->users_connected = $r->fetchall();
    }

}
