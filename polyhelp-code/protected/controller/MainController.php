<?php

/**
 * Controller for the home page.
 */

class MainController extends EPFHelpParentController {

    static private $data_defaults = Array(
        'section' => 'Accueil',
        'css' => Array('main'),
        'js' => Array(),
    );


    /**
     * Home page
     */
    public function index() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "main";
       
        $this->render('template', $data, $this);
    }


    /**
     * LDAP tester (activate-deactivate in routes.conf.php)
     */
    public function lt() {
        $gaspar = array_get($this->params, 'gaspar', '!!!');

        $ldap = new LDAP();
        $ldap->main_connect();
        echo '<pre>';
        print_r($ldap->user_info($gaspar));
    }

}

?>
