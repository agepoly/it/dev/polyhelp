<?php

/**
 * Controller for the profile page (by user).
 */

class ProfileController extends EPFHelpParentController {

    static private $data_defaults = array(
        'section' => 'Profile',
        'css' => array('profile'),
        'js' => array(),
        'errors' => array(),
    );

    /**
     * Displayed when a user is clicked, display his profile.
     */
    public function user() {
        array_append($data, self::$data_defaults);
        $data['js'][] = 'block_report';
        $data['js'][] = 'profile';
        $data['pagename'] = "profile";
        $user_id = $this->params['uid'];

        /* check he exists and gets the user informations */
        $q =
            'SELECT u.section_id, u.faculty_id, avatar, first_name, last_name,
                    creation_time, professor, score, degree, superadmin,
                    f.name_fr AS fname, s.name_fr AS sname, u.id
            FROM users u
            JOIN faculties f ON f.id = u.faculty_id
            JOIN sections s ON s.id = u.section_id
            WHERE u.id = :uid';

        $r = Doo::db()->query($q, array( ':uid' => $user_id ));
        if (!$user_info = $r->fetch()) {
            $data['errors'][] = array(
                'text' => "Cet utilisateur n'a pas été trouvé",
            );
            goto error;
        }

        /* default avatar if necessary */
        if (strlen($user_info['avatar']) == 0) {
            $user_info['avatar'] = Globals::$user_avatar_default;
        }

        $data['user_info'] = $user_info;
        $data['section'] .=
            " de {$user_info['last_name']} {$user_info['first_name']}";

        /* get the user contact informations */
        $q = 'SELECT name, value, description FROM user_addresses
            WHERE user_id = :uid';
        $r = Doo::db()->query($q, array( ':uid' => $user_id ));
        $data['addresses'] = $r->fetchall();

        /* add floating edit if the user has users edition rights */
        $data['edit'] = $this->user_rights_users($user_info);
        $data['user_id'] = $user_id;
        /* display the report block for the current file */
        $data['block_report'] = array(
            'relid' => $user_id,
            'reltype' => 3, /* users */
        );
        $data['score_stars'] = user_score($user_info['score']);

        error:
        $this->render('template', $data, $this);

        Doo::loadModel('Telemetry');
        Telemetry::log('profile/view', array('can-edit' => $data['edit']));
    }

}

?>
