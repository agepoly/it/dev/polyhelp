<?php
/**
 * ErrorController
 * Feel free to change this and customize your own error message
 *
 * @author darkredz
 */
class ErrorController extends EPFHelpParentController {

    static private $data_defaults = Array(
        'section' => 'Erreur',
        'css' => Array(),
        'js' => Array(),
        'errors' => Array(),
    );

    /** Displayed when the page was not found (404). */
    public function error_404() {
        array_append($data, self::$data_defaults);
        $data['section'] = "Page introuvable (erreur 404)";
        $data['pagename'] = 'error';
        $data['errors'][] = Array(
            'code' => 404,
            'text' => "La page demandée n'a pas été trouvée",
            'img'  => '/global/img/error.gif'
        );
        $this->render ('template', $data, $this);
    }

    /** Displayed when the an unknown fatal (internal) error happened. */
    public function error() {
        array_append($data, self::$data_defaults);
        $code = array_get($this->params, 'code', '(aucun)');
        $data['section'] = "Erreur interne";
        $data['pagename'] = 'error';
        $data['errors'][] = Array(
            'text' => "Une erreur interne s'est produite (problème N°{$code})",
        );
        $this->render('template', $data, $this);
    }

}
?>
