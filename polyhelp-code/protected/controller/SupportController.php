<?php

/**
 * Controller for the support pages.
 */

class SupportController extends EPFHelpParentController {

    static private $data_defaults = Array(
        'section' => 'Support',
        'css' => Array('support'),
        'js' => Array('jquery.dataTables.min', 'tables_custom', 'pdf'),
        'errors' => Array(),
    );

    /**
     * First page of the support part (displays sections by faculties).
     */
    public function start() {
        if ($this->user_is_professor()) $this->start_professors();
        else                            $this->start_students();
    }

    /** Support page for students. */
    private function start_students() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "support";
        $user_section = $this->user_section();

        /* get all the sections along with its faculty */
        $q =
            'SELECT s.id AS sid, s.name_fr AS sname, f.id AS fid,
                    f.name_fr AS fname, f.short AS fshort
            FROM sections s
            JOIN faculties f ON f.id = s.faculty_id
            WHERE s.id > 0 AND f.id > 0
            ORDER BY f.short';

        $r = Doo::db()->query($q);
        while ($row = $r->fetch()) {
            $faculties[$row['fid']] = Array(
                'name'  => $row['fname'],
                'short' => $row['fshort'],
            );
            /* the user section is separated at the top */
            if ($row['sid'] == $user_section) {
                $data['pref_section'] = $row;
            }
            /* other sections are displayed after */
            else {
                $sections[$row['fid']][] = Array(
                    'id'   => $row['sid'],
                    'name' => $row['sname'],
                );

            }
        }

        /* we regroup sections by faculty ($k is the section_id) */
        foreach ($sections as $k => $v) {
            array_append($faculties[$k]['sections'], $sections[$k]);
        }

        /* get other category-less forums to dislay them here */
        $q = 'SELECT id, name FROM postgroups WHERE reltype = 0';
        $r = Doo::db()->query($q);
        $data['forums_catless'] = $r->fetchall();

        /* the view will display sections by faculty */
        $data['faculties'] = $faculties;

        $this->render('template', $data, $this);
    }

    /** Returns a description of dataTable for courses of teachers. */
    private function table_factory_teacher_courses($rows) {
        return Array(
            'html_id' => 'courses',
            'headers' => Array("Nom", "Sections", "Forum"),
            'rows' => array_map(function ($v) {
                return Array(
                    Array(
                        'value' => $v['name'],
                        'link' => "/support/course/{$v['course_id']}"),
                    Array('value' => implode(' ', $v['sections'])),
                    Array(
                        'value' => "accès",
                        'link' => "/support/postgroup/{$v['postgroup_id']}"),
                );
            }, $rows),
        );
    }

    /** Support page for teachers. */
    private function start_professors() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "support_teachers";
        $data['js'][] = 'support_teachers';
        $data['js'][] = 'tables_custom';
        $data['js'][] = 'jquery.balloon.min';
        $data['js'][] = 'balloon_extended';
        $data['css'][] = 'datatables';
        $courses = $this->user_teaching_courses();
        uasort($courses, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });
        $data['table_courses'] = $this->table_factory_teacher_courses($courses);

        $this->render('template', $data, $this);
    }

    /**
     * The page showing the courses of the selected section.
     */
    public function section() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "support-section";
        $data['css'][] = 'datatables';
        $data['js'][] = 'support-section';
        $data['js'][] = 'jquery.balloon.min';
        $data['js'][] = 'balloon_extended';
        $section_id = $this->params['sid'];

        if ($this->user_is_professor()) {
            $data['errors'][] = Array(
                    'text' => "Cette page est reservé aux étudiants", 
                );
            goto error;
        }

        /* check the section exists and gets its name, its faculty, and the
         * forum associated links (postgroups). */
        $q =
            'SELECT s.name_fr AS name, spg.id AS spgid, fpg.id AS fpgid,
                    f.name_fr AS fname
            FROM sections s
            JOIN postgroups spg ON spg.reltype = 2 AND spg.relid = s.id
            JOIN faculties f ON f.id = s.faculty_id
            JOIN postgroups fpg ON fpg.reltype = 1 AND fpg.relid = s.faculty_id
            WHERE s.id = :sid';

        $r = Doo::db()->query($q, Array(':sid' => $section_id));
        if (!$row = $r->fetch()) {
            $data['errors'][] = Array(
                'text' => "Cette section n'a pas été trouvée",
            );
            goto error;
        }
        $data['section'] = "Section {$row['name']}";
        $data['faculty'] = "Faculté {$row['fname']}";
        $data['pg_section'] = $row['spgid'];
        $data['pg_faculty'] = $row['fpgid'];

        /* get all the courses of the given section */
        $q =
            'SELECT c.id, c.name, cs.degree, cs.semester, u.id AS uid,
                    u.first_name, u.last_name,
                    (SELECT COUNT(*) FROM filegroups WHERE course_id = c.id)
                    AS fcount, c.isa_key
            FROM courses c
            JOIN courses_sections cs ON cs.course_id = c.id
            JOIN course_prof cp ON cp.course_id = c.id
            JOIN users u ON u.id = cp.user_id
            WHERE cs.section_id = :cid
            ORDER BY cs.degree, cs.semester, c.name, c.id';

        $r = Doo::db()->query($q, Array(':cid' => $section_id) );
        $courses = Array();
        $last_course = null;
        $counter = 0;
        while ($row = $r->fetch()) {
            $teacher = Array(
                'id'    => $row['uid'],
                'lname' => $row['last_name'],
                'fname' => $row['first_name']
            );

            /* we merge the course for multiples teachers */
            if (array_get($last_course, 'id', null) === $row['id']) {
                $last_course['teachers'][] = $teacher;
                continue;
            }

            $row['semester'] = intval($row['semester']);
            $row['teachers'] = Array($teacher);
            $courses[] = $row;
            $last_course = &$courses[$counter];
            $counter += 1;
        }

        /* some extra information useful for pretty format */
        $data['courses'] = $courses;
        $data['degrees'] = Globals::$degrees;
        $data['semesters'] = Globals::$semesters;
        $data['favcourses'] = $this->user_favcourses_hash;

        /* default table search is the user's degree and current semester */
        $data['default_search'] = Globals::$semesters[Globals::current_semester()]['name']
            . " " . Globals::$degrees[$this->user_degree()]['name'];

        error:
        $this->render('template', $data, $this);
    }

    /**
     * The page showing the files/posts of the given course.
     */
    public function course() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "support-course";
        $data['css'][] = 'datatables';
        $data['js'][] = 'support-course';
        $data['js'][] = 'jquery.balloon.min';
        $data['js'][] = 'balloon_extended';
        $course_id = $this->params['cid'];

        /* check the access for professors */
        if ($this->user_is_professor()
         && !$this->user_rights_teacher_course($course_id))
        {
            $data['errors'][] = Array(
                'text' => "L'accès à ce cours vous a été refusé",
            );
            goto error;
        }

        /* check and get the course name to display it (along the sections) */
        $q = 'SELECT c.name, c.postgroup_id, MAX(pg.id),
                string_agg(s.short, \'||\') AS sections,
                string_agg(s.id::varchar, \',\') AS sections_ids
            FROM courses c
            JOIN courses_sections cs ON cs.course_id = c.id
            JOIN sections s ON s.id = cs.section_id
            JOIN postgroups pg ON pg.id = c.postgroup_id
            WHERE c.id = :cid
            GROUP BY c.id';

        $r = Doo::db()->query($q, array(':cid' => $course_id, ));

        if (!$course = $r->fetch()) {
            $data['errors'][] = Array(
                'text' => "Le cours choisi n'a pas été trouvé",
            );
            goto error;
        }
        $postgroup_id = $course['postgroup_id'];
        $course_name = $course['name'];
        $course_sections = implode(' ', explode('||', $course['sections']));
        $data['section'] = "Cours {$course_name} - [{$course_sections}]";
        $data['postgroup_id'] = $postgroup_id;
        /* FIXME: used for upload to suggest the correct section */
        $data['section_id_first'] = array_get(explode(',',
            $course['sections_ids']), 0, $this->user_section());

        /* get the filegroup list of this course */
        /* WARNING: FIXME: This query needs some optimisations! */
        $q =
            'SELECT fg.id, fg.name, fg.category, fg.author_id AS aid,
                    fg.uploader_id AS uid,
                    extract(\'epoch\' from fg.time) as time,
                    u1.first_name AS afname, u1.last_name AS alname,
                    u2.first_name AS ufname, u2.last_name AS ulname,
                    (SELECT COUNT(*) FROM files WHERE filegroup_id = fg.id)
                        AS fcount,
                    f.id IS NOT NULL AS corrections, fbest.id AS fid,
                    fbest.rating, u2.score AS uscore
            FROM filegroups fg
            JOIN users u1 ON u1.id = fg.author_id
            JOIN users u2 ON u2.id = fg.uploader_id
            LEFT JOIN files f ON f.filegroup_id = fg.id AND f.subtype > 1
            LEFT JOIN (SELECT DISTINCT ON(filegroup_id) id, filegroup_id,
                       rating_base+rating_cache AS rating FROM files
                       ORDER BY filegroup_id, rating_base+rating_cache DESC,
                       subtype ASC) fbest
                ON fbest.filegroup_id = fg.id
            WHERE course_id = :cid
            ORDER BY fbest.rating DESC, fg.name, fg.id';

        $r = Doo::db()->query($q, Array(':cid' => $course_id));

        $data['filegroups'] = Array();
        $fg_last_id = null;
        while ($row = $r->fetch()) {
            /* we can have duplicates, so we safely skip them */
            if ($fg_last_id === $row['id']) continue;

            /* calculate stars for this filegroup (best file) */
            $row['best_stars'] = file_score($row['rating']);

            $row['date'] = date(Globals::$date_day_format, $row['time']);
            $data['filegroups'][] = $row;
            $fg_last_id = $row['id'];
        }

        /* get the posts for the course forum */
        $q =
            'SELECT p.id, p.title, p.user_id, p.file_related,
                    u.first_name, u.last_name,
                    extract(\'epoch\' from p.creation_time) as ctime
            FROM posts p
            JOIN users u ON u.id = user_id
            WHERE postgroup_id = :pgid AND post_id IS NULL
                  AND file_related = false
            ORDER BY p.creation_time DESC';

        $r = Doo::db()->query($q, Array(':pgid' => $postgroup_id));

        $data['posts'] = Array();
        while ($row = $r->fetch()) {
            $row['cdate'] = date(Globals::$date_day_format, $row['ctime']);
            $data['posts'][] = $row;
        }

        /* get the same course in other sections (based on the name) */
        $q =
            'SELECT c.id, string_agg(s.name_fr, \'||\') AS sections
            FROM courses c
            JOIN courses_sections cs ON cs.course_id = c.id
            JOIN sections s ON s.id = cs.section_id
            WHERE c.name = :cname AND c.id != :cid
            GROUP BY c.id';

        $r = Doo::db()->query($q,   Array(    ':cname' => $course_name,
                                            ':cid' => $course_id
                                        )
                            );

        while ($row = $r->fetch()) {
            $row['sections'] = explode('||', $row['sections']);
            $data['courses_related'][] = $row;
        }

        /* some extra information useful for pretty format */
        $data['categories'] = Globals::$file_categories;
        $data['course_id'] = $course_id;

        error:
        $this->render('template', $data, $this);
    }

    /**
     * Page displayed when a file has been clicked.
     */
    public function file() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "support-file";
        $data['section'] = "Fichiers:";
        $data['css'][] = 'datatables';
        $data['css'][] = 'forum';
        $data['js'][] = 'support-file';
        $data['js'][] = 'form_hint';
        $data['js'][] = 'jquery.balloon.min';
        $data['js'][] = 'balloon_extended';
        $data['js'][] = 'jquery-ui-1.9.0.custom.min';
        $data['js'][] = 'block_report';
        $data['css'][] = 'jquery-ui-1.9.0.custom.min';
        $filegroup_id = $this->params['fgid'];
        $file_id = array_get($this->params, 'fid', false);
        $user_id = $this->user_id();
        $log_errors = array();

        /* get the filegroup information */
        $q =
            'SELECT fg.name, fg.post_id, fg.author_id, fg.uploader_id,
                    extract(\'epoch\' from fg.time) as time,
                    uu.first_name AS ufname, uu.last_name AS ulname,
                    ua.first_name AS afname, ua.last_name AS alname,
                    fg.reviewed, c.id AS course_id, c.name AS cname,
                    uu.score AS uscore, ua.score AS ascore
            FROM filegroups fg
            JOIN users uu ON uu.id = fg.uploader_id
            JOIN users ua ON ua.id = fg.author_id
            JOIN courses c ON c.id = fg.course_id
            WHERE fg.id = :fgid';

        $r = Doo::db()->query($q, Array(':fgid' => $filegroup_id));

        if ($row = $r->fetch()) {
            $row['date'] = date(Globals::$date_day_format, $row['time']);
            $row['edit_rights'] = $this->user_rights_filegroup($row);
            $post_id = $row['post_id'];
            $filegroup_info = $row;
            $data['filegroup'] = $row;
            $course_id = $row['course_id'];
        }
        else {
            $data['errors'][] = Array(
                'text' => "Le groupe de fichier n'existe pas",
            );
            $log_errors[] = 'filegroup-not-there';
            goto error;
        }

        /* check the access for professors */
        if ($this->user_is_professor()
         && !$this->user_rights_teacher_course($course_id))
        {
            $data['errors'][] = Array(
                'text' => "L'accès à ce cours vous a été refusé",
            );
            $log_errors[] = 'no-right-prof';
            goto error;
        }

        /* add the file name to the page title */
        $data['section'] .= " {$data['filegroup']['name']}";

        /* get every files info in the filegroup */
        $q =
            'SELECT f.id, f.subtype, f.extension, f.filepath, f.uploader_id,
                f.rating_base+f.rating_cache AS rating, f.description,
                u.id AS uid, f.origin,
                extract(\'epoch\' from f.creation_time) as ctime,
                extract(\'epoch\' from f.upload_time) as utime,
                u.first_name AS ufname, u.last_name AS ulname,
                fr.file_id IS NOT NULL as rated
            FROM files f
            JOIN users u ON u.id = f.uploader_id
            LEFT JOIN filerates fr ON fr.file_id = f.id AND fr.user_id = :uid
            WHERE f.filegroup_id = :fgid
            ORDER BY f.rating_base+f.rating_cache DESC, f.subtype';

        $r = Doo::db()->query($q, Array(':fgid' => $filegroup_id,
                                        ':uid' => $this->user_id()));
        $data['files'] = Array();
        $i = 0;
        while ($row = $r->fetch()) {
            $row['udate'] = date(Globals::$date_day_format, $row['utime']);
            $row['cdate'] = date(Globals::$date_day_format, $row['ctime']);
            $row['url'] = $row['filepath'];
            $row['edit_rights'] = $this->user_rights_file($row,
                $filegroup_info);

            $data['files'][] = $row;

            if ($file_id === false) { /* default file is the first one */
                $file_id = $row['id'];
                $data['cur_file'] = $i;
            }
            else if ($row['id'] == $file_id) { /* the user selected this one */
                $data['cur_file'] = $i;
            }
            ++$i;
        }

        /* if there isn't any file in this group, report it */
        if (!isset($row)) {
            $data['errors'][] = Array(
                'text' => "Ce groupe de fichier ne contient aucun fichier, ceci "
                ."est probablement un bug",
            );
            $log_errors[] = 'filegroup-empty';
            goto error;
        }

        /* test that the selected file is in our list */
        if (!isset($data['cur_file'])) {
            $data['errors'][] = Array(
                'text' => "Le fichier sélectionné n'existe pas dans ce groupe,"
                ." ceci est probablement un bug",
            );
            $log_errors[] = 'file-not-in-filegroup';
            goto error;
        }

        /* get the posts related to this filegroup */
        $this->retrieve_post_data($post_id, $data);

        $data['filegroup_id'] = $filegroup_id;
        $data['cur_file_f'] = $data['files'][$data['cur_file']];
        $data['file_corrections'] = Globals::$file_corrections;
        $data['parent_id'] = $post_id;
        $data['action'] = 'add';
        $data['superadmin'] = $this->user_is_superadmin();
        /* display the report block for the current file */
        $data['block_report'] = Array(
            'relid' => $data['cur_file_f']['id'],
            'reltype' => 0, /* files */
        );
        /* hide the post report */
        $data['hide_post_report'] = true;
        
        error:

        // Log file view
        Doo::loadModel('Telemetry');
        Telemetry::log('file/view', array(
            'filegroup-id' => $filegroup_id,
            'file-id' => $file_id,
            'errors' => $log_errors
        ));

        $this->render('template', $data, $this);
    }

    /**
     * Allows the user to download a file, we can control who access it.
     */
    public function download() {
        array_append($data, self::$data_defaults);
        $file_id = $this->params['fid'];
        $log_errors = array();
        
        $q = 'SELECT filepath, name, subtype, category FROM files 
                JOIN filegroups
                ON files.filegroup_id = filegroups.id
                WHERE files.id = :fid';
        $r = Doo::db()->query($q, Array(':fid' => $file_id));
        $row = $r->fetch();
        if (!$row) {
            $data['errors'][] = Array(
                'text' => "Le fichier demandé n'existe pas",
            );
            $log_errors[] = 'file-not-there';
            goto error;
        }

        $filepath = $row['filepath'];
        if (!file_exists($filepath)) {
            $data['errors'][] = Array(
                'text' => "Le fichier demandé n'a pas été trouvé à sa place",
            );
            $log_errors[] = 'file-not-at-filepath';
            goto error;
        }
        
        $name = $row['name'];
        if($row['category'] != 3 && $row['category'] != 4){
            $name .= "_".Globals::$file_corrections[$row['subtype']];
        }
        

        $handle = fopen($filepath, 'r');

        // Log file download
        Doo::loadModel('Telemetry');
        Telemetry::log('file/download', array(
            'file-id' => $file_id
        ));

        header('Content-Type: '. file_get_mime($filepath));
        header('Content-Disposition: attachment; filename='. $name);
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '. filesize($filepath));
        header('Expires: 0');

        stream_chunked_file($handle);
        fclose($handle);

        // die('no problem');
        exit();

        error:
        
        // Log file download error
        Doo::loadModel('Telemetry');
        Telemetry::log('file/download', array(
            'file-id' => $file_id, 'errors' => $log_errors
        ));

        $data['pagename'] = 'error';
        if (isset($handle)) fclose($handle);
        $this->render('template', $data, $this);
    }

    /**
     * Send a ZIP pack of files, no view, just streams the archive.
     */
    public function zippack() {
        array_append($data, self::$data_defaults);

        /* get and check/clean the files list */
        $fgids = Array();
        foreach (array_get($_POST, 'fgids', Array()) as $v) {
            if (is_numeric($v)) {
                $fgids[] = intval($v);
            }
        }

        /* check that we have cleaned and there is some file id left */
        if (count($fgids) < 1) {
            $data['errors'][] = Array(
                'text' => "Aucun groupe de fichiers correct spécifié",
            );
            goto error;
        }

        /* construct the IN SQL query part */
        $params = Array();
        $in_cond = '';
        foreach ($fgids as $k => $v) {
            if ($k > 0) $in_cond .= ', ';

            $in_cond .= ':fgid_'. $k;
            $params[':fgid_'. $k] = $v;
        }

        /* get the files and its associated filegroup, author info */
        $q =
            'SELECT f.id, f.subtype, f.extension, f.filepath, f.subtype,
                    fg.name AS fgname, fg.category, u.first_name, u.last_name,
                    c.name AS cname, f.filegroup_id
            FROM files f
            JOIN filegroups fg ON fg.id = f.filegroup_id
            JOIN users u ON u.id = fg.author_id
            JOIN courses c ON c.id = fg.course_id
            WHERE f.filegroup_id IN ('. $in_cond .')';

        $r = Doo::db()->query($q, $params);
        $row_count = $r->rowCount();
        $files = $r->fetchAll();
        $filegroups_r = array_map(function ($v) {
            return $v['filegroup_id'];
        }, $files);

        /* check if the DB didn't or returned less entries than expected */
        foreach ($fgids as $fgid) {
            $errors_fg = 0;
            if (!in_array($fgid, $filegroups_r)) {
                $data['errors'][] = Array(
                    'text' => "La base de donnée n'a pas trouvé certains "
                    ."des fichiers demandés (comme N°{$fgid})",
                );
                ++$errors_fg;
            }
            if ($errors_fg > 0) {
                goto error;
            }
        }

        try {
            /* create a zip file in a temporary directory */
            $zip_path = tempnam('/tmp', 'zippack');
            $zip = new ZipArchive();
            if ($zip->open($zip_path, ZIPARCHIVE::CREATE) !== true) {
                $data['errors'][] = Array(
                    'text' => "Impossible de créer l'archive dynamique",
                );
                goto error;
            }

            /* check that the file exists and add the files to the zip */
            foreach ($files as $k => $file) {
                $file_path = $file['filepath'];
                $zip_authors[] = "{$file['last_name']} {$file['first_name']}";
                if (!file_exists($file_path)) {
                    $data['errors'][] = Array(
                        'text' => "Le fichier demandé (N°{$file['id']}) n'a "
                        ."pas été trouvé à sa place",
                    );
                    goto error;
                }

                $file_local = Globals::archive_localpath($file);
                if ($zip->addFile($file_path, $file_local) !== true) {
                    $data['errors'][] = Array(
                        'text' => "Le fichier demandé (N°{$file['id']}) n'a "
                        ."pas pu être ajouté à l'archive ZIP",
                    );
                    goto error;
                }
            }
        }
        catch (Exception $e) {
            $data['errors'][] = Array(
                'text' => "Une exception s'est produite lors de la création "
                ."dynamique de l'archive",
            );
            goto error;
        }

        /* add a ZIP comment as a copyright notice */
        $zip_comment =
            "Le contenu de cette archive provient du site polyhelp.ch, "
            ."certains fichiers peuvent provenir de l'EPFL (École "
            ."Polytechnique Fédérale de Lausanne), de ce fait, ils peuvent "
            ."contenir du matériel sous copyright, droit d'auteur.<br/><br/>";
        $zip_comment .= "Les fichiers on été crées par les auteurs suivants "
            ."(veuillez respecter les droits de ceux-ci:<br/>";
        $zip_comment .= implode('<br/>', array_rem_duplicates($zip_authors));
        $zip->setArchiveComment($zip_comment);

        /* now stream the zip archive */
        $zip->close();
        $this->setContentType('zip');
        $zip_name = 'epfhelp-zippack-'. time() .'.zip';
        Header('Content-Disposition: inline; filename='. $zip_name);
        Header('Content-Length: '. filesize($zip_path));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        ob_clean();
        flush();
        readfile($zip_path);
        flush();

        /* remove the zip, it is no longer used */
        unlink($zip_path);

        exit();

        error:
        $data['pagename'] = 'error';

        if (isset($zip)) $zip->close();
        if (isset($zip_path)) unlink($zip_path);

        $this->render('template', $data, $this);
    }

    /**
     * Display a seach form and a table with the results.
     */
    public function search() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "search";
        $data['section'] = "Recherche";
        $data['css'][] = 'search';
        $data['css'][] = 'datatables';
        $data['js'][] = 'search';
        $data['js'][] = 'jquery.balloon.min';
        $data['js'][] = 'balloon_extended';

        /* the search on file is enabled */
        if (array_get($_POST, 'search_file', false)) {
            /* default values */
            $filters = 0;
            $file_name    =  array_get($_POST,  'file_name',    false);
            $file_type    =  array_get($_POST,  'file_type',    false);
            $file_course  =  array_get($_POST,  'file_course',  false);
            $params = Array();
            $data['files'] = Array();

            $q =
                'SELECT fg.id, fg.name, c.id AS cid, c.name AS cname,
                    fg.category, u.last_name AS alname, u.first_name AS afname,
                    extract(\'epoch\' from fg.time) as ctime
                FROM filegroups fg
                JOIN courses c ON c.id = fg.course_id
                JOIN users u ON u.id = fg.author_id
                WHERE ';

            /* name and course filter */
            $text_filters = Array(Array($file_name,   'fg.name'),
                                  Array($file_course, 'c.name'));
            sql_where_append_texts($q, $params, $text_filters, $filters);

            /* category filter */
            if ($file_type && is_numeric($file_type) > 0) {
                if ($filters > 0) $q .= 'AND ';

                $q .= 'fg.category = :fgcat ';
                $params[':fgcat'] = intval($file_type);

                ++$filters;
            }

            if ($filters == 0) {
                $data['errors'][] = Array(
                    'text' => "Il faut au moins un filtre sur les fichiers",
                );
                goto error;
            }

            $q .= 'LIMIT 200';

            $r = Doo::db()->query($q, $params);
            while ($row = $r->fetch()) {
                $row['date'] = date(Globals::$date_day_format, $row['ctime']);
                $data['files'][] = $row;
            }
        }

        /* the search on posts is enabled */
        if (array_get($_POST, 'search_post', false)) {
            /* default values */
            $filters = 0;
            $post_title    =  array_get($_POST,  'post_title',    false);
            $post_content  =  array_get($_POST,  'post_content',  false);
            $post_frelated =  array_get($_POST,  'post_file',     false);
            $params = Array();
            $data['posts'] = Array();

            $q =
                'SELECT p.id, pp.id AS ppid, pp.title AS pptitle, u.id AS uid,
                    pg.id AS pgid, pg.name AS pgname, p.title,
                    u.last_name AS alname, u.first_name AS afname,
                    extract(\'epoch\' from p.creation_time) as ctime
                FROM posts p
                JOIN postgroups pg ON pg.id = p.postgroup_id
                LEFT JOIN posts pp ON pp.id = p.post_id
                JOIN users u ON u.id = p.user_id
                WHERE ';

            /* title and content filter */
            $text_filters = Array(  Array($post_title,   'p.title'),
                                    Array($post_content, 'p.content'));
            sql_where_append_texts($q, $params, $text_filters, $filters);

            /* file related filter */
            if ($filters > 0) $q .= 'AND ';
            $q .= 'p.file_related = '. ($post_frelated ? 'true' : 'false') .' ';

            if ($filters == 0) {
                $data['errors'][] = Array(
                    'text' => "Il faut au moins un filtre sur les forums",
                );
                goto error;
            }

            $q .= 'LIMIT 200';

            $r = Doo::db()->query($q, $params);
            while ($row = $r->fetch()) {
                /* if the post is the parent one, set to it */
                if (!isset($row['ppid'])) {
                    $row['ppid'] = $row['id'];
                    $row['pptitle'] = $row['title'];
                }
                $row['date'] = date(Globals::$date_day_format, $row['ctime']);
                $data['posts'][] = $row;
            }
        }

        error:
        $this->render('template', $data, $this);
    }

}

?>
