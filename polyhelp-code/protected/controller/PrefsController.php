<?php

/**
 * Controller for the preferences of the user.
 */

class PrefsController extends EPFHelpParentController {

    static private $data_defaults = array(
        'section' => 'Préférences',
        'css' => array('prefs'),
        'js' => array(),
        'errors' => array(),
    );

    /**
     * Displayed when a user wants to edit its or one profile if granted.
     */
    public function prefs() {
        array_append($data, self::$data_defaults);
        $data['pagename'] = "prefs";
        $data['js'][] = "form_hint";
        $data['js'][] = "prefs";
        $real_user_id = $this->user_id();
        $user_id = array_get($this->params, 'uid', $real_user_id);
        $admin_edit = ($user_id != $real_user_id);
        $superadmin_edit = $this->user_is_superadmin();
        $errors = 0;

        Doo::loadModel('Telemetry');
        Telemetry::log('profile/edit-attempt');

        /* get the user current info and avatar */
        try {
            $q = 'SELECT id, superadmin, avatar FROM users WHERE id = :uid';
            $r = Doo::db()->query($q, array(':uid' => $user_id));
            $user_info = $r->fetch();
            $avatar_old = $user_info['avatar'];
        }
        catch (PDOException $e) {
            $data['errors'][] = array(
                'text' => "L'utilisateur de ce profil n'a pas été trouvé",
            );
            goto error;
        }

        /* check rights of the user to edit this profile */
        if ($admin_edit && !$this->user_rights_users($user_info)) {
            $data['errors'][] = array(
                'text' => "Vous n'avez pas les droits nécessaires",
            );
            goto error;
        }
        /* in the other case, the user has rights (his profile or admin) */

        /* update the user informations */
        if ($_POST) {
            /* validate the form fields */
            $fields_check = array(
                'section' => array('name' => 'Section', 'type' => 'numeric',
                                   'min' => 0),
                'degree' => array('name' => 'Année', 'type' => 'numeric',
                               'in_array' => array_keys(Globals::$degrees)),
            );

            /* add the admin extra fields, yes we check admins too */
            if ($admin_edit) {

                array_append($fields_check, array(
                    'score' => array('name' => "Score", 'type' => 'numeric'),
                ));
            }

            $addresses = array_zip(array_get($_POST, 'name', array()),
                array_get($_POST, 'address', array()),
                array_get($_POST, 'desc', array()));

            /* for each address add checks */
            foreach ($addresses as $k => $v) {
                $name = &$addresses[$k][0];
                $addr = &$addresses[$k][1];
                $desc = &$addresses[$k][2];
                $name_len = strlen($name);
                $addr_len = strlen($addr);
                $desc_len = strlen($desc);

                /* if all the fields are blank, then skip it */
                if ($name_len + $addr_len + $desc_len == 0) {
                    unset($addresses[$k]);
                    continue;
                }

                /* we do a custom field checking and cleaing */
                $fields_info = array(
                    array("Titre N°",       &$name, $name_len, 3, 64),
                    array("Adresse N°",     &$addr, $addr_len, 3, 64),
                    array("Description N°", &$desc, $desc_len, null, null),
                );

                foreach ($fields_info as $field_info) {
                    list($n, $_, $len, $min, $max) = $field_info;
                    $value = &$field_info[1];

                    if ($min != null && $len < $min) {
                        $data['warnings'][] = array(
                            'text' => "Le champ {$n}".($k+1)." devrait avoir "
                            ."une " ."longueur d'au minimum {$min}",
                        );
                    }
                    if ($max != null && $len > $max) {
                        $data['warnings'][] = array(
                            'text' => "Le champ {$n}".($k+1)." devrait avoir "
                            ."une longueur d'au maximum {$max}",
                        );
                    }
                    $value = htmlentities($value);
                }
            }

            /* validate and parse superadmin fields about rights */
            $rights_fields = array_get($_POST, 'rights', array());
            $rights_list = array();
            foreach ($rights_fields as $k => $v) {
                /* if it's not enabled, then ignore it */
                if (!isset($v['enabled'])) {
                    unset($rights_fields[$k]);
                    continue;
                }

                /* check the right type */
                if (!in_array($k, array_keys(Globals::$user_rights_type))) {
                    $data['warnings'][] = array(
                        'text' => "Un droit à un type est invalide",
                    );
                    break;
                }

                $type = Globals::$user_rights_type[$k];
                if ($type['require_id'] && !isset($v['relid'])) {
                    $data['warnings'][] = array(
                        'text' =>
                        "Le droit {$type['name']} a besoin d'un objet cible.",
                    );
                    break;
                }

                $right_info = array(
                    'reltype' => $k,
                    'relid'   => array_get($v, 'relid', null),
                );

                /* check the right object existence */
                $object_info = $this->rights_object_info($right_info);
                if (!$object_info['found']) {
                    $data['warnings'][] = array(
                        'text' =>
                        "L'objet du droit {$type['name']} est introuvable",
                    );
                    break;
                }

                $rights_list[] = $right_info;
            }

            $errors += count_nofail($data['warnings']);
            $errors += check_form($fields_check, $_POST, $data['warnings'],
                $values);

            /* try to retrieve the avatar file if a new one is uploaded */
            $file = array_get($_FILES, 'avatar', false);
            if ($file !== false && $file['name']) {

                if ($file === false || $file['error']) {
                    $data['warnings'][] = array(
                        'text' => "Une erreur s'est produite avec l'envoi du "
                        ."fichier d'avatar",
                    );
                    ++$errors;
                    goto upload_fail;
                }

                $info = new finfo(FILEINFO_MIME);
                $exploaded = explode(';', $info->file($file['tmp_name']), 2);
                $file_mime = $exploaded[0];

                /* check the avatar file mime */
                if ($file_mime === false || !array_key_exists($file_mime,
                    Globals::$avatar_accept_mimes))
                {
                    if ($file_mime === false) $file_mime = 'inconnu';
                    $data['warnings'][] = array(
                        'text' => "Le type de fichier de {$file['name']} "
                        ."({$file_mime}) n'est pas à un format autorisé",
                    );
                    ++$errors;
                    goto upload_fail;
                }

                /* check the file size, don't allow too big */
                if (filesize($file['tmp_name']) > Globals::$avatar_max_size) {
                    $data['warnings'][] = array(
                        'text' => "Le fichier d'avatar {$file['name']} devrait "
                        ."avoir une taille d'au maximum "
                        . (Globals::$avatar_max_size/1024) . " KiB",
                    );
                    ++$errors;
                    goto upload_fail;
                }

                /* move the avatar file to its final destination */
                $avatar_path = Globals::avatar_file_path($user_id, $file_mime);
                if (!move_uploaded_file($file['tmp_name'], $avatar_path)) {
                    $data['warnings'][] = array(
                        'text' => "Le fichier {$file['name']} n'a pas pu être "
                        ."déplacé sur le serveur",
                    );
                    ++$errors;
                    goto upload_fail;
                }

                /* delete the actual avatar file but make some checks */
                if (strlen($avatar_old) > 0
                    && startsWith($avatar_old, Globals::$avatar_dir)
                    && $avatar_path != $avatar_old)
                {
                    unlink($avatar_old);
                }
            }
            else {
                /* the avatar didn't change */
                $avatar_path = $avatar_old;
            }

            upload_fail:
            if ($errors > 0) goto retrieve_info;

            Doo::db()->beginTransaction();

            try {
                /* update user section/faculty and degree from inputs */
                $params = array(
                            ':uid' => $user_id,
                            ':sid' => $_POST['section'],
                            ':degree' => $_POST['degree'],
                            ':avatar' => $avatar_path);

                $q =
                    'UPDATE users
                    SET section_id = :sid, faculty_id = s.faculty_id,
                        degree = :degree, avatar = :avatar ';

                /* add the extra fields the admins can edit */
                if ($admin_edit) {
                    $q .= ', professor = :professor, score = :score,
                             enabled = :enabled ';
                    $params[':score']     = $_POST['score'];
                    $params[':professor'] =
                        booltostring(isset($_POST['professor']));
                    $params[':enabled']   =
                        booltostring(isset($_POST['enabled']));
                }

                /* add extra fields for superadmin */
                if ($superadmin_edit) {
                    $q .= ', superadmin = :superadmin ';
                    $params[':superadmin'] =
                        booltostring(isset($_POST['superadmin']));
                }

                /* the rest of the update query */
                $q .= 'FROM sections s
                    WHERE users.id = :uid AND s.id = :sid';

                $r = Doo::db()->query($q, $params);

                /* first delete all addresses, then add these of the form */
                $q = 'DELETE FROM user_addresses WHERE user_id = :uid';
                $r = Doo::db()->query($q, array(':uid' => $user_id) );

                /* update users addresses, if any */
                if (count($addresses) > 0) {
                    $q =
                        'INSERT INTO user_addresses
                            (user_id, name, value, description)
                         VALUES ';

                    $params = array(':uid' => $user_id);
                    foreach ($addresses as $k => $v) {
                        if ($k > 0) $q .= ', ';

                        $q .= '(:uid, :n_'. $k .', :v_'. $k .', :d_'. $k .') ';

                        $params[':n_'. $k] = $v[0];
                        $params[':v_'. $k] = $v[1];
                        $params[':d_'. $k] = $v[2];
                    }

                    $r = Doo::db()->query($q, $params);
                }

                /* add requested rights if by admin */
                if ($superadmin_edit && count($rights_list) > 0) {
                    $q = 'INSERT INTO user_rights (user_id, reltype, relid)
                        VALUES ';
                    $params = array(':uid' => $user_id);

                    /* add each rights to the DB insertion query */
                    foreach ($rights_list as $k => $v) {
                        if ($k > 0) $q .= ', ';

                        $q .= '(:uid, :reltype_'. $k .', :relid_'. $k .') ';
                        $params[':reltype_'. $k] = $v['reltype'];
                        $params[':relid_'. $k] = $v['relid'];
                    }

                    $r = Doo::db()->query($q, $params);
                }
            }
            catch (Exception $e) {
                Doo::db()->rollback();
                $data['errors'][] = array(
                    'text' => "Impossible de mettre à jour la base de donnée ",
                );
                goto error;
            }

            Doo::db()->commit();
            $data['successes'][] = array(
                'text' => "Les informations ont été enregistrées avec succès",
            );
        }

        retrieve_info:
        /* get the user informations */
        $q =
            'SELECT u.section_id, u.faculty_id, avatar, first_name, last_name,
                    creation_time, u.professor, u.score, degree, u.enabled,
                    f.name_fr AS fname, s.name_fr AS sname, u.superadmin,
                    u.gaspar
            FROM users u
            JOIN faculties f ON f.id = u.faculty_id
            JOIN sections s ON s.id = u.section_id
            WHERE u.id = :uid';

        $r = Doo::db()->query($q, array(':uid' => $user_id));
        if (!$user_info = $r->fetch()) {
            $data['errors'][] = array(
                'text' => "L'utilisateur de ce profil n'a pas été trouvé",
            );
            goto error;
        }

        /* get the faculties and sections list */
        $q = 'SELECT id, name_fr AS name FROM faculties';
        $data['faculties'] = Doo::db()->query($q)->fetchall();
        $data['sections'] = $this->sections(true);

        /* default avatar if necessary */
        if (strlen($avatar_old) == 0) {
            $user_info['avatar'] = Globals::$user_avatar_default;
        }

        /* add once connected based on gaspar existence in DB */
        $user_info['connected_once'] = ($user_info['gaspar'] == true);

        $data['user_info'] = $user_info;

        /* get the user contact informations */
        if (!$_POST) {
            $q = 'SELECT name, value, description FROM user_addresses
                WHERE user_id = :uid';
            $r = Doo::db()->query($q, array(':uid' => $user_id));
            $data['addresses'] = $r->fetchall();
        }
        /* uses the POST information */
        else {
            $data['addresses'] = array_map(function ($v) {
                return array(
                    'name' => $v[0], 'value' => $v[1],
                    'description' => $v[2]);
            }, $addresses);
        }

        /* get the user rights for superadmins */
        $data['rights'] = array();
        if ($superadmin_edit) {
            $q = 'SELECT id, reltype, relid
                FROM user_rights WHERE user_id = :uid';
            $r = Doo::db()->query($q, array(':uid' => $user_id));

            while ($row = $r->fetch()) {
                /* WARNING: it queries the DB for each row, may be slow! */
                $info = $this->rights_object_info($row);
                array_append($row, $info);

                $data['rights'][] = $row;
            }
        }
        /* retrieve object list for each right type */
        $data['rights_objects'] = $this->rights_objects_list();

        $data['user_id'] = $user_id;
        $data['real_user_id'] = $real_user_id;
        $data['admin_edit'] = $admin_edit;
        $data['superadmin_edit'] = $superadmin_edit;

        error:
        $this->render('template', $data, $this);
    }

}

?>
