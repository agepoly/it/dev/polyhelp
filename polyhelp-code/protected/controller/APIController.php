<?php

/**
 * This is the Controller for the public API (for SPD), it uses Tequila to auth 
 * users and communicate using JSON as output.
 *
 * Here is a list of the format expected by the API for the parameters:
 *   - tokens and sessions must stay unmodified;
 *   - course: the ISA ID must be used, eg: CS-320;
 *   - file id: the EPFhelp internal id must be used;
 *   - rate: must be +1 or -1;
 *
 */
class APIController extends EPFHelpParentController {

    private static $nologin_res = [ 'gentoken', 'gensession' ];

    /** Override the EPFHelpParentController beforeRun. */
    public function beforeRun($ressource, $action) {
        /* set a custom error handler so we can print out JSON. */
        set_error_handler("APIController::error_handler", E_ALL);

        /* check client IP address and filter his requests */
        /* removed because android clients needs to use these API directly
        if (Globals::protection_check_address()) {
            die(json_error('your access to the API was forbidden', false));
        }
        */

        /* we enforce authentification for all the API services except for the
         * gentoken and gensession calls */
        if (!in_array($action, self::$nologin_res)) {
            $this->ensure_login();
        }
    }

    static public function error_handler($errno, $errstr, $errfile, $errline) {
        $code = mine_log_that_error($errno, $errstr, $errfile, $errline, null);
        die(json_error('internal error (was logged under #'. $code .')'));
    }


    /** Override the ensure login to get session from the http headers. */
    private function ensure_login() {
        $session = array_get($_POST, 'session', null);
        if ($session === null) {
            die(json_error('the session argument is missing'));
        }

        $user_id = $this->associate_session($session);
        if ($user_id === null) {
            die(json_error('the session is invalid'));
        }

        /* associate the user */
        $this->_user_id = $user_id;
    }

    /** Generate the file download url for the API. */
    static private function download_url($fid) {
        # FIXME: assume https is enabled and default port used
        return 'https://'. $_SERVER['SERVER_NAME']
            . '/api/file/download/'. $fid;
    }

    /** Get a course id from its ISA id. */
    static public function course_from_isa($course_isa) {
        $q = 'SELECT id FROM courses WHERE isa_key = :isa';
        $r = Doo::db()->query($q, [ ':isa' => $course_isa ]);
        if (!$row = $r->fetch()) {
            return null;
        }
        return $row['id'];
    }

    /** Generate a Tequila token and output it. */
    public function gentoken() {
        $teq = new Tequila();
        $token = $teq->request($_SERVER['HTTP_HOST']);

        if ($token === null) {
            die(json_error('the Tequila token generation was denied'));
        }

        die(json_encode([ 'token' => $token ]));
    }

    /** Generate an internal session so we remember the user. */
    public function gensession() {
        $token = array_get($_POST, 'token', null);
        if ($token === null) {
            die(json_error('the token argument is missing'));
        }

        $teq = new Tequila();
        $user_info = $teq->fetch($token);
        $gaspar = $user_info['gaspar'];

        if ($user_info === null) {
            die(json_error("Tequila token validation failed"));
        }

        try {
            $this->_user_id = $this->associate_user($gaspar, $user_info);
        }
        catch (UnexpectedValueException $e) {
            die(json_error($e->getMessage()));
        }

        if ($this->_user_id == null) {
            die(json_error('user association failed'));
        }

        $ldap = new LDAP();
        if (!$ldap->agepoly_connect()) {
            die(json_error('cannot connect to the AGEpoly LDAP'));
        }

        if ($ldap->user_is_blacklisted($gaspar)) {
            die(json_error('the user is blacklisted by the AGEpoly'));
        }

        /* register and send session id */
        $session_hash = $this->generate_session($gaspar);
        if ($session_hash === null) {
            die(json_error('session generation failed'));
        }

        die(json_encode([ 'session' => $session_hash ]));
    }

    /** Returns the filegroups list for the given course. */
    public function filegroups() {
        $course_isaid = $this->params['isaid'];
        $out = [ ];

        /* check the course exists by ISA ID and get the DB id */
        $course_id = self::course_from_isa($course_isaid);
        if ($course_id === null) {
            die(json_error('the course was not found'));
        }

        /* get filegroups info for this course and build the output */
        $q =
            'SELECT fg.id, fg.name, u.first_name, u.last_name,
                fg.category, fg.post_id,
                extract(\'epoch\' from fg.time) as time
            FROM filegroups fg
            JOIN users u ON u.id = fg.author_id
            WHERE fg.course_id = :cid';

        $r = Doo::db()->query($q, [ ':cid' => $course_id ]);
        while ($row = $r->fetch()) {
            $fgid = $row['id'];
            $pid = $row['post_id'];

            /* main info of the filegroup */
            $fg = [
                'id'             =>  $fgid,
                'title'          =>  $row['name'],
                'upload_date'    =>  $row['time'],
                'course'         =>  $course_isaid,
                'category'       =>  $row['category'],
                'creation_date'  =>  $row['time'],
                'posts'          =>  [ ],
                'files'          =>  [ ],
                'author'         =>  [
                    'first' => $row['first_name'],
                    'last' => $row['last_name'],
                ],
            ];

            /* files of the filegroup */
            $q =
                'SELECT id, subtype, rating_base+rating_cache AS rating,
                    origin, description, filepath,
                    (SELECT COUNT(*) FROM filerates fr WHERE fr.file_id = id)
                        AS rating_count
                FROM files
                WHERE filegroup_id = :fid';

            $s = Doo::db()->query($q, [ ':fid' => $fgid ]);
            while ($file = $s->fetch()) {
                $fg['files'][] = [
                    'id'            =>  $file['id'],
                    'rating'        =>  $file['rating'],
                    'rating_count'  =>  $file['rating_count'],
                    'corrections'   =>  $file['subtype'],
                    'origin'        =>  $file['origin'],
                    'information'   =>  $file['description'],
                    'download_url'  =>  self::download_url($file['id']),
                    'title' => Globals::$file_corrections[$file['subtype']]
                        . " " . basename($file['filepath']),
                ];
            }

            /* posts of the filegroup */
            $q =
                'SELECT p.id, p.title, p.content, u.first_name, u.last_name,
                    extract(\'epoch\' from p.creation_time) as time
                FROM posts p
                JOIN users u ON u.id = p.user_id
                WHERE p.id = :pid OR p.post_id = :pid';

            $s = Doo::db()->query($q, [ ':pid' => $pid ]);
            while ($post = $s->fetch()) {
                $fg['posts'][] = [
                    'id'       =>  $post['id'],
                    'title'    =>  $post['title'],
                    'content'  =>  $post['content'],
                    'date'     =>  $post['time'],
                    'author'   =>  [
                        'first' => $row['first_name'],
                        'last'  => $row['last_name'],
                    ],
                ];
            }

            $out[] = $fg;
        }

        die(json_encode([ 'filegroups' => $out ]));
    }

    /** Send (stream) the requested file. */
    public function file_download() {
        $file_id = $this->params['fid'];

        $q = 'SELECT filepath FROM files WHERE id = :fid';
        $r = Doo::db()->query($q, [ ':fid' => $file_id ]);
        $row = $r->fetch();
        if (!$row || !file_exists($row['filepath'])) {
            header('Status: 404 Not Found');
            die(json_error('the file was not found', false));
        }

        $filepath = $row['filepath'];
        $name = basename($filepath);
        $handle = fopen($filepath, 'r');

        header('Content-Type: '. file_get_mime($filepath));
        header('Content-Disposition: attachment; filename='. $name);
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '. filesize($filepath));
        header('Expires: 0');

        stream_chunked_file($handle);
        fclose($handle);

        exit();
    }

    /** Receive (upload) a file to add. */
    public function upload() {
        $data['errors'] = [ ];
        $errors = 0;

        $info = array_get($_POST, 'files', null);
        if ($info === null) {
            die(json_error('the file information is missing'));
        }

        /* the filegroups info are send as json, so decode it */
        $info = json_decode($info, true);
        if ($info === null) {
            die(json_decode('the file info is not a valid json'));
        }

        /* check the course exists by ISA ID and get the DB id */
        $course_id = course_from_isa($info['course']);
        if ($course_id === null) {
            die(json_error('the course was not found'));
        }

        /* check the filegroup information form */
        $fg_check = [
            'course'   => [ 'name' => 'Course',  'type' => 'string',
                            'minlen' => 1, 'maxlen' => false ],
            'title'    => [ 'name' => 'Titre',  'type' => 'string',
                            'maxlen' => 87, 'minlen' => 5, 'clean' => true ],
            'category' => [ 'name' => 'Category',   'type' => 'numeric',
                            'in_array' =>
                                array_keys(Globals::$file_categories) ],
            'author_name' => [ 'name' => 'Author', 'type' => 'string',
                               'minlen' => 3, 'maxlen' => false ],
            'creation_date' => [ 'name' => 'date', 'type' => 'numeric' ],
        ];
        $error += check_form($fg_check, $info, $data['errors'], $results);

        $fg_info = [
            'title'          => $results['title'],
            'course_default' => $info['course'],
            'category'       => $info['type'],
            'author_default' => $info['author'],
            'weeks_defaults' => $info['weeks'],
        ];
        $f_info = [ ];

        /* check the files */
        $file_fields_check = [
            'corrections' => [ 'name' => 'Corrections',
                               'type' => 'numeric',
                               'in_array' =>
                                 array_keys(Globals::$file_corrections) ],
            'title'       => [ 'name' => 'Title', 'type' => 'string' ],
            'date'        => [ 'name' => 'Date', 'type' => 'numeric' ],
            'information' => [ 'name' => 'Comments', 'type' => 'string',
                               'maxlen' => false, 'minlen' => false,
                               'clean' => true ],
            'origin'      => [ 'name' => 'Origin', 'type' => 'numeric',
                               'in_array' =>
                                 array_keys(Globals::$file_origins) ],
        ];

        foreach ($info['files'] as $k => $file) {
            $check = $file_fields_check;
            foreach (array_keys($check) as $field) {
                $check[$field]['name'] .= " (fichier ". ($k+1) .")";
            }

            $error += check_form($check, $file, $data['errors'], $results);
        }

        /* if there was an error, stop now */
        if ($errors > 0) {
            die(json_error("some fields are incorrect:\n"
                . implode("\n", $data['errors'])));
        }


        /* XXX: need more testing. */
        /* POST: the <session> and the file along its information fields:
         * <course_ISAid>
         * <title>
         * <category>(see CPEFileCategory)
         * <author_firstname> <author_lastname>
         * <weeks[]>(see ISO 8601 week number W)
         * <type>(see CPEFileType)
         * <origin>(see CPEFileOrigin)
         * <creation_date>(unix timestamp)
         * <comment>.
         * Use the multipart/form-data to send the whole
         */
    }

    /** Rate a file. */
    public function file_rate() {
        $file_id = $this->params['fid'];
        $rate = $this->params['rate'];

        if (!in_array($rate, [ -1, 1 ])) {
            die(json_error('invalid rating value'));
        }

        $q = 'SELECT id FROM files WHERE id = :fid';
        $r = Doo::db()->query($q, [ ':fid' => $file_id ]);
        if ($r->rowCount() != 1) {
            die(json_error('the file was not found'));
        }

        Doo::db()->beginTransaction();
        try {
            $q = 'INSERT INTO filerates (file_id, user_id, rate)
                VALUES (:fid, :uid, :rate)';

            $r = Doo::db()->query($q, [ ':fid' => $file_id,
                                        ':uid' => $this->user_id(),
                                        ':rate' => $rate ]);

            $q = 'UPDATE files SET rating_cache = rating_cache + :rate
                WHERE id = :fid';

            $r = Doo::db()->query($q, [ ':fid' => $file_id, ':rate' => $rate ]);
        }
        catch (PDOException $e) {
            Doo::db()->rollback();

            if (strpos($e->getMessage(), 'Unique violation') !== false) {
                die(json_error('you already rated this file'));
            }
            else {
                die(json_error('an error happened during the rating'));
            }
        }

        Doo::db()->commit();
        die(json_encode([ 'text' => 'your rate was saved' ]));
    }

    /* Add a new post to a filegroup. */
    public function filegroup_post() {
        $filegroup_id = $this->params['fgid'];
        $warnings = [ ];

        $q =
            'SELECT p.id, p.title, p.postgroup_id, p.file_related
            FROM posts p WHERE id = (
                SELECT fg.post_id FROM filegroups fg WHERE fg.id = :fgid
            )';
        $r = Doo::db()->query($q, [ ':fgid' => $filegroup_id ]);
        if (!$ppost = $r->fetch()) {
            die(json_error('the filegroup was not found'));
        }

        /* recolt the input and set a title (use the parent post title) */
        $data = [
            'content' => array_get($_POST, 'content', null),
            'title'   => $ppost['title'], # assume the parent post pass check
        ];

        /* check the input form */
        $warns = check_form(Globals::$post_fields_check, $data,
            $warnings, $fields_cleaned);

        /* returns the form errors */
        if ($warns > 0) {
            $warns_text = array_map(function ($a) {
                return $a['text'];
            }, $warnings);

            die(json_error("some fields are incorrect:\n"
                . implode("\n", $warns_text)));
        }

        /* the API doesn't allow to create new posts */
        $q =
            'INSERT INTO posts (user_id, postgroup_id, post_id, title,
                content, file_related)
            VALUES (:uid, :pgid, :pid, :title, :content, :frel)';

        $r = Doo::db()->query($q , [
            ':uid'     => $this->user_id(),
            ':pgid'    => $ppost['postgroup_id'],
            ':pid'     => $ppost['id'],
            ':title'   => $fields_cleaned['title'],
            ':content' => $fields_cleaned['content'],
            ':frel'    => $ppost['file_related'],
        ]);

        die(json_encode([ 'text' => 'your post was saved' ]));
    }

}

?>
