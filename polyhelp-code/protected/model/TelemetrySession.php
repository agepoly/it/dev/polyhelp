<?php
/** 
 *  A telemetry session. 
 *  
 *  We separate the session system for telemetry from the user sessions table or anything that
 *  directly identifies the individual using this session (e.g. gaspar, sciper, IP, full user agent).
 */

Doo::loadCore('db/DooSmartModel');

class TelemetrySession extends DooSmartModel {

	/** Cookie name for telemetry session. */
    const COOKIE_NAME = 'tsession';

    /** Telemetry cookie duration in seconds. */
    const COOKIE_EXPIRE = 604800; # 1 week, preferably equal to $cookie_expire

	public $id;
	public $cookie;
	public $unit;
	public $browser;
	public $platform;
	public $device;

	public $_table = 'telemetry_sessions';
	public $_primarykey = 'id';
	public $_fields = array('id', 'cookie', 'unit', 'browser', 'platform', 'device');
	
	public function __construct() {
		parent::$className = __CLASS__;
	}

	/**
	 *  Find a telemetry session's ID by its telemetry session cookie.
	 * 
	 *  Returns: the corresponding session ID if the cookie value was found, null if not.
	 */
	public static function id_from_cookie($cookie) {
		if (is_null($cookie)) {
			return null;
		}

		$session_finder = new TelemetrySession();
		$session_finder->cookie = $cookie;
		$session = $session_finder->find(array('limit' => 1));
		
		if ($session === false) {
			return null;
		} else {
			return $session->id;
		}
	}

}

?>