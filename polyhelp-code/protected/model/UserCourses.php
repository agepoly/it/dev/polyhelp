<?php
/*
* HA user-course relation.
*
*	It associates the user with his courses, so it's possible to show only his courses
*
*
*/

Doo::loadCore('db/DooModel');

class UserCouses extends DooModel{

	public $id;
	public $user_id;
	public $course_id;
	public $date_added;

	public $_table = 'user_courses';
	public $_primarykey = 'id';
	public $_fields = array('id', 'user_id', 'course_id', 'date_added');

	public function __construct() {
		parent::$className = __CLASS__;
	}
}

?>