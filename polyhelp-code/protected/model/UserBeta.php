<?php
Doo::loadCore('db/DooSmartModel');

class UserBeta extends DooSmartModel{
    public $id;
    public $beta_id;
    public $user_id;

    public $_table = 'user_beta';
    public $_primarykey = 'beta_id';

    public $_fields = array('id',
						    'beta_id',
                            'user_id'
					    );

    function __construct(){
    	parent::$className = __CLASS__;
    }

    public function get_by_id(){
        if(intval($this->id)<=0)
            return null;
        return Doo::db()->find($this, array('limit'=>1));
    }
}
?>