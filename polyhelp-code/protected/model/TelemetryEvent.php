<?php
/** 
 *  A telemetry event. 
 *  
 *  Forms the core of the telemetry system. It is always associated to a session. 
 *  An event's type can be anything: loading of a document, typing of a search query, clicking on a search result...
 */

Doo::loadCore('db/DooModel');

class TelemetryEvent extends DooModel {
	
	public $id;
	public $session_id;
	public $time;
	public $type;

	public $_table = 'telemetry_events';
	public $_primarykey = 'id';
	public $_fields = array('id', 'session_id', 'time', 'type');
	
	public function __construct() {
		parent::$className = __CLASS__;
	}

}

?>