<?php
Doo::loadCore('db/DooSmartModel');

class Beta extends DooSmartModel{
    public $id;
    public $name;
    public $description;

    public $_table = 'beta';
    public $_primarykey = 'id';

    public $_fields = array('id',
						    'name',
                            'description'
					    );

    function __construct(){
    	parent::$className = __CLASS__;
    }

    public function get_user($sciper) {
        $req = array(   'where'=>'sciper=?',
                        'param'=>array($sciper),
                        'limit' => 'first'
                    );

        return Doo::db()->relate('User', __CLASS__, $req);
    }

    public function find_users(){
        $req = array(   'where'=>'beta_id=?',
                        'param'=>array($this->$id)
                    );

        return Doo::db()->relate('User', __CLASS__, $req);
    }

    public function get_by_id(){
        if(intval($this->id)<=0)
            return null;
        return Doo::db()->find($this, array('limit'=>1));
    }

    public static function userHasAccesToBeta($sciper, $beta_name){
        Doo::loadModel('Beta');
        $beta = Beta::_getOne(array('where'=>'name=\''.$beta_name.'\''));

        Doo::loadModel('User');
        $user = User::_getOne(array('where'=>'sciper=\''.$sciper.'\''));

        return ($user->userIsSubcribedTo($beta->id));
    }
}
?>