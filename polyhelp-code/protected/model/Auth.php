<?php
/**
 *  Manages authentication and login.
 * 
 *  Login happens in 2 steps: 
 * 
 *  1. We send the user to Tequila (to_tequila)
 *     If the user is not authenticated on Tequila, Tequila shows a login form.
 *     Otherwise the user is directly sent back to us.
 *  
 *  2. We catch the user back from Tequila (from_tequila)
 *     Here we get to fetch the user information we wanted from Tequila.
 * 
 *  The private methods (associate_session, associate_user, db_ldap_map, update_user, create_user, 
 *  generate_session) are mostly from legacy code moved from EPFHelpParentController.
 */

class Auth {

	/**
	 *  Send the user to Tequila.
	 * 
	 *  Returns: URL where the user should be sent to, or null if Tequila is unavailable.
	 */
	public static function to_tequila() {
		$teq = new Tequila();
		$teq_key = $teq->request(Doo::conf()->APP_URL . '/login/fetch');
		if ($teq_key) {
			return $teq->auth_url($teq_key);
		} else {
			return null;
		}
	}

	/**
	 *  Catch the user back from Tequila.
	 * 
	 *  Returns: array with current user's info if user is authenticated, null otherwise.
	 */
	public static function from_tequila() {

		// Tequila gives us its key as a GET parameter
		$key = array_get($_GET, 'key', null);

		$teq = new Tequila();
		$info = $teq->fetch($key);

		// Expecting $info array as follows:
		// [first_name, last_name, sciper, student, affi (unit), dn (gaspar), email]
		
		/* Match the info we have with the users table */
		try {
			$user_id = Auth::associate_user($info);
		}
		catch (UnexpectedValueException $e) {
			// die($e->getMessage());
			return null;
		}

		// Generate new session for this user
		$session_hash = Auth::generate_session($user_id, $info['gaspar']);
		if ($session_hash === null) {
			return null;
			throw new ErrorException('Could not save session.');
			exit;
		}

		// Store session
		setcookie(Globals::$cookie_name, $session_hash, 
			time() + Globals::$cookie_expire, '/');
		$_COOKIE[Globals::$cookie_name] = $session_hash;

		$info['user_id'] = $user_id;
		return $info;
	}

	/**
	 *  Check if the user is authenticated.
	 * 
	 *  Returns: the current user's ID if user is authenticated, null otherwise.
	 */
	public static function check() {
		$session_hash = array_get($_COOKIE, Globals::$cookie_name, null);
		if ($session_hash !== null) {
			$user_id = Auth::associate_session($session_hash);
			/* if the association by session is successful then we are done */
			if ($user_id !== null) {
				///* bind the user to the found session */
				return $user_id;
			}
		}
		return null;
	}

	/**
	 *  Find the current user based on session ID. 
	 * 
	 *  Returns: the current user's id, or null if session ID invalid.
	 */
	private static function associate_session($session_hash) {
		
		/* get the user_id associated with this sesion_id */
		$q = 'SELECT user_id,
				extract(\'epoch\' from expiration) as expiration
			FROM user_sessions
			WHERE id = :ss';
		$r = Doo::db()->query($q, Array(':ss' => $session_hash));

		/* check the entry exists */
		if ($r->rowCount() == 1) {
			$session = $r->fetch();

			/* check the entry hasn't expired */
			if ($session['expiration'] >= time()) {
				return $session['user_id'];
			}

			/* in other case we profit to delete all expired entries */
			$q = 'DELETE FROM user_sessions WHERE expiration < now()';
			Doo::db()->query($q);
		} else {
			return null;
		}
	}

	/**
     * Associate the user to our DB based on LDAP, returns the user_id.
     *
     * We can ask our function to just return it and to not modify anything ($update = false).
     */
    private static function associate_user($ldap_user, $update=true) {
    	if (empty($ldap_user)) return null;

        /* we search the user based on his name and his UID */
        $q =
            'SELECT id, gaspar
            FROM users
            WHERE last_name || \' \' || first_name = :name OR gaspar = :gaspar';

        $r = Doo::db()->query($q, 
                Array( 
                    ':name' => $ldap_user['last_name'] . 
                    ' '. $ldap_user['first_name'], ':gaspar' => $ldap_user['gaspar'] ));
        $rows = $r->rowCount();

        /* if multiple users match, then abort, this should not happen */
        if ($rows > 1) {
            if (!$update) return null;

            throw new UnexpectedValueException("Erreur: trouvé {$rows} entrées "
                ."utilisateurs identiques, impossible de distinguer");
        }
        /* if there is only one match, then we found our user */
        else if ($rows == 1) {
            /* Sync user data in DB with LDAP data */
            $entry = $r->fetch();

            if ($update) {
                Auth::update_user($entry['id'], $ldap_user);
            }

            return $entry['id'];
        }
        /* if the user was not found, then we can create it */
        else {
            if (!$update) return null;

            return Auth::create_user($ldap_user);
        }
    }

    /** Generic map for the database. */
    private static function db_ldap_map($ldap_user) {
        return Array(
            ':lname'    =>  $ldap_user['last_name'],
            ':fname'    =>  $ldap_user['first_name'],
            ':sciper'   =>  $ldap_user['sciper'],
            ':gaspar'   =>  strtolower($ldap_user['gaspar']),
            ':prof'     =>  booltostring(!$ldap_user['student']),
            ':enabled'  =>  booltostring($ldap_user['student']),
            ':degree'   =>  array_get($ldap_user, 'degree', 0),
            ':email'    =>  $ldap_user['email'],
            ':s_ldap'   =>  array_get($ldap_user, 'section', '---')
        );
    }

    /** Update a user information based on the LDAP entry. */
    private static function update_user($user_id, $ldap_user) {
        $params = Auth::db_ldap_map($ldap_user);

        // Get faculty_id & section_id in DB corresponding to LDAP unit
        $q = 'SELECT faculties.id faculty_id, sections.id section_id
                FROM faculties, sections
                WHERE faculties.id = sections.faculty_id AND sections.ldap_short = :s_ldap';
        $r = Doo::db()->query($q, array(':s_ldap' => array_get($ldap_user, 'section', '---')));
        if ($r->rowCount() != 1) {
            $faculty_id = 0;
            $section_id = 0;
        } else {
            $row = $r->fetch();
            $faculty_id = $row['faculty_id'];
            $section_id = $row['section_id'];
        }

        // Sync user in DB with LDAP values
        $q =
            'UPDATE users
            SET last_name = :lname, first_name = :fname, sciper = :sciper, gaspar = :gaspar,
                professor = :prof, enabled = :enabled, email = :email, 
                faculty_id = :faculty_id, section_id = :section_id, degree = :degree,
                creation_time = coalesce(creation_time, now())
            WHERE users.id = :user_id AND :s_ldap = :s_ldap';

        $r = Doo::db()->query($q, array_merge($params, Array(
                    ':user_id'  =>  $user_id,
                    ':faculty_id' => $faculty_id,
                    ':section_id' => $section_id,
                )));
        if ($r->rowCount() != 1) {
            die("Erreur: l'utilisateur n'a pas été mis à jour, introuvable?");
        }
    }

    /** Add the user based on the LDAP information and returns his user_id. */
    private static function create_user($ldap_user) {
        $q =
            'INSERT INTO users
            (last_name, first_name, sciper, professor, enabled, degree,
                section_id, faculty_id, email, creation_time, gaspar)
            (SELECT :lname, :fname, :sciper, :prof, :enabled, :degree,
                        s.id, f.id, :email, now(), :gaspar
                FROM faculties f, sections s
                WHERE s.ldap_short = :s_ldap AND f.id = s.faculty_id
                LIMIT 1
            )';

        $params = Auth::db_ldap_map($ldap_user);
        $r = Doo::db()->query($q, $params);
        if ($r->rowCount() != 1) {
            /* if we didn't die here, the user could spoof an other session */
            die("Erreur: l'utilisateur n'a pas pu être ajouté");
        }

        $user_id = Doo::db()->lastInsertId('users_id_seq');
        return $user_id;
    }

    /** Generate and register a new session. */
    private static function generate_session($user_id, $gaspar) {
        /* random session to identify this user */
        $random_session = hash('sha256',
            mt_rand() . hash('md5', $gaspar) . microtime(true));

        /* register the session in the database */
        $q = 'INSERT INTO user_sessions (id, user_id, expiration)
            VALUES (:ss, :uid, now() + :expiration)';

        try {
            $r = Doo::db()->query($q, Array(
                ':ss' => $random_session,
                ':uid' => $user_id,
                ':expiration' => Globals::$cookie_expire . ' seconds',
            ));
        }
        catch (PDOException $e) {
            return null;
        }

        return $random_session;
    }
}
