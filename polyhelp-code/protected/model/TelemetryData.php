<?php
/** 
 *  A telemetry data entry. 
 *  
 *  Optional data as a key-value pair associated to a telemetry event.
 *  (Multiple entries with same key may be considered as a set?)
 */

Doo::loadCore('db/DooModel');

class TelemetryData extends DooModel {
	
	public $id;
	public $event_id;
	public $attr;	// Not named "key/value" to avoid SQL reserved keywords
	public $val;

	public $_table = 'telemetry_data';
	public $_primarykey = 'id';
	public $_fields = array('id', 'event_id', 'attr', 'val');
	
	public function __construct() {
		parent::$className = __CLASS__;
	}

	/** 
	 *  Insert data associated with an event. 
	 * 
	 *  $data_array : associative array with key-value pairs. If a value is an array, each item
	 *  	in the array is inserted as a row indexed by the key, ordered by the id field.
	 * 
	 *  Returns: number of key-value pairs inserted.
	 */
	public static function insert_data($data_array, $event_id) {
		// Raw query building is better than using DooSmartModel->insert() many many times
		$q = 'INSERT INTO telemetry_data (event_id, attr, val) VALUES ';
		$params = array();
		
		// Prepare query and parameters for Doo
		$i = 0;
		foreach ($data_array as $attr => $val) {
			if (is_array($val)) {
				// Insert one row per item in $val
				foreach ($val as $item) {
					$q .= ($i ? ', ': '') . '(?, ?, ?)';
					array_push($params, $event_id, $attr, $item);
					++$i;
				}
			} else {
				if (is_bool($val)) {
					$val = $val ? 'true' : 'false';
				}
				
				$q .= ($i ? ', ': '') . '(?, ?, ?)';
				array_push($params, $event_id, $attr, $val);
				++$i;
			}
		}
		
		if ($i) {
			Doo::db()->query($q, $params);
		}

		return $i;
	}

}

?>