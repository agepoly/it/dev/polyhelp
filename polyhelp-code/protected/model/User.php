<?php
Doo::loadCore('db/DooSmartModel');

class User extends DooSmartModel{
    public $id;
    public $section_id;
    public $faculty_id;
    public $degree;
    public $avatar;
    public $first_name;
    public $last_name;
    public $creation_time;
    public $professor;
    public $superadmin;
    public $score;
    public $enabled;
    public $connected;
    public $sciper;
    public $gaspar;
    public $email;

    public $_table = 'users';
    public $_primarykey = 'id';

    public $_fields = array('id',
						    'section_id',
						    'faculty_id',
						    'degree',
						    'avatar',
						    'first_name',
						    'last_name',
						    'creation_time',
						    'professor',
						    'superadmin',
						    'score',
						    'enabled',
						    'connected',
						    'sciper',
						    'gaspar',
						    'email'
					    );

    public function get_beta ($id) {
    	$req = array(   'where'=>'user-id=?',
                    'param'=>array($id),
                    'limit' => 'first'
                );

        return Doo::db()->relate('User', __CLASS__, $req);
    }

    public function userIsSubcribedTo( $beta_id){
		Doo::loadModel('UserBeta');
		$ub = new UserBeta();
		$ub->beta_id = $beta_id;
		$ub->user_id = $this->id;

		$result = Doo::db()->find($ub, array('limit'=>1));

		return ($result != null);
    }

    function __construct(){
    	parent::$className = __CLASS__;
    }

    public function get_by_id(){
        if(intval($this->id)<=0)
            return null;
        return Doo::db()->find($this, array('limit'=>1));
    }
    
    public function subscribeToBeta($beta_id){
		Doo::loadModel('UserBeta');
		$ub = new UserBeta();
		$ub->beta_id = $beta_id;
		$ub->user_id = $id;

		$result = Doo::db()->find($ub, array('limit'=>1));
		if($result){
		    return false;
		}else{
		    return Doo::db()->insert($ub);
		}
    }
    
    public function unsubscribeToBeta($beta_id){
		Doo::loadModel('UserBeta');
		$ub = new UserBeta;
		$ub->beta_id = $beta_id;
		$ub->user_id = $id;

		Doo::db()->delete($ub);
    }
}
?>