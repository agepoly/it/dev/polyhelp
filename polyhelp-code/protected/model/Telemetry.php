<?php
/** 
 *  Handles high-level telemetry operations. 
 *  
 *  This model should be used in day-to-day use instead of the data-access models 
 *  TelemetryEvent, TelemetrySession & TelemetryData.
 * 
 *  This class is tightly coupled with TelemetryEvent, TelemetrySession & TelemetryData.
 * 
 *  Usage of this class is slightly coupled with our business logic, where we *need to know* the
 *  unit of the user (e.g. IN-BA5) before allowing a session start. This means anything that uses
 *  Telemetry::start($gaspar, $unit) will need to change when our business logic changes.
 */

class Telemetry {

	/**
	 * Emergency switch in case Telemetry starts getting a consciousness of its own.
	 * 
	 * When false, the entire Telemetry class does nothing.
	 * Note: this does not change the behavior of the "sub-models".
	 */
	const ENABLED = true;
	
	/** 
	 *  Log an event with its associated data, automatically starting a session if that was not done before
	 *  Usage: Telemetry::log('clicky', array('awesome' => 'no'));
	 * 
	 *  Returns: true if event was logged successfully, false otherwise
	 */
	public static function log($type, $data_array = array(), $cookie = null) {
		if (!self::ENABLED) {
			return false;
		}

		Doo::loadModel('TelemetryEvent');
		Doo::loadModel('TelemetrySession');
		Doo::loadModel('TelemetryData');

		// Get default $cookie from $_COOKIE
		if (is_null($cookie)) {
			$cookie = array_get($_COOKIE, TelemetrySession::COOKIE_NAME, null);
		}

		// Get session_id from DB if cookie is valid, or start a session
		$session_id = TelemetrySession::id_from_cookie($cookie);
		if (is_null($session_id)) {
			$session_id = Telemetry::start();
		}

		// Was telemetry able to start? If not, abort.
		if (is_null($session_id)) {
			return false;
		}

		// Insert event
		$event = new TelemetryEvent();
		$event->session_id = $session_id;
		$event->type = $type;
		$event->insert();

		// Insert associated data
		TelemetryData::insert_data($data_array, Doo::db()->lastInsertId('telemetry_events_id_seq'));

		return true;
	}

	/** Start a new telemetry session (will overwrite any ongoing session)
	 * 
	 *  $gaspar : if provided, uses this value instead of looking it up in a hackish way (slow)
	 *  $unit : if provided, uses this value instead of looking it up via LDAP (slow)
	 *
	 *  Returns: ID of the telemetry session, or null if the session could not be started
	 */
	public static function start($gaspar = null, $unit = null) {
		if (!self::ENABLED) {
			return null;
		}

		Doo::loadModel('TelemetrySession');

		/* Get user ID from sessions table if necessary */
		// (dirty SQL work essentially copy-pasted from EPFHelpParentController->associate_session)
		// FIXME Refactor into good model objects!
		if (is_null($gaspar)) {
			$q = 'SELECT users.gaspar, extract(\'epoch\' from user_sessions.expiration) AS expiration 
					FROM user_sessions, users
					WHERE user_sessions.user_id = users.id AND user_sessions.id = :session_id';
        	$r = Doo::db()->query($q, Array(
        		':session_id' => array_get($_COOKIE, Globals::$cookie_name, null)
        	));
        	if ($r->rowCount() == 1) {
        		$session = $r->fetch();
        		if ($session['expiration'] >= time()) {
        			// Found valid gaspar
        			$gaspar = $session['gaspar'];
        		}
        	}
        	unset($q, $r, $session);
		}

		// Abort telemetry if user is superadmin
		// FIXME Ugly SQL again! Refactor this into a proper User model!
		$q = 'SELECT id FROM users WHERE superadmin = TRUE AND gaspar = :gaspar';
		$params = array(':gaspar' => $gaspar);
		if (Doo::db()->query($q, $params)->rowCount() >= 1) {
			return null;
		}
		unset($q, $params);

		// Find out the user's unit (via LDAP if needed)
		if (is_null($unit)) {
			$unit = '';
			if (!is_null($gaspar)) {
				$ldap = new LDAP();
				if ($ldap->main_connect()) {
					$user_info = $ldap->user_info($gaspar);
					$unit = $user_info['ou'];
				}
			}
			unset($user_info);
		}

		// By this point, it is still possible that $gaspar == null, thus making $unit == '' ...
		// We'll start telemetry anyways.

		// Get info on user's technology
		$browser = getBrowser();
		$detect = new MobileDetect();

		// Save session to DB & client
		$session = new TelemetrySession();
		$session->cookie = uniqid('', true);
		$session->unit = $unit;
		$session->browser = $browser['name'];
		$session->platform = $browser['platform'];
		$session->device = $detect->isMobile() ? 'mobile' : ($detect->isTablet() ? 'tablet' : 'computer');
		$session->insert();

		setcookie(TelemetrySession::COOKIE_NAME, $session->cookie, time() + TelemetrySession::COOKIE_EXPIRE, '/');
		$_COOKIE[TelemetrySession::COOKIE_NAME] = $session->cookie;
		
		return Doo::db()->lastInsertId('telemetry_sessions_id_seq');
	}

	/** 
	 *  Stop current telemetry session by deleting its cookie.
	 */
	public static function stop() {
		if (!self::ENABLED) {
			return null;
		}

		setcookie(TelemetrySession::COOKIE_NAME, "", time() - 3600);
	}

}

?>