<?php
/**
 * Defines the site pages (routes along its Controller).
 */

/* Home */
//Checks if user has visited site before, is yes, then redirect towards last open file, if not, go homepage!
$route['*']['/'] = array('MainController',    'index');
//$route['*']['/lt/:gaspar'] = array('MainController', 'lt');	// LDAP tester -- comment out when you're done using it


/* Errors */
$route['*']['/error'] = array('ErrorController', 'error');
$route['*']['/error/:code'] = array('ErrorController', 'error');
$route['*']['/error_404'] = array('ErrorController', 'error_404');


/* Upload */
$route['get' ]['/upload/cnew/:sid/:cid'] =  array('UploadController',  'start');
$route['get' ]['/upload/add/:fgid'] =  array('UploadController',  'start');
$route['post']['/upload/new']       =  array('UploadController',  'new_post');
$route['get' ]['/upload/edit/:ids'] =  array('UploadController',  'edit');
$route['post']['/upload/edit/:ids'] =  array('UploadController',  'edit');


/* Support */
//$route['get']['/support/section/:sid'] = array('SupportController', 'section');
//$route['get']['/support/course/:cid'] = array('SupportController', 'course');
//$route['get']['/search'] = array('SupportController', 'search');
//$route['post']['/search'] = array('SupportController', 'search');
/* -- files */
$route['get']['/support/files/:fgid'] = array('SupportController', 'file');
$route['get']['/support/files/:fgid/:fid'] = array('SupportController', 'file');
$route['post']['/support/zippack'] = array('SupportController', 'zippack');
$route['get']['/support/download/:fid'] = array('SupportController', 'download');
/* -- posts */
//$route['get']['/support/post/view/:pid'] = array('ForumController', 'post');
//$route['post']['/support/post/add'] = array('ForumController', 'post');
//$route['get']['/support/post/new/:pgid'] = array('ForumController', 'new_post');
//$route['*']['/support/post/edit/:pid'] = array('ForumController', 'edit_post');
///* -- postgroup/forum */
//$route['get']['/support/postgroup/:pgid'] = array('ForumController', 'view_postgroup');


/* Ajax */
# -- favorites
$route['*']['/ajax/favcourse/add/:cid'] = array('AjaxController', 'favcourses_add');
$route['*']['/ajax/favcourse/rem/:cid'] = array('AjaxController', 'favcourses_rem');
$route['*']['/ajax/favcourse/black/:cid'] = array('AjaxController', 'favcourses_black');
$route['*']['/ajax/files/vote/:fid/:rate'] = array('AjaxController', 'file_vote');
# -- forms
$route['post']['/ajax/report'] = array('AjaxController', 'report');
# -- posts
$route['*']['/ajax/post/rem/:pid'] = array('AjaxController', 'delete_post');
# -- admin
$route['*']['/ajax/report/delete/:rid'] = array('AjaxController', 'delete_problem');
$route['*']['/ajax/rights/rem/:rid'] = array('AjaxController', 'delete_right');
$route['*']['/ajax/files/rem/:fid'] = array('AjaxController', 'delete_file');
# -- Search
$route['*']['/ajax/search/:term'] = array('AjaxController', 'search_results');
# -- File explorer
$route['*']['/ajax/explorer/default_path'] = array('AjaxController', 'explorer_default_path');
$route['*']['/ajax/explorer/list/:path'] = array('AjaxController', 'explorer_list');
$route['*']['/ajax/explorer/list'] = array('AjaxController', 'explorer_list');
#-- users
$route['*']['/ajax/user/info/:uid'] = array('AjaxController', 'user_info');
$route['*']['/ajax/user/info'] = array('AjaxController', 'user_info');
# -- Telemetry
$route['*']['/ajax/t/explorer/navigate/:path'] = array('AjaxController', 'telemetry_explorer_navigate');
$route['*']['/ajax/t/search/:term'] = array('AjaxController', 'telemetry_search');
$route['*']['/ajax/t/search/access/:index'] = array('AjaxController', 'telemetry_search_access');
$route['*']['/ajax/t/upload/panel'] = array('AjaxController', 'telemetry_upload_panel');
$route['*']['/ajax/t/upload/panel/:fgid'] = array('AjaxController', 'telemetry_upload_panel');




/* Users profile */
$route['get']['/login'] = array('EPFHelpParentController', 'login');
$route['get']['/login/fetch'] = array('EPFHelpParentController', 'login_fetch');
$route['get']['/profile/:uid'] = array('ProfileController', 'user');
$route['get']['/logout'] = array('EPFHelpParentController', 'logout');


/* User preferences/settings */
$route['*']['/prefs'] = array('PrefsController', 'prefs');
# -- admin access for user edition
$route['*']['/profile/edit/:uid'] = array('PrefsController', 'prefs');


/* Admin */
$route['get']['/admin'] = array('AdminController', 'start');
$route['get']['/admin/files'] = array('AdminController', 'full_files');
$route['get']['/admin/posts'] = array('AdminController', 'full_posts');

///* Beta */
//$route['get']['/beta/admin'] = array('BetaController', 'showIndex');
//$route['get']['/beta/admin/edit'] = array('BetaController', 'showBetaEditor');
//$route['get']['/beta/users/admin/edit'] = array('BetaController', 'showBetaUserEditor');
//$route['get']['/beta/admin/list'] = array('BetaController', 'searchBeta'); //AJAX
//$route['get']['/beta/users/admin/list'] = array('BetaController', 'searchBetaUser'); //AJAX
//$route['post']['/beta/admin/insert'] = array('BetaController', 'insertBeta');
//$route['post']['/beta/users/admin/insert'] = array('BetaController', 'insertBetaUser');

/* API */
// APIController is broken after refactoring login code into Auth. 
// Not considering fixing because we might be changing APIs anyways.
// $route['post']['/api/gentoken'] = array('APIController', 'gentoken');
// $route['post']['/api/gensession'] = array('APIController', 'gensession');
// $route['post']['/api/filegroups/course/:isaid'] = array('APIController', 'filegroups');
// $route['post']['/api/file/download/:fid'] = array('APIController', 'file_download');
// $route['post']['/api/upload'] = array('APIController', 'upload');
// $route['post']['/api/file/rate/:fid/:rate'] = array('APIController', 'file_rate');
// $route['post']['/api/filegroup/post/:fgid'] = array('APIController', 'filegroup_post');
?>
