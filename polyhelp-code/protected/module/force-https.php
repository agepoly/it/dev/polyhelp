<?php

// Redirects anything else to https://polyhelp.epfl.ch

function force_https() {
        $hostname = $_SERVER['HTTP_HOST'];
        $protocol = $_SERVER['HTTP_X_FORWARDED_PROTO'];
        $query_string = $_SERVER['QUERY_STRING'];

        $froms = ['epfhelp.ch', 'epfhelp.epfl.ch', 'epfhelp.agepoly.ch', 'polyhelp.ch', 'polyhelp.epfl.ch', 'polyhelp.agepoly.ch'];
        if (! in_array($hostname, $froms)) {
                return;
        }

        $from = '';
        if ($protocol != 'https') {
                $from = 'http-' . $hostname;
        } else {
                if ($hostname !== 'polyhelp.epfl.ch') {
                        $from = 'https-' . $hostname;
                }
        }

        if ($from) {
                $from = $query_string ? '&from=' . $from : 'from=' . $from;
                $newhref = 'https://polyhelp.epfl.ch' . $_SERVER['SCRIPT_NAME'] . '?' . $query_string . $from;
//              echo '<!--' . $newhref . '-->';
                header("Location: $newhref");
                exit;
        }
}
force_https();
