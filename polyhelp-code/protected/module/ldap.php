<?php

/**
 * A thin wrapper around the EPFL LDAP to retrieve the informations we need
 * about any user given its UID, to login them and to retrieve the AGEpoly
 * member blacklist too.
 */

class LDAP {
    /* use the real main server */
    static private $main = Array('ip' => 'ldaps://ldap.epfl.ch', 'port' => 636);
    /* TODO: fill the agepoly server */
    static private $agepoly = Array('ip' => '', 'port' => 0);

    private $main_ds = null;
    private $agepoly_ds = null;


    /** Connect to the main server, on demand. */
    public function main_connect() {
        $this->main_ds = nofail(
            'ldap_connect', self::$main['ip'], self::$main['port']);

        /* connect to the server */
        $b = nofail('ldap_bind', $this->main_ds);
        if (!$b) return false;


        return ($this->main_ds == true);
    }

    /** Connect to the AGEpoly server, on demand. */
    public function agepoly_connect() {
        $this->agepoly_ds = nofail(
            'ldap_connect', self::$agepoly['ip'], self::$agepoly['port']);

        return ($this->agepoly_ds == true);
    }

    /** The destructor closes all pending connections. */
    function __destruct() {
        $this->close();
    }

    /** Close all connections. */
    public function close() {
        if ($this->main_ds) ldap_close($this->main_ds);
        if ($this->agepoly_ds) ldap_close($this->agepoly_ds);
    }

    /** Returns an uniformized structure for user info given the gaspar/UID. */
    public function user_info($uid) {
        /* do the real search about the user */
        $sr = ldap_search($this->main_ds, "o=epfl,c=ch", "uid=". $uid, Array(
            'sn', # surname
            'givenName', # first name
            'uid',
            'ou', # LDAP path
            'uniqueidentifier', # sciper
            'edupersonaffiliation', # eg: student
            'mail',
        ));

        /* fetch the entry if any or error */
        $info = ldap_get_entries($this->main_ds, $sr);
        if ($info['count'] != 1) {
            return null;
        }

        /* get the first entry */
        $entry = $info[0];

        /* return a beautiful uniformized structure */
        $user_info = Array(
            'first_name' => $entry['givenname'][0],
            'last_name' => $entry['sn'][0],
            'sciper' => $entry['uniqueidentifier'][0],
            'student' => ($entry['edupersonaffiliation'][0] == 'student'),
            'affi' => $entry['edupersonaffiliation'][0],
            'dn' => $entry['dn'],
            'email' => isset($entry['mail']) ? $entry['mail'][0] : '',
            'gaspar' => $uid,
            'ou' => $entry['ou'][0]
        );

        /* Match to see if the student is either in {SECTION}-{BACHELOR/MASTER}{YEAR} */
        /* user_info.semester: 0 is N/A, odd is autumn, even is summer
           user_info.degree: 0 is N/A, 1 is BA1 BA2, 2 is BA3 BA4, 3 is BA5 BA6, 4 is MA1 MA2, 5 is MA3 MA4 */
        if(preg_match('/([A-Z]+)-([A-Z]+)(\d+)/', $entry['ou'][0], $user_ou)){
            array_append($user_info, Array(
            'section' => strtolower($user_ou[1]),
            'semester' => $user_ou[3] + ($user_ou[2] == 'MA' ? 6 : 0),
            'degree' => (($user_ou[3]-1) >> 1)+1 + ($user_ou[2] == 'MA' ? 3 : 0),
            ));
        }else if(preg_match('/([A-Z]+)-([A-Z]+)/', $entry['ou'][0], $user_ou)){
            array_append($user_info, Array(
            'section' => strtolower($user_ou[1]),
            'semester' => ($user_ou[2] == "H" ? 1 : ($user_ou[2] == "E" ? 2 : 0)),
            'degree' => 0,
            ));
        }
        return $user_info;
    }

    /** Check the user has valid credentials given his dn and password. */
    public function user_login($dn, $pass) {
        //return true;
        return nofail('ldap_bind', $this->main_ds, $dn, $pass);
    }

    /** Returns whether the user is not part of AGEpoly (blacklist). */
    public function user_is_blacklisted($uid) {
        # TODO: implement using AGEpoly server
        return false;
    }

}

?>
