<?php

/**
 * Contains useful PHP functions, utilitie functions
 */

/**
 * Returns the key in the array or the default value.
 */
function array_get($array, $key, $default) {
    if   (!isset($array[$key]))  return $default;
    else                         return $array[$key];
}

/**
 * Set the array entry if it is not already the case, returns the value.
 */
function array_set(&$array, $key, $default) {
    if (!isset($array[$key])) {
        $array[$key] = $default;
    }

    return $array[$key];
}

/**
 * Returns the first value for the given key in one array, or the default one.
 */
function array_get_chain(/*$default, $key, $arrays...*/) {
    $args = func_get_args();
    $default = array_shift($args);
    $key = array_shift($args);

    foreach ($args as $array) {
        if (isset($array[$key])) return $array[$key];
    }
    return $default;
}

/**
 * Remove in-place the duplicates entries in a already-sorted array.
 */
function array_rem_duplicates(&$array, $cmp=null) {
    $len = count($array);

    if ($len < 2) return $array;

    if ($cmp == null) $cmp = function ($a,$b) { return $a==$b; };

    for ($i = $len-2; $i >= 0; --$i) {
        if ($cmp($array[$i], $array[$i+1]))
            array_splice($array, $i+1, 1);
    }

    return $array;
}

/**
 * Append the second array into the first one.
 */
function array_append(&$ar1, $ar2) {
    if (count($ar2) < 1) return;
    if (!isset($ar1)) $ar1 = array();
    $ar1 = array_merge($ar1, $ar2);
}

/**
 * Prepend the first array into the second one.
 */
function array_prepend(&$ar1, $ar2) {
    if (count($ar2) < 1) return;
    if (!isset($ar1)) $ar1 = array();
    $ar1 = array_merge($ar2, $ar1);
}

/**
 * Remove null entries in the associative array (in-place).
 */
function array_remove_null(&$ar) {
    foreach ($ar as $k => $v) {
        if ($v == null) {
            unset($ar[$k]);
        }
    }
}

/**
 * Enhance strtotime to parse date in dd/mm/yyyy format.
 */
function custom_strtotime($str) {
    $str = preg_replace('/\//', '.', $str);
    return strtotime($str);
}

/** Returns the mime part for the given file. */
function file_get_mime($filepath) {
    $info = new finfo(FILEINFO_MIME);
    $temp = explode(';', $info->file($filepath), 2);
    return $temp[0];
}

/**
 * Function to extract file information, depends on file format.
 */
function file_extract_info($file, $mime) {
    /* PDF */
    if (preg_match('/^application\/pdf/', $mime)) {
        return file_extract_info_pdf($file, $mime);
    }

    return array('error' => 'no valid extractor found');
}

/**
 * Comparison function for integer.
 */
function intcmp($a, $b) {
    return ($a-$b) ? ($a-$b)/abs($a-$b) : 0;
}

/**
 * Extract information for PDF files.
 */
function file_extract_info_pdf($file, $mime) {
    $path = $file['tmp_name'];
    $pdfinfo = exec('/usr/bin/pdfinfo "'. $path .'"', $out, $rv);

    if ($rv != 0) { /* pdfinfo has failed */
        return array('error' => 'cannot parse pdf header');
    }

    /* parse pdfinfo output */
    $info = array('error' => false);

    foreach ($out as $line) {
        $tuple = explode(':', $line, 2);
        $val = trim($tuple[1]);
        if (empty($val)) continue;
        $info[strtolower($tuple[0])] = $val;
    }

    $info['title_name'] = array_get($info, 'title', '') . ' '
        . array_get($file, 'name', '');

    /* read a sample of the content using pdftotext (first page) */
    exec('/usr/bin/pdftotext -q -f 1 -l 1 "'. $path .'" -', $lines, $rv);
    if ($rv != 0) { /* pdftotext has failed */
        $info['sample'] = '';
    }
    else {
        $info['sample'] = implode(' ', $lines);
    }

    /* try to guess the category of the file */
    foreach (Globals::$file_regexes_fields as $ar) {
        list($fields, $re, $cat, $cor) = $ar;

        //var_dump($re);

        foreach ($fields as $key) {
            if (!empty($info[$key]) && preg_match($re, $info[$key])) {
                if ($cat != null && array_get($info, 'category',    0) == 0)
                    $info['category'] = $cat;
                if ($cor != null && array_get($info, 'corrections', 0) == 0)
                    $info['corrections'] = $cor;

                if (array_get($info, 'category',    0) != 0
                 && array_get($info, 'corrections', 0) != 0)
                {
                    break 2;
                }
            }
        }
    }

    /* choose a date among the headers */
    if (array_key_exists('moddate', $info)) {
        $info['date'] = $info['moddate'];
    } elseif (array_key_exists('creationdate', $info)) {
        $info['date'] = $info['creationdate'];
    }

    /* parse and format the date if present */
    if (array_key_exists('date', $info)) {
        $time = strtotime($info['date']);
        $info['timestamp'] = $time;
        $info['date'] = date(Globals::$date_day_format, $time);
    }

    /* tidy the author part */
    if (array_key_exists('author', $info)) {
        $info['author'] =
            preg_replace('/\s+/i', ' ',
            preg_replace('/^[\s\(\[]*([^,:;]+).*$/', '${1}', $info['author']));
    }

    /* add some useful comments about the file */
    $info['comments'] = ''
        . (array_key_exists('pages', $info)
            ? "Pages: {$info['pages']}\n" : '')
        . (array_key_exists('page size', $info)
            ? "Papier: {$info['page size']}\n" : '')
        . (array_key_exists('keywords', $info)
            ? "Mot clés: {$info['keywords']}\n" : '')
        . (array_key_exists('name', $file)
            ? "Nom original: {$file['name']}\n" : '');

    /* Check if the title exist and if yes : clean the title part (remove program name, extension, etc) 
    Otherwise, but nothing to throw an error when there is the final upload*/
    if (array_key_exists('title', $info)){
        if(preg_match('/(.*)\.[^.]+$/', $info['title'], $matches) > 0){
            $info['title'] = $matches[1];
        }
        $info['title'] = ucwords(str_replace (array('_', '-', '.'), ' ', $info['title']));
    }else {
        $info = array('title' => '');
    }

    /* if we are out of luck we may try to get the course from the content of
     * the file, let's try 10 lines and hope for the best */
    $lines_count = min(count($lines), 10);
    for ($i = 0; $i < $lines_count; ++$i) {
        $line = $lines[$i];
        $line_len = strlen($line);

        if ($line_len < 4 || $line_len > 64
         || preg_match('/^(?:(?:prof|doct)\S*)?([^,-\p{Pd}\p{Mc}\p{P}]+)/iu', $line, $matches) < 1
         || strlen($matches[1]) < 4) {
             continue;
        }

        $info['sample_select'][] = trim($matches[1]);
    }

    return $info;
}

/**
 * A without-fail implementation of count.
 */
function count_nofail(&$tab) {
    if (!isset($tab)) return 0;
    else              return count($tab);
}

/**
 * Check a form given the constraints in $fields_check and the fields values in
 * $fields as an associative array (fields name => value), add errors in the
 * $errors_txt (array) as formated text and returns the number of errors.
 *
 * Some useful values can be returned through $returns array, we can try to
 * parse the format and if it successful store it so it can be used upstream.
 */
function check_form($fields_check, $fields, &$errors_txt, &$returns=null) {
    $errors = 0;

    # check every fields
    foreach ($fields_check as $k => $checks) {
        $field_value = array_get($fields, $k, null);
        $clean_value = null;

        # if the field is in a array, use the index */
        if (array_get($checks, 'index', false) !== false) {
            $field_value = $field_value[$checks['index']];
        }

        # check if the field exists
        if ($field_value === null) {
            $errors_txt[] = array(
                'text' => "Le champ {$checks['name']} devrait "
                ."être rempli",
            );
            ++$errors;
            continue;
        }

        # check numeric
        if ($checks['type'] == 'numeric') {
            if (!is_numeric($field_value)) {
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait "
                    ."être numérique",
                );
                ++$errors;
            }
            if (array_get($checks, 'in_array', false) !== false
                && !in_array($field_value, $checks['in_array']))
            {
                $values_display = implode(', ', $checks['in_array']);
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait "
                    ."avoir une valeur parmi: ". $values_display,
                );
                ++$errors;
            }
            if (array_get($checks, 'min', false) !== false
                && $field_value < $checks['min'])
            {
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait avoir une "
                    ."valeur supérieur à ". $checks['min'],
                );
                ++$errors;
            }
            if (array_get($checks, 'max', false) !== false
                && $field_value > $checks['max'])
            {
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait avoir une "
                    ."valeur inférieur à ". $checks['max'],
                );
                ++$errors;
            }
        }

        # check string
        elseif ($checks['type'] == 'string') {
            $len = strlen($field_value);
            if (array_get($checks, 'maxlen', false) !== false
             && $len > $checks['maxlen'])
            {
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait avoir une "
                    ."longueur de {$checks['maxlen']} au maximum",
                );
                ++$errors;
            }
            if (array_get($checks, 'minlen', false) !== false
             && $len < $checks['minlen'])
            {
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait avoir une "
                    ."longueur de {$checks['minlen']} au mimimum",
                );
                ++$errors;
            }
            if ($errors < 1 && array_get($checks, 'clean', false) !== false) {
                $clean_value = htmlspecialchars($field_value, ENT_COMPAT, "UTF-8");
            }
        }

        # check date format
        elseif ($checks['type'] == 'date') {
            $date = custom_strtotime($field_value);
            if ($date == null) {
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait avoir un "
                    ."format de date reconnu, essayez sous forme: jj.mm.aaaa",
                );
                ++$errors;
            }
            else {
                $clean_value = $date;
            }
        }

        # check array
        elseif ($checks['type'] == 'array') {
            if (!is_array($field_value)) {
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait avoir "
                    ."une ou plusieurs valeurs",
                );
                ++$errors;
                continue;
            }
            $len = count($field_value);
            if (array_get($checks, 'maxlen', false) !== false
             && $len > $checks['maxlen'])
            {
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait avoir "
                    ."au plus {$checks['maxlen']} valeurs",
                );
                ++$errors;
            }
            if (array_get($checks, 'minlen', false) !== false
             && $len < $checks['minlen'])
            {
                $errors_txt[] = array(
                    'text' => "Le champ {$checks['name']} devrait avoir "
                    ."au moins {$checks['minlen']} valeurs",
                );
                ++$errors;
            }
        }

        # save the sanitarized parsed value
        if (array_get($checks, 'index', false) !== false) {
            $returns[$k][$checks['index']] = $clean_value;
        }
        else {
            $returns[$k] = $clean_value;
        }
    }

    return $errors;
}


/**
 * Used to import into templates files easily like in DooPHP.
 */
function dooinclude($page, $data) {
    include Doo::conf()->SITE_PATH . Doo::conf()->PROTECTED_FOLDER . "view/"
        . $page;
}

/**
 * Returns a pretty delta time text (like X minutes ago).
 *
 * original source: http://css-tricks.com/snippets/php/time-ago-function/
 */
function time_ago($time) {
    $periods = array('sec', 'mn', 'h', 'j', 'sem', 'mois', 'année');
    $lengths = array(60, 60, 24, 7, 4.35, 12);

    $difference = time() - $time;
    $len = count($lengths);

    for ($j = 0; $difference >= $lengths[$j] && $j < $len-1; ++$j) {
        $difference /= $lengths[$j];
    }

    $difference = round($difference);

    return $difference .' '. $periods[$j];
}

/**
 * Modify the given SQL query in-place to add the WHERE filter clauses.
 * The variable filters is incremented for each correct filter applied.
 */
function sql_where_append_texts(&$q, &$params, $text_filters, &$filters) {
    foreach ($text_filters as $i => $field) {
        if ($field[0]) {
            $field[0] = preg_replace('/\s+/', ' ', $field[0]);

            if ($filters > 0) $q .= 'AND ';

            /* we do a AND with the terms separated by spaces */
            $names = explode(' ', $field[0]);
            if (count($names) > 0) {
                $q .= '( ';
                foreach ($names as $k => $v) {
                    if ($k > 0) $q .= 'AND ';

                    $param = ':n'. $i .'_'. $k;
                    $q .= $field[1] .' ILIKE '. $param .' ';
                    $params[$param] = '%'. $v .'%';
                }
                $q .= ') ';

                ++$filters;
            }
        }
    }

    return $filters;
}

/**
 * Zip multiple arrays together by keys, take the common length.
 */
function array_zip() {
    $lists = func_get_args();
    $ret = array();
    $len = (count($lists) > 0) ? count($lists[0]) : 0;

    foreach ($lists as $l) {
        $len = min($len, count($l));
    }

    foreach ($lists as $l) {
        foreach ($l as $k => $v) {
            if ($k >= $len) break;

            $ret[$k][] = $v;
        }
    }

    return $ret;
}

/**
 * Check a string begins with a given pattern.
 */
function startsWith($target, $pattern) {
    return !strncmp($target, $pattern, strlen($pattern));
}

/**
 * Returns whether there exists a value v in the array such that f(v) = true.
 */
function array_exists($ar, $f) {
    foreach($ar as $v) {
        if ($f($v)) return true;
    }
    return false;
}

/**
 * Returns one big array merged from the all the subarrays.
 */
function array_flatten($arrays) {
    $r = array();

    foreach ($arrays as $v) {
        array_merge($r, $v);
    }

    return $r;
}

/**
 * Returns the array flat-mapped.
 */
function array_flatmap($array, $fn) {
    return array_reduce($array,
        function ($accu, $v) use($fn) {
            return array_merge($accu, $fn($v));
        }, array());
}

/**
 * Returns the string without the characters set.
 */
function stripchars($s, $chars, $replace="") {
    return str_replace(str_split($chars), $replace, $s);
}

/**
 * Empty NOP function.
 */
function nop() {
}

/**
 * Wrapper so the given function with the given parameters will never fail.
 */
function nofail(/*$f, $arg0, $arg1, ... */) {
    $args = func_get_args();
    $f = array_shift($args);

    /* protected the functior from having any type of error */
    $old = set_error_handler("nop");
    $r = call_user_func_array($f, $args);
    set_error_handler($old);

    return $r;
}

/**
 * Convert any variable to string (just a PHP hack).
 */
function booltostring($arg) {
    return ($arg) ? 'true' : 'false';
}

/**
 * Get the score based on the file information.
 */
function file_base_score($fe) {
    $score = 0;

    if ($fe['extension'] == 'pdf') {
        $score += 1;
    }

    $origins_genuine = array(
        Globals::$file_origin_official
    );

    if (in_array($fe['origin'], $origins_genuine)) {
        $score += 3;
    }

    return $score;
}

/**
 * Get the number of stars on 5 for the user scores.
 */
function user_score($score) {
    /*
     * Score levels:
     *  <-10  =>  skull
     *  <4   =>  0  star
     *  4    =>  1  star
     *  14   =>  2  stars
     *  50   =>  3  stars
     *  182  =>  4  stars
     *  666  =>  5  stars (was not on purpose, sorry)
     */
    if ($score < -10) {
        return -1;
    }
    else if ($score < 3) {
        return 0;
    }

    /* should not receive $score <= 0 (invalid log domain) */
    return min(floor(log($score) / 1.3), 5);
}

/** Returns the image path for the given number of stars (for users). */
function user_score_image($stars) {
    return "/global/img/scores_{$stars}.png";
}

/**
 * Get the number of stars on 5 for file scores.
 */
function file_score($score) {
    /*
     * -2 => skull
     *  3 => 1.0
     *  7 => 2.0
     * 16 => 3.0
     * 38 => 4.0
     * 95 => 5.0
     */
    if ($score < -1) {
        return -1;
    }
    else if ($score < 3) {
        return 0;
    }

    return min(floor(log($score) * 1.1), 5);
}

/** Returns the image path for the given number of stars (for files). */
function file_score_image($stars) {
    return "/global/img/scores_{$stars}.png";
}

/** Returns the image path for the given image. */
function file_star_image() {
    return "/global/img/fav.png";
}

/** Generate a json error. */
function json_error($msg, $header=true) {
    if ($header) header('Status: 400 Bad Request');

    return json_encode(array(
        'error' => 1,
        'text' => $msg,
    ));
}

/** Stream a file to the browser chunk by chunk. */
function stream_chunked_file($handle) {
    $buffer = '';

    while (!feof($handle)) {
        $buffer = fread($handle, 1048576); # 1 Mio of buffer
        echo($buffer);
        ob_flush();
        flush();
    }
}

/* Better version of get_browser that mostly works.
 * Adapted from http://php.net/manual/en/function.get-browser.php#101125
 */
function getBrowser() {
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";
    $ub = '';

    // First get the platform
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // Finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // We have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version = $matches['version'][0];
        }
        else {
            $version = $matches['version'][1];
        }
    }
    else {
        $version = $matches['version'][0];
    }

    // check if we have a number
    if ($version == null || $version == "") {
        $version="?";
    }

    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}

?>
