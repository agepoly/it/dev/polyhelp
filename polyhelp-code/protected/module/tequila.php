<?php

/**
 * Tiny tequila module to auth and fetch with it.
 */

class Tequila {

    private $curl = null;
    private static $tequilla_host = 'https://tequila.epfl.ch';
    private static $url_request = '/cgi-bin/tequila/createrequest';
    private static $url_fetch = '/cgi-bin/tequila/fetchattributes';
    private static $url_auth = '/cgi-bin/tequila/requestauth';
    private static $service_name = "PolyHelp (EPFhelp)";


    function __construct() {
        $this->curl = curl_init();
    }

    function __destruct() {
        curl_close($this->curl);
    }



    private function http($url, $data) {
        $data_ar = Array();
        foreach ($data as $k => $v) {
            $data_ar[] = $k . '=' . $v;
        }
        $data_str = implode("\n", $data_ar);

        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data_str);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($this->curl, CURLOPT_HEADER, false);

        $output = curl_exec($this->curl);
        $code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        if ($output === false || $code != 200) {
            return null;
        }

        return Array('code' => $code, 'out' => $output);
    }

    /**
     * Generate a Tequila token from the Tequila server.
     */
    public function request($return_url) {
        $data = Array(
            'request'    =>  implode(',', Array(
                'name', 'firstname', 'uniqueid', 'unit', 'unitid', 'where',
                'group', 'email', 'title',
            )),
            'service'    =>  self::$service_name,
            'urlaccess'  =>  $return_url,
        );

        $ret = $this->http(self::$tequilla_host . self::$url_request, $data);
        if ($ret === null) {
            return null;
        }

        $lines = explode("\n", $ret['out']);
        $split = explode("=", $lines[0], 2);
        return $split[1];
    }

    /**
     * Check and fetch the user attributes from Tequila after auth.
     */
    public function fetch($token) {
        $data = Array('key' => $token, );

        $ret = $this->http(self::$tequilla_host . self::$url_fetch, $data);
        if ($ret === null) {
            return null;
        }

        $lines = explode("\n", $ret['out']);
        $attrs = Array();
        foreach ($lines as $line) {
            $split = explode('=', $line, 2);
            if (count($split) != 2) continue;

            $attrs[$split[0]] = $split[1];
        }

        $wheres = explode('/', $attrs['where']);
        $where_degree = $wheres[0];
        $student = in_array('ETU', $wheres);

        $user_info = Array(
            'first_name'  =>  $attrs['firstname'],
            'last_name'   =>  $attrs['name'],
            'sciper'      =>  $attrs['uniqueid'],
            'gaspar'      =>  $attrs['user'],
            'student'     =>  $student,
            'affi'        =>  $where_degree,
            'email'       =>  $attrs['email'],
        );

        /* parse the IN-BA5 format if available */
        if (preg_match('/([A-Z]+)-BA(\d+)/', $where_degree, $user_ou)) {
            array_append($user_info, Array(
                'section' => strtolower($user_ou[1]),
                'semester' => $user_ou[2],
                'degree' => (($user_ou[2]-1) >> 1)+1,
            ));
        }

        return $user_info;
    }

    // Returns the URL to Tequila's login page
    public function auth_url($key) {
        return self::$tequilla_host . self::$url_auth . '?requestkey=' . $key;
    }

}

?>
