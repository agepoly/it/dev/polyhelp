<?php

/**
 * Defines globals and utilities funtcions used for the platform, like enum in
 * the databases, weeks and period of the semesters.
 */
class Globals {

    /**
    * Beta test mode
    **/
    static $beta_test = false;

    /**
     * Special users id
     */
    static $user_unknown_id = 1;

    /**
     * Date format used for the display in french (to the day)
     */
    static $date_day_format = 'd/m/Y';

    /**
     * Date format used for the display in french (to the second)
     */
    static $date_seconds_format = 'd/m/Y H:i:s';

    /**
     * Information about the semesters
     */
    static $semesters = Array(
        Array(
            'name' => 'Semestre d\'automne',
            'first_week'  => 38,
            'last_week'   => 51,
        ),
        Array(
            'name' => 'Semestre de printemps',
            'first_week'  => 8,
            'last_week'   => 22,
        ),
    );

    /**
     * Information about studies degree at EPFL.
     */
    static $degrees = Array(
        0 => Array('name' => 'N/A', ),
        1 => Array('name' => '1ère année Bachelor ' , ),
        2 => Array('name' => '2ème année Bachelor ' , ),
        3 => Array('name' => '3ème année Bachelor ' , ),
        4 => Array('name' => '1ère année Master '   , ),
        5 => Array('name' => '2ème année Master '   , ),
    );

    /**
     * Categories of files, corresponds to DB filegroups.category values.
     *
     * WARNING: Don't modify! These values are assumed as is.
     */
    static $file_categories = Array(
        1 => 'Exercice',
        2 => 'Examen',
        3 => 'Cours',
        4 => 'Résumé',
        6 => 'Travaux pratiques (TP)',
        7 => 'Projet',
        5 => 'Autre',
    );

    /**
     * File correction flag, corresponds to DB files.subtype values.
     *
     * WARNING: Don't modify! These values are assumed as is.
     */
    static $file_corrections = Array(
        1 => 'Donnée',
        2 => 'Donnée avec corrections',
        3 => 'Corrections',
    );

    /**
     * File correction flag, corresponds to DB files.origin values.
     *
     * WARNING: Don't modify! These values are assumed as is.
     */
    static $file_origins = Array(
        0 => 'Inconnu',
        1 => 'Officiel',    // This was moodle before
        //2 => 'Officiel',    // This is the original "Officiel"
        2 => 'Personnel',
    );

    /** Values for the above list. */
    static $file_origin_unknown    =  0;
    static $file_origin_official   =  1;
    static $file_origin_personnal  =  2;

    /**
     * Corresponds to the description of reltype in postgroups
     *
     * WARNING: Don't modify! These values are assumed as is.
     */
    static $postgroups_reltype = Array(
        0 => 'Autre',
        1 => 'Faculté',
        2 => 'Section',
        3 => 'Cours',
    );

    /**
     * Regex on file info to detect file category and corrections.
     *
     * format [ [fields], regex, category, corrections ]
     */
    public static $file_regexes_fields = Array(
        Array(
            Array('title_name' ),
            '/\.(ppt|odp|pptx|ppsx)/i', 3, null
        ),
        Array(
            Array('creator'),
            '/PowerPoint|Impress/i', 3, null
        ),
        Array(
            Array('title_name', 'sample'),
            '/(é|e|´)nonc(é|e|´)|(é|e|´)xercice|exercise|exos?(\b|$)|assignment|s(e|é|´)rie|homework/i', 1, null
        ),
        Array(
            Array('title_name', 'sample'),
            '/trava(il|ux) pratique|practice/i', 6, null
        ),
        Array(
            Array('title_name', 'sample'),
            '/(^|\b)TP(\b|$)/', 6, null
        ),
        Array(
            Array('title_name', 'sample'),
            '/(projet|project)(\b|$)/i', 7, null
        ),
        Array(
            Array('title_name', 'sample'),
            '/(partiel|partial|exa|(e|é|´)xamen|test)(\b|$)/i', 2, null
        ),
        Array(
            Array('title_name', 'sample'),
            '/formulaire|formular|r(é|e|´)sum(é|e|´)|summary/i', 4, null
        ),
        Array(
            Array('title_name'),
            '/(corr|sol)(\b|$)|corrig(e|é|´)|correct|solution/i', null, 3
        ),
    );

    /**
     * MIME list of accepted files for uploads (with its extension).
     */
    static $upload_accept_mimes = Array(
        'application/pdf'     =>  'pdf',
        'text/x-tex'          =>  'tex',
        'text/plain'          =>  'txt',
        'image/jpeg'          =>  'jpg',
        'image/png'           =>  'png',
        'application/zip'     =>  'zip',
        'application/msword'  =>  'doc',
    );

    /**
     * MIME list of accepted avatar image files.
     */
    static $avatar_accept_mimes = Array(
        'image/jpeg'          =>  'jpg',
        'image/png'           =>  'png',
    );

    /**
     * Avatar directory prefix.
     */
    static $avatar_dir = "avatars";

    /**
     * Path for the avatar files.
     */
    static function avatar_file_path($user_id, $mime) {
        return self::$avatar_dir . "/{$user_id}."
            . self::$avatar_accept_mimes[$mime];
    }

    /**
     * The max size of the avatar file size.
     */
    static $avatar_max_size = 524288; # 512 KiB

    /**
     * Path prefix of the uploaded files.
     */
    static $upload_prefix = 'files';

    /**
     * Returns a short and clean file title using file informations.
     */
    static function upload_shorttitle($filegroup_info, $file_info) {
        $filename = implode('_', 
                Array(
                    $filegroup_info['author_default'],
                    Globals::$file_categories[$filegroup_info['category']],
                    $filegroup_info['title'] 
                ));
        $filename = preg_replace('/[^a-zA-Z0-9\._-]+/', '', $filename);

        if (strlen($filename) > 65) {
            $filename = substr($filename, 0, 65);
        }

        return $filename;
    }

    /**
     * Construct the prefix path of a file group.
     */
    static function upload_file_dir($course_id, $filegroup_id) {
        return self::$upload_prefix ."/{$course_id}/{$filegroup_id}";
    }

    /**
     * Construct the path of a file according its info.
     */
    static function upload_file_name($file_id, $short_title, $extension) {
        return "{$file_id}_{$short_title}.{$extension}";
    }

    /**
     * List of problems category, corresponds to reltype of the problems table.
     *
     * WARNING: Don't modify the array root key, these are used in the code!
     */
    static $problems_types = Array(
        0 => Array(
            'name' => "Fichier",
            'link' => '/support/files/%d/%d',
            'categories' => Array(
                0 => "Fichier endommagé",
                1 => "Fichier ne respectant pas les règles",
                2 => "Fichier sans rapport",
                3 => "Fichier mal répertorié",
                4 => "Fichier erroné",
                5 => "Il y a un bug d'interface",
                6 => "Autre",
            ),
        ),
        1 => Array(
            'name' => "Groupe de fichiers",
            'link' => '/support/files/%d',
            'categories' => Array(
                0 => "Fichiers ne respectant pas les règles",
                1 => "Fichiers mal répertorié",
                2 => "Il y a un bug d'interface",
                3 => "Autre",
            ),
        ),
        2 => Array(
            'name' => "Post",
            'link' => '/support/post/view/%d#%d',
            'categories' => Array(
                0 => "Post ne respectant pas les règles",
                1 => "Post injurieux",
                2 => "Post contenant des fautes",
                3 => "Il y a un bug d'interface",
                4 => "Autre",
            ),
        ),
        3 => Array(
            'name' => "Utilisateur",
            'link' => '/profile/%d',
            'categories' => Array(
                0 => "Cet utilisateur est impoli",
                1 => "Cet utilisateur cause des problèmes",
                2 => "Cet utilisateur a un avatar inaproprié",
                3 => "Il y a un bug d'interface",
                4 => "Autre",
            ),
        ),
    );

    /**
     * The list of rights an user can have.
     *
     * Corresponds to the reltype in the user_rights table.
     * WARNING: Don't modify, the values are used in the code!
     */
    static $user_rights_type = Array(
        # all courses in the given section are granted
        0 => Array(
            'name' => 'Section',
            'require_id' => true,
            'link' => '/support/section/%d',
        ),
        # given parameter course is granted
        1 => Array(
            'name' => 'Branche',
            'require_id' => true,
            'link' => '/support/course/%d',
        ),
        # can edit and delete users
        2 => Array(
            'name' => 'Utilisateurs',
            'require_id' => false,
        ),
        # course teached by this user are granted
        3 => Array(
            'name' => 'Propres cours',
            'require_id' => false,
        ),
    );

    /** The bonus score for posting a new forum post. */
    static $score_post = 3;

    /** The bonus score for voting for a file. */
    static $score_vote = 1;

    /** The bonus score for uploading a file. */
    static $score_file = 10;

    /**
     * Takes an associative array and put inner keys outside.
     * Useful for $_FILES reversed array.
     *
     * source: php.net contributed notes
     */
    static function diverse_array($vector) {
        $result = array();
        foreach($vector as $key1 => $value1)
            foreach($value1 as $key2 => $value2)
                $result[$key2][$key1] = $value2;
        return $result;
    }

    /**
     * Convert $_FILES into a usuable array (that is: an array per form variable
     * name, then one array per file containing 'name', 'type', etc).
     */
    static function files_array($ar) {
        $files = array();
        foreach ($ar as $file_key => $file_arr) {
            $files[$file_key] = self::diverse_array($file_arr);
        }
        return $files;
    }

    /**
     * This function returns the timestamp for the given week of the year
     */
    static function time_year_week($year, $week) {
        /* get the date of the semester using ISO 8601 week dates */
        return strtotime("{$year}W" . sprintf('%02d', $week));
    }

    /**
     * Returns the first and last week of the semester as an timestamp array.
     */
    static function semester_weeks($semester, $year) {
        $arr = array();

        foreach (Array('first_week', 'last_week') as $key) {
            $arr[] = self::time_year_week($year,
                                          self::$semesters[$semester][$key]);
        }

        return $arr;
    }

    /**
     * Returs a string representating a duration in a textual french format
     */
    static function format_date_duration($start, $end) {
        return
            date(self::$date_day_format, $start) .
            ' - ' .
            date(self::$date_day_format, $end);
    }

    /**
     * Returns the semester of the given date (0 is automn, 1 is spring).
     */
    static function get_semester($date) {
        $current_week = intval(date('W', $date));
        $first_week = Array(
            self::$semesters[0]['first_week'],
            self::$semesters[1]['first_week'],
        );

        if     ($current_week >= $first_week[0])  return 0;
        elseif ($current_week >= $first_week[1])  return 1;
        else                                      return 0;
    }

    /**
     * Returns the current semester.
     */
    static function current_semester() {
        return self::get_semester(time());
    }

    /**
     * Returns the user default avatar.
     */
    static $user_avatar_default = "global/img/avatar_default.png";

    /**
     * Returns the file path in the zippack created by PHP on patch download.
     */
    static function archive_localpath($file) {
        return $file['cname'] .
            '/' . $file['fgname'] .
            '_' . $file['last_name'] .
            '-' . $file['first_name'] .
            '_' . self::$file_categories[$file['category']] .
            '_' . self::$file_corrections[$file['subtype']] .
            '.' . $file['extension'];
    }

    /** Cookie name for the session. */
    static $cookie_name = "session";

    /** Cookie duration in seconds. */
    static $cookie_expire = 604800; # 1 week

    /** Delta time after when we consider the user offline in seconds. */
    static $presence_timeout = 600; # 10 minutes

    /** IP addresses for the access. */
    static $api_addresses = Array('128.178.132.6');

    /**
     * Check the ID addresses (true means we deny).
     */
    static function protection_check_address() {
        return !in_array($_SERVER['REMOTE_ADDR'], self::$api_addresses);
    }

    /** Form checks for forum posts. */
    static public $post_fields_check = Array(
        'title'   => 
            Array(
                'name' => 'Titre',  'type' => 'string',
                'maxlen' => 64, 'minlen' => 5, 'clean' => true ),
        'content' => 
            Array(
                'name' => 'Contenu',  'type' => 'string',
                'minlen' => 3, 'clean' => true
            ),
    );


}
