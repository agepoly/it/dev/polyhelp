<?php
if (count_nofail($data['errors']) > 0 )
	$this->render('error', $data);
?>
<div class="beta default view">
	<div class="beta search field">
		<?=get_html_search_field($data);?>
	</div>
	<div class="beta list pane">

	</div>
</div>

<?php
function get_html_search_field($data){
	$html = '<input type="text" name="search_value"/>'
			.'<input type="submit" style="visibility: hidden;" />';
	$html = '<form class="beta form" method="post" action="/admin/beta/search">'.$html.'</form>';

	return $html;
}
?>