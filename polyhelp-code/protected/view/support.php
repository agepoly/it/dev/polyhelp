<p>
Dans cette partie, vous pouvez naviguer dans les facultés et les sections, pour trouver les fichiers et forum du cours que vous souhaitez.

<?php if (count_nofail($data['forums_catless']) > 0): ?>
Vous pouvez accéder aux forums généraux:
<span class="forums_catless">
<?php foreach($data['forums_catless'] as $k => $v): ?>
    <a href="/support/postgroup/<?=$v['id'];?>" class="forum">[ <?=$v['name'];?> ]</a>
<?php endforeach; ?>
</span>
<?php endif; ?>

</p>

<?php if (array_get($data, 'pref_section', Array())): ?>
<p>

<label>Accès direct à ta section:</label>

<ul class="facs">
    <li class="pref_fac facs-list">
    <span class="facs-short fac-<?=$data['pref_section']['fshort'];?>"><?=$data['pref_section']['fshort'];?></span>
    <span class="facs-long"><?=$data['pref_section']['fname'];?></span>
    <ul class="sections">
        <li class="sections-list">
        <a href="/support/section/<?=$data['pref_section']['sid'];?>">
        <?=$data['pref_section']['sname'];?>
        </a>
        </li>
    </ul>
    </li>
</ul>

<p>
<label>Accéder aux autres sections par facultés:</label>
</p>
<?php endif ?>

<?php foreach (Array(0, 1) as $p): ?>
<?php $counter = 0; ?>
<div class="facs-col">
<ul class="facs">
<?php foreach ($data['faculties'] as $k1 => $v1): ?>
<?php if ($counter++ % 2 == $p): ?>
    <li class="facs-list">
    <span class="facs-short fac-<?=$v1['short'];?>"><?=$v1['short'];?></span>
    <span class="facs-long"><?=$v1['name'];?></span>
    <ul class="sections">
        <?php foreach ($v1['sections'] as $k2 => $v2): ?>
        <li class="sections-list">
        <a href="/support/section/<?=$v2['id'];?>">
        <?=$v2['name'];?>
        </a>
        </li>
        <?php endforeach; ?>
    </ul>
    </li>
<?php endif; ?>
<?php endforeach; ?>
</ul>
</div>
</p>
<?php endforeach; ?>
