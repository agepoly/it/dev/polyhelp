<p>
Ceci est la partie administration, cette page contient principalement les principales données qui ont été envoyées dernièrement. La charge associé à la récupération des informations de cette page est assez lourde, ne rafraichissez pas cette page de manière abusive.
</p>

<?php if (isset($data['table_files'])): ?>
<div class="slider">
    <h3>Dernier Fichiers</h3>

    <div class="panel">
        <div class="right_float_action">
            <a href="/admin/files">
                Accéder à la liste entière (à utiliser avec modération)
            </a>
        </div>

        <?php $this->render('block-table', $data['table_files'], $this); ?>
    </div>
</div>
<?php endif; ?>

<?php if (isset($data['table_posts'])): ?>
<div class="slider">
    <h3>Dernier Posts</h3>

    <div class="panel">
        <div class="right_float_action">
            <a href="/admin/posts">
                Accéder à la liste entière (à utiliser avec modération)
            </a>
        </div>

        <?php $this->render('block-table', $data['table_posts'], $this); ?>
    </div>
</div>
<?php endif; ?>

<?php if (isset($data['table_reports'])): ?>
<div class="slider">
    <h3>Rapports de problèmes</h3>

    <div class="panel">
        <?php $this->render('block-table', $data['table_reports'], $this); ?>
    </div>
</div>
<?php endif; ?>

<?php if (isset($data['table_users'])): ?>
<div class="slider">
    <h3>Utilisateurs par droits</h3>

    <div class="panel">
        <?php $this->render('block-table', $data['table_users'], $this); ?>
    </div>
</div>
<?php endif; ?>
