<p>
Dans cette partie, vous pourrez chercher dans la grande collection des fichiers dans le support et parmi des posts des forums. Vous pouvez taper votre filtre dans le champs ci-dessous, le nombre de résultats est limité à 200, essayez d'ajouter des termes si trop de résultats est retournés.
</p>

Vous pouvez chercher dans une ou les deux parties du site (n'oubliez pas de cocher la recherche que vous souhaitez):
<form action="/search" method="POST">
    <div id="form">
        <div class="column file-part">
            <div class="part_checkbox">
                <input type="checkbox" name="search_file" value="1" <?=(array_get($_POST, 'search_file', $_SERVER['REQUEST_METHOD'] == 'GET') ? 'checked="checked"' : '');?> /> Fichiers
            </div>

            <div>
                <table>
                    <tr>
                        <td>
                            <label>Nom:</label>
                        </td>
                        <td>
                            <input type="text" name="file_name" value="<?=array_get($_POST, 'file_name', '');?>" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Cours:</label>
                        </td>
                        <td>
                            <input type="text" name="file_course" value="<?=array_get($_POST, 'file_course', '');?>"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Type:</label>
                        </td>
                        <td>
                            <select name="file_type">
                                <option>
                                    Tous
                                </option>
                                <?php foreach (Globals::$file_categories as $k => $v): ?>
                                <option value="<?=$k;?>" <?=($k == array_get($_POST, 'file_type', 0)) ? 'selected="selected"' : '';?> >
                                    <?=$v;?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="column post-part">
            <div class="part_checkbox">
                <input type="checkbox" name="search_post" value="1" <?=(array_get($_POST, 'search_post', false) ? 'checked="checked"' : '');?>  /> Forums
            </div>

            <div>
                <table>
                    <tr>
                        <td>
                            <label>Titre:</label>
                        </td>
                        <td>
                            <input type="text" name="post_title" value="<?=array_get($_POST, 'post_title', '');?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Contenu:</label>
                        </td>
                        <td>
                            <input type="text" name="post_content" value="<?=array_get($_POST, 'post_content', '');?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Lié à un fichier:</label>
                        </td>
                        <td>
                            <input type="checkbox" name="post_file" <?=(array_get($_POST, 'post_file', false) ? 'checked="checked' : '');?>" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="search">
            <input class="gradient_button" type="submit" value="Chercher" />
        </div>
    </div>
</form>

<?php if (count_nofail($data['errors']) > 0 ): ?>
    <?php $this->render('error', $data); ?>
<?php else: ?>

<?php if (array_get($_POST, 'search_file', false)): ?>

<form action="/support/zippack/" method="POST">
<h3>Résultats pour les fichiers</h3>

<div class="result">
    <table id="files" class="support-table">
        <thead>
            <tr class="support-header">
                <th>Nom &nbsp; &#x25BC;</th>
                <th>Cours &nbsp; &#x25BC;</th>
                <th>Type &nbsp; &#x25BC;</th>
                <th>Auteur &nbsp; &#x25BC;</th>
                <th>Date &nbsp; &#x25BC;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['files'] as $v): ?>
            <tr class="support-table-tr">
                <td>
                    <input type="checkbox" class="selfile" name="fgids[]" value="<?=$v['id'];?>" value="1" />&nbsp;
                    <a href="/support/files/<?=$v['id'];?>">
                        <?=$v['name'];?>
                    </a>
                </td>
                <td>
                    <a href="/support/course/<?=$v['cid'];?>">
                        <?=$v['cname'];?>
                    </a>
                </td>
                <td><?=Globals::$file_categories[$v['category']];?></td>
                <td><?=$v['alname'];?> <?=$v['afname'];?>
                <td data="<?=$v['ctime'];?>"><?=$v['date'];?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<p>
<input type="submit" id="zip-download" value="Télécharger" />
<span class="select-checkbox">
    <label>Sélectionner</label>:&nbsp;
    [<a href="#" class="select" onClick="select_all('.selfile'); return false;">tout</a>]
    &nbsp;
    [<a href="#" class="select" onClick="select_none('.selfile'); return false;">rien</a>]
</span>
</p>

</form>

<?php endif; ?>

<?php if (array_get($_POST, 'search_post', false)): ?>

<h3>Résultats pour les posts</h3>

<div class="result">
    <table id="posts" class="support-table">
        <thead>
            <tr class="support-header">
                <th>Nom &nbsp; &#x25BC;</th>
                <th>Forum &nbsp; &#x25BC;</th>
                <th>Auteur &nbsp; &#x25BC;</th>
                <th>Date &nbsp; &#x25BC;</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data['posts'] as $v): ?>
            <tr class="support-table-tr">
                <td>
                    <a href="/support/post/view/<?=$v['ppid'];?>#<?=$v['id'];?>">
                        <?=$v['title'];?>
                    </a>
                </td>
                <td>
                    <a href="/support/postgroup/<?=$v['pgid'];?>">
                        <?=$v['pgname'];?>
                    </a>
                </td>
                <td>
                    <a href="/profile/<?=$v['uid'];?>">
                        <?=$v['alname'];?> <?=$v['afname'];?>
                    </a>
                </td>
                <td data="<?=$v['ctime'];?>"><?=$v['date'];?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php endif; ?>

<?php endif; ?>
