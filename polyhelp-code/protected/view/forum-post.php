<?php if( count_nofail($data['errors']) > 0 ): ?>

<?php $this->render('error', $data); ?>

<?php else: ?>

<script type="text/javascript" src="/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [['${{','}}$']],
            displayMathDelimiters: [[ '@@{', '}@@' ]],
        },
        jax: ["input/TeX", "input/AsciiMath", "output/HTML-CSS", "output/NativeMML"],
        TeX: {
            extensions: ["AMSmath.js", "AMSsymbols.js", "noErrors.js", "noUndefined.js"]
        },
    });
</script>

<?php if (count_nofail($data['warnings']) > 0 ): ?>
    <?php dooinclude('block-warns.php', $data); ?>
<?php elseif (count_nofail($data['successes']) > 0 ): ?>
    <?php dooinclude('block-success.php', $data); ?>
<?php endif; ?>

<p>
Ceci est un fil de discussion dans le forum <b><?=array_get($data, 'postgroup_name', '(nom inconnu)');?></b>. Ce fil peut contenir plusieurs messages de la même thématique et vous pouvez répondre en complétant le formulaire au bas, votre message sera ajouté à la fin du fil. Les posts sont associés à une personne dont l'avatar et le profil miniature apparaît sur la droite du post correspondant.
</p>

<p class="forum-links">
Actions utiles en relation avec le support:
<ul class="links">
    <?php if (isset($data['postgroup_id'])): ?>
    <li>
        <a href="/support/postgroup/<?=$data['postgroup_id'];?>">
        Revenir au forum <b><?=$data['postgroup_name'];?></b>
        </a>
    </li>
    <?php endif; ?>

    <?php if (isset($data['course_id'])): ?>
    <li>
        <a href="/support/course/<?=$data['course_id'];?>">
        Aller au support de la branche <b><?=$data['course_name'];?></b>
        </a>
    </li>
    <?php endif; ?>

    <?php if (isset($data['section_id'])): ?>
    <li>
        <a href="/support/section/<?=$data['section_id'];?>">
        Aller au support de la section <b><?=$data['section_name'];?></b>
        </a>
    </li>
    <?php endif; ?>
</ul>
</p>

<?php if (!array_get($data, 'hide_posts', false)): ?>
<div id="posts_view">
    <?php foreach($data['posts'] as $k => $v): ?>
    <a name="<?=$v['id'];?>">&nbsp</a>
    <div class="post" dbid="<?=$v['id'];?>">
        <div class="post_header">
            <div class="header left">
                <span class="header title">
                    <?=$v['title'];?>
                </span>
                par
                <span class="header name">
                    <a href="/profile/<?=$v['user_id'];?>">
                        <?=$v['last_name'];?> <?=$v['first_name'];?>
                    </a>
                </span>
                à
                <span class="header date">
                    <?=$v['cdate'];?>
                </span>
            </div>

            <div class="header right">
            <?php if ($v['rights']): ?>
                [<a class="edit" href="/support/post/edit/<?=$v['id'];?>">Éditer</a>]
                [<a class="delete" href="/ajax/post/rem/<?=$v['id'];?>">Supprimer</a>]
            <?php endif; ?>
            </div>
        </div>

        <table class="post-container">
            <tr>
                <td class="post_content post_style">
                    <div>
                        <?=$v['content'];?>
                    </div>
                </td>

                <td class="post_user">
                    <span class="user_title">
                        <img class="avatar" src="/<?=$v['avatar'];?>" class="user_avatar" />
                        <br />
                        <a href="/profile/<?=$v['user_id'];?>">
                            <?=$v['last_name'];?> <?=$v['first_name'];?>
                        </a>
                        <br >
                        Points: <?=$v['score'];?> <img class="user_stars" src="<?=user_score_image(user_score($v['score']));?>" />
                    </span>
                </td>
            </tr>
        </table>

        <div class="clear">
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<div class="post_form">
    <p>
        Vous pouvez poster votre message avec le formulaire ci-dessous, vous pouvez mettre entre double dollars vos formules:
    </p>
    <form class="post_style" method="POST" action="/support/post/add/">
        <input type="hidden" name="action" value="<?=$data['action'];?>" />
        <input type="hidden" name="parent" value="<?=$data['parent_id'];?>" />
        <div class="field_container">
        <input class="hint field" type="text" name="title" hint="Titre du post" value="<?=array_get($_POST, 'title', '');?>" />
        </div>
        <div class="field_container">
            <textarea class="hint field" name="content" hint="Contenu du post"><?=array_get($_POST, 'content', '');?></textarea>
        </div>
        <div class="field_container centered">
            <input class="gradient_button" type="submit" value="Envoyer" />
        </div>
    </form>
</div>

<?php if (!array_get($data, 'hide_post_report', false)): ?>
<?php $this->render('block_report', $data['block_report'], $this); ?>
<?php endif; ?>

<?php endif; ?>
