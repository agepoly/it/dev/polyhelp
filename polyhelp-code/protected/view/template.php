<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<!--[if lte IE 8]
    <meta http-equiv="Refresh" content="0;url=http://www.google.ch/intl/fr/chrome/browser/">
    [endif]-->
<?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "viewc//header.php"; ?>
<body>
    <div id="page">
        <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "view//navbar.php"; ?>
        <div id="page-main" tabindex="2">
            <div id="page-content">
                <?php $this->render($data['pagename'], $data, $this); ?>
            </div>
        </div>
    </div>

    <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . 'view//condition_modal.php';?>

    <?php if (isset($_GET['pagemove1'])): ?>
        <script type="text/javascript" src="/global/js/jquery.pagemove.js"></script>
    <?php else: ?>
        <script type="text/javascript" src="/global/js/jquery.pagemove.2.js"></script>
    <?php endif; ?>

    <!-- G-Analytics -->
    <script type="text/javascript" src="/global/js/google-analytics.js"></script>
    <!-- Condition modale -->
    <script type="text/javascript" src="/global/js/condition-modal.js"></script>
</body>
</html>
