<?php if (count_nofail($data['errors']) > 0 ): ?>

<?php $this->render('error', $data); ?>

<?php else: ?>

<?php if (count_nofail($data['warnings']) > 0 ): ?>
    <?php dooinclude('block-warns.php', $data); ?>
<?php elseif (count_nofail($data['successes']) > 0 ): ?>
    <?php dooinclude('block-success.php', $data); ?>
<?php endif; ?>

<?php if ($data['action'] == 'new'): ?>
<p>
Vous allez créer un nouveau fil de conversation, c'est à dire une nouvelle entrée dans la liste des posts dans le forum <b><?=$data['postgroup_name'];?></b>, le type de ce forum est '<?=$data['reltype_desc'];?>'. Vous pouvez mettre des formules latex en les entourant de deux symboles dollars (de chaque côté).
</p>
<?php endif; ?>

<?php if ($data['action'] == 'new'): ?>
    <?php if ($data['parent_id'] == null): ?>
    <h3>Information sur le nouveau fil de discussion</h3>
    <?php else: ?>
    <h3>Information sur le nouveau post</h3>
    <?php endif; ?>
<?php elseif ($data['action'] == 'edit'): ?>
    <h3>Information sur le post à éditer</h3>
<?php endif; ?>

<div class="post_form">
<?php if ($data['action'] == 'new'): ?>
    <form class="post_style" method="POST" action="/support/post/add/">
        <input type="hidden" name="parent" value="<?=$data['parent_id'];?>" />
        <input type="hidden" name="action" value="<?=$data['action'];?>" />
<?php elseif ($data['action'] == 'edit'): ?>
    <form class="post_style" method="POST" action="/support/post/edit/<?=$data['post_id'];?>">
<?php endif; ?>
        <div class="field_container">
        <input class="hint field" type="text" name="title" hint="Titre du post" value="<?=array_get($_POST, 'title', '');?>" />
        </div>
        <div class="field_container">
            <textarea class="hint field" name="content" hint="Contenu du post"><?=array_get($_POST, 'content', '');?></textarea>
        </div>
        <div class="field_container">
            <input type="submit" value="Envoyer" />
        </div>
    </form>
</div>

<?php endif; ?>
