<?php if (count_nofail($data['errors']) > 0 ): ?>

<?php $this->render('error', $data); ?>

<?php else: ?>

<?php
    $score_html_display = '';
    $score = file_score($data['cur_file_f']['rating']);
    $nav = '<i class="icon-star icon-white"></i>';
    for($i = 0; $i<$score; $i++)
        $score_html_display .= $nav;
?>

<script type="text/javascript">
    var files = [
    <?php foreach($data['files'] as $k1 => $v1): ?>
        { 'id': <?=$v1['id'];?>, 'path': '/<?=$v1['filepath'];?>', 'ext': '<?=$v1['extension'];?>' },
    <?php endforeach; ?>
    ];
    var file_selected = <?=$data['cur_file'];?>;

    // store the url for printing
    urlStore = '/<?php echo $data['cur_file_f']['url']; ?>';
</script>
<?php $this->render('block_report', $data['block_report'], $this); ?>

<!-- <div id="view-header-small" class="row-fluid view-header-small-scrollup">
    <div id="view-header-small-generic" class="span3">
        <div id="view-header-small-generic-infos">
            <h5><?=$data['filegroup']['name'];?></h5>
            <p>Auteur: <a href="/profile/<?=$data['filegroup']['author_id'];?>"><?=$data['filegroup']['afname'];?> <?=$data['filegroup']['alname'];?></a></p>
        </div>
    </div>

    <div id="file-controller" class="span3">
        <div class="scaling" id="plus" title="Zoom+ (raccourci flèche haut)"><i class="glyphicons-icon circle_plus"></i></div>
        <div class="scaling" id="minus" title="Zoom- (raccourci flèche bas)"><i class="glyphicons-icon circle_minus"></i></div>
        <div class="movement" id="prevPage" title="Page précédente (raccourci flèche droite)"><i class="glyphicons-icon left_arrow"></i></div>
        <div class="movement" id="nextPage" title="Page suivante (raccourci flèche droite)"><i class="glyphicons-icon right_arrow"></i></div>
    </div>
    
    <div id="view-header-small-technical-share" class="span3">
        <div class="btn-group">
            <a href="#" id="share" title="Partager le fichier"><i class="glyphicons-icon new_window"></i></a>

            <?php if ($data['cur_file_f']['edit_rights']): ?>
                <a href="/upload/edit/<?=$data['cur_file_f']['id'];?>" title="Éditer le groupe de fichiers">><i class="glyphicons-icon pencil"></i><a>
            <?php endif; ?>
            <a href="#" id="report-button-smallheader" title="Le fichier présente des bugs ou ne respecte pas les règles" onclick=report_show("<?=$data['cur_file_f']['id'];?>")><i class="glyphicons-icon circle_exclamation_mark"></i></a>
            <a href="/support/download/<?=$data['cur_file_f']['id'];?>" title="Télécharger le fichier"><i class="glyphicons-icon cloud-download"></i></a>
            <a href="#" id="print2" title="Imprimer le fichier"><i class="glyphicons-icon print"></i></a>
        </div>

        <div id="share-popover" class="popover bottom popover-wrapper" style="display: none;">
            <div class="arrow"></div>
            <h3 class="popover-title">Partage cette page !</h3>
            <div class="popover-content">
                <div class='st_facebook_large share-box' displayText='Facebook' title="Partager sur Facebook"></div>
                <div class='st_googleplus_large share-box' displayText='Google +' title="Partager sur Google+"></div>
                <div class='st_twitter_large share-box' displayText='Tweet' title="Tweet"></div>
                <div class='st_email_large share-box' displayText='Email' title="Partager par mail"></div>
            </div>
        </div>
    </div>

    <div id="view-header-small-technical" class="span3">
        <div id="view-header-small-technical-infos">
            <p><strong>Uploadé par :</strong> <a href="/profile/<?=$data['cur_file_f']['uid'];?>"><?=$data['cur_file_f']['ufname'];?> <?=$data['cur_file_f']['ulname'];?></a></p>
            <p><strong>Dernières modifications :</strong> <?=$data['cur_file_f']['cdate'];?></p>
        </div>
    </div>
</div> -->

<div id="view-header" class="row-fluid">
    <div id="view-header-generic" class="span5">
        <div id="view-header-generic-infos">
            <h4><?=$data['filegroup']['name'];?></h4>
            <p><strong>Cours:</strong> <!--<a href="/support/course/<?=$data['filegroup']['course_id'];?>">--><?=$data['filegroup']['cname'];?><!--</a>--></p>
            <p><strong>Auteur:</strong> <a href="/profile/<?=$data['filegroup']['author_id'];?>"><?=$data['filegroup']['afname'];?> <?=$data['filegroup']['alname'];?></a></p>
        </div>
    
        <div id="view-header-generic-thumbs" class="btn-toolbar">
            <div class="btn-group"> 
                    <a href="/ajax/files/vote/<?=$data['cur_file_f']['id'];?>/upvote" title="Ce fichier est utile et cohérent" target="votes"><i class="glyphicons-icon thumbs_up vote_sym <?=($data['cur_file_f']['rated'])?'vote_sym_off':'vote_sym_on';?>" rate="upvote" ></i></a>
                    <a href="/ajax/files/vote/<?=$data['cur_file_f']['id'];?>/dwvote" title="Ce fichier n'est pas utile, ou possède des incohérences"target="votes"><i class="glyphicons-icon thumbs_down vote_sym <?=($data['cur_file_f']['rated'])?'vote_sym_off':'vote_sym_on';?>" rate="dwvote" ></i></a>
            </div>
            <div id="view-header-badge" class="file-score badge badge-info"><?=$score_html_display;?></div>
        </div>
    </div>

    <div id="view-header-avatar" class="span1">
        <img src="/<?=$data['posts']['0']['avatar'];?>">
    </div>
    <div id="view-header-technical" class="span6">
        <div id="edit" class="btn-toolbar">
            <div class="btn-group">
                <?php if ($data['cur_file_f']['edit_rights']): ?>
                    <a href="/upload/edit/<?=$data['cur_file_f']['id'];?>" title="Éditer le groupe de fichiers"><i class="glyphicons-icon pencil"></i><a>
                <?php endif; ?>
            </div>
        </div>
        <div id="view-header-technical-infos">
            <p style="margin-top: 20px"><strong>Uploadé par :</strong> <a href="/profile/<?=$data['cur_file_f']['uid'];?>"><?=$data['cur_file_f']['ufname'];?> <?=$data['cur_file_f']['ulname'];?></a></p>
            <p><strong>Uploadé :</strong> <?=$data['cur_file_f']['udate'];?></p>
            <p><strong>Dernières modifications :</strong> <?=$data['cur_file_f']['cdate'];?></p>
        </div>
    </div>
    
    <div id="view-header-technical-share" class="btn-toolbar">
        <div class="btn-group">
            <a href="#" id="share" title="Partager le fichier"><i class="glyphicons-icon new_window"></i></a>
            <a href="#" id="report-button-bigheader" title="Le fichier présente des bugs ou ne respecte pas les règles" onclick=report_show("<?=$data['cur_file_f']['id'];?>")><i class="glyphicons-icon circle_exclamation_mark"></i></a>
            <a href="/support/download/<?=$data['cur_file_f']['id'];?>" title="Télécharger le fichier"><i class="glyphicons-icon cloud-download"></i></a>
            <a href="#" id="print" title="Imprimer le fichier"><i class="glyphicons-icon print"></i></a>
        </div>

        <div id="share-popover" class="popover bottom popover-wrapper" style="display: none;">
            <div class="arrow"></div>
            <h3 class="popover-title">Partage cette page !</h3>
            <div class="popover-content">
                <h4>Coming soon...<h4>
            </div>
        </div>
    </div>
</div>

<div id="view-parent">
    <div id="file-view">
        <?php if ($data['cur_file_f']['extension'] == 'pdf'): ?>
        <object id="pdf-native" data="../../../<?=$data['cur_file_f']['url']?>" type="text/html" codetype="application/pdf"></object>

        <?php elseif (in_array($data['cur_file_f']['extension'],  Array('png', 'jpg') )): ?>
        <img class="view view-img" src="/<?=$data['cur_file_f']['url'];?>" />
        <?php elseif (in_array($data['cur_file_f']['extension'], Array('txt', 'tex'))): ?>
        <pre class="view view-plain"><?php readfile($data['cur_file_f']['url']); ?></pre>
        <?php else: ?>
        <span class="view">
            Le fichier ne peut être affiché au sein d'un navigateur (extension: <?=$data['cur_file_f']['extension'];?>), téléchargez le et ouvrez le avec la bonne application.
        </span>
        <?php endif; ?>
    </div>
</div>



<?php endif; ?>
