<?php if (count_nofail($data['errors']) > 0 ): ?>

<?php $this->render('error', $data); ?>

<?php else: ?>

<?php if (count_nofail($data['warnings']) > 0 ): ?>
    <?php dooinclude('block-warns.php', $data); ?>
<?php elseif (count_nofail($data['successes']) > 0 ): ?>
    <?php dooinclude('block-success.php', $data); ?>
<?php endif; ?>

<form action="<?=$_SERVER["REQUEST_URI"];?>" method="post" enctype="multipart/form-data">
<div class="row-fluid" id="prefs">
    <div class="span6">
            <?php if ($data['admin_edit']): ?>
                <h3>Édition du profil de <?=$data['user_info']['last_name'];?> <?=$data['user_info']['first_name'];?></h3>
            <?php else: ?>
                <h3>Votre profil</h3>
            <?php endif; ?>

            <table>
                <tbody>
                    <tr>
                        <td class="data-label">
                            Faculté
                        </td>
                        <td>
                            <span class="input uneditable-input"><?=$data['user_info']['fname'];?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="data-label">
                            Section
                        </td>
                        <td>
                            <select name="section">
                                <?php foreach ($data['sections'] as $k => $v): ?>
                                <option value="<?=$v['id'];?>" <?=($data['user_info']['section_id'] == $v['id'] ? 'selected="selected"' : '');?> ><?=$v['name'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="data-label">
                            Année
                        </td>
                        <td>
                            <select name="degree">
                                <?php foreach (Globals::$degrees as $k => $v): ?>
                                <option value="<?=$k;?>" <?=($data['user_info']['degree'] == $k ? 'selected="selected"' : '');?> ><?=$v['name'];?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="data-label">
                            Professeur
                        </td>
                        <td>
                            <?php if ($data['admin_edit']): ?>
                                <label>Professeur ?<input id="input-prof" type="checkbox" name="professor" value="1" <?=($data['user_info']['professor']) ? 'checked="checked"' : '';?>/></label>
                            <?php else: ?>
                                <span class="input uneditable-input"><?=($data['user_info']['professor'] ? 'Oui' : 'Non');?></span>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="data-label">
                            Score
                        </td>
                        <td>
                            <?php if ($data['admin_edit']): ?>
                                <input type="text" name="score" value="<?=$data['user_info']['score'];?>" />
                            <?php else: ?>
                                <span class="input uneditable-input"><?=$data['user_info']['score'];?></span>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php if ($data['admin_edit']): ?>
                        <tr>
                            <td class="data-label">
                                Compte
                            </td>
                            <td>
                                <label>Compte activé ? <input type="checkbox" name="enabled" value="1" <?=($data['user_info']['enabled']) ? 'checked="checked"' : '';?> /></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="data-label">
                                Login GASPAR
                            </td>
                            <td>
                                <?=$data['user_info']['gaspar'];?>
                                (<?=($data['user_info']['connected_once'] ? "s'est déjà connecté" : "ne s'est jamais connecté");?>)
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($data['superadmin_edit']): ?>
                    <tr>
                        <td class="data-label">
                            Superadmin
                        </td>
                        <td>
                            <label>Superadmin ? <input type="checkbox" name="superadmin" value="1" <?=($data['user_info']['superadmin']) ? 'checked="checked"' : '';?> /></label>
                        </td>
                    </tr>
                    <?php endif; ?>
                </tbody>
            </table>

            <h3>Adresses de contact</h3>
            <?php foreach ($data['addresses'] as $v): ?>
            <table>
                <tbody>
                    <tr>
                        <td class="data-label">
                            Titre
                        </td>
                        <td>
                            <input class="hint field" type="text" name="name[]" hint="Titre" value="<?=$v['name'];?>" />
                        </td>
                    </tr>

                    <tr>
                        <td class="data-label">
                            Adresse
                        </td>
                        <td>
                            <input class="hint field" type="text" name="address[]" hint="Adresse" value="<?=$v['value'];?>" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="data-label">
                            Description
                        </td>
                        <td>
                            <input class="hint field" type="text" name="desc[]" hint="Description" value="<?=$v['description'];?>" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php endforeach; ?>

            <h5>Ajouter :</h5>
            <table>
                <tbody>
                    <tr>
                        <td class="data-label">
                            Titre
                        </td>
                        <td>
                            <input class="hint field" type="text" name="name[]" hint="Titre" />
                        </td>
                    </tr>

                    <tr>
                        <td class="data-label">
                            Adresse
                        </td>
                        <td>
                            <input class="hint field" type="text" name="address[]" hint="Adresse" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="data-label">
                            Description
                        </td>
                        <td>
                            <input class="hint field" type="text" name="desc[]" hint="Description" />
                        </td>
                    </tr>
                </tbody>
            </table>


            <?php if ($data['superadmin_edit']): ?>
            <h3>Droits :</h3>
                <ul>
                    <?php foreach ($data['rights'] as $v): ?>
                    <li>
                        de <?=$v['type'];?> sur
                        <?php if ($v['link'] !== null): ?>
                        <a href="<?=$v['link'];?>">
                            <?=$v['title'];?>
                        </a>
                        <?php else: ?>
                        <?=$v['title'];?>
                        <?php endif; ?>

                        [<a class="right remove" href="/ajax/rights/rem/<?=$v['id'];?>" target="out">enlever</a>]
                    </li>
                    <?php endforeach; ?>
                    <?php foreach (Globals::$user_rights_type as $v => $r): ?>
                    <li>
                        Ajouter un droit de:
                        <input type="checkbox" name="rights[<?=$v;?>][enabled]" value="1" /> <?=$r['name'];?>

                        <?php if ($r['require_id']): ?>
                        <select name="rights[<?=$v;?>][relid]">
                            <?php foreach($data['rights_objects'][$v] as $o): ?>
                            <option value="<?=$o['id'];?>"><?=$o['name'];?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php endif; ?>
                    </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <input class="btn btn-large btn-primary" type="submit" value="Appliquer les modifications"></input>  
    </div>

    <div class="span6">
        <img class="avatar" src="/<?=$data['user_info']['avatar'];?>" />
        <input type="file" name="avatar" />
    </div>
</div>
</form>

<?php endif; ?>
