<?php if (count_nofail($data['errors']) > 0 ): ?>
    <?php $this->render('error', $data); ?>
<?php else: ?>

<?php if (count_nofail($data['warnings']) > 0 ): ?>
    <?php dooinclude('block-warns.php', $data); ?>
<?php elseif (count_nofail($data['successes']) > 0 ): ?>
    <?php dooinclude('block-success.php', $data); ?>
<?php endif; ?>

<?php if ($data['fg0_edit']): ?>

<form id="section-switcher" class="form-horizontal" action="/upload/edit/<?=$data['edit_list'];?>" method="POST">
        <legend><strong>Description du groupe de fichiers</strong></legend>

        <div class="control-group">
            <input type="hidden" name="section_switcher" value="1" />
            <label class="control-label">Section</label>
            <div class="controls">
                <select name="preferred_section" onchange="submit();">
                    <?php foreach($data['sections'] as $v): ?>
                        <option value="<?=$v['id'];?>" <?=($data['preferred_section'] == $v['id'] ? 'selected="selected"' : '' );?> ><?=$v['name'];?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            
        </div>

</form>
<?php endif ?>


<form id="upload-form" class="form-horizontal" action="/upload/edit/<?php echo $data['edit_list']; ?>" method="post" enctype="multipart/form-data">
        <?php if (!$data['fg0_edit']): ?>
            <input type="hidden" name="fg0_hide" value="1" />
        <?php endif; ?>
<?php if($data['fg0_edit']): ?>   
    <fieldset>
        <input type="hidden" name="preferred_section" value="<?=$data['preferred_section'];?>" />
        <?php if (isset($data['preferred_course']) ): ?>
            <input type="hidden" name="preferred_course" value="<?php echo $data['preferred_course']; ?>">
        <?php endif; ?>


        <div class="control-group">
            <label class="control-label">Branche</label>
            <div class="controls">
                <select name="fg0-course" />
                    <?php if( count_nofail($data['filegroups-0']['suggest_courses']) > 0 ): ?>
                    <optgroup label="Suggestions">
                        <?php foreach($data['filegroups-0']['suggest_courses'] as $k1=>$v1): ?>
                        <option value="<?php echo $v1['id']; ?>" <?php if( $data['filegroups-0']['course_default'] == $v1['id'] ): ?> selected="selected" <?php endif; ?> ><?php echo $v1['name']; ?></option>
                        <?php endforeach; ?>
                    </optgroup>
                    <?php endif; ?>

                    <?php foreach($data['courses'] as $k1=>$v1): ?>
                    <optgroup label="Année <?php echo $k1; ?>">
                        <?php foreach($v1 as $k2=>$v2): ?>
                        <option value="<?php echo $v2['id']; ?>" <?php if( $data['filegroups-0']['course_default'] == $v2['id'] && count_nofail($data['filegroups-0']['suggest_courses']) == 0 ): ?> selected="selected" <?php endif; ?> ><?php echo $v2['name']; ?></option>
                        <?php endforeach; ?>
                    </optgroup>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Titre du groupe de fichiers</label>
            <div class="controls">
                <input name="fg0-title" type="text" value="<?php echo $data['filegroups-0']['title']; ?>" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Type</label>
            <div class="controls">
                <select name="fg0-type">
                    <?php foreach(Globals::$file_categories as $k1=>$v1): ?>
                    <option value="<?php echo $k1; ?>" <?php if( $k1 == $data['filegroups-0']['category'] ): ?> selected="selected" <?php endif; ?> ><?php echo $v1; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Auteur</label>
            <div class="controls">
                <select name="fg0-author" />
                    <?php if( count_nofail($data['filegroups-0']['suggest_authors']) > 0 ): ?>
                    <optgroup label="Suggestions">
                        <?php foreach($data['filegroups-0']['suggest_authors'] as $k1=>$v1): ?>
                        <option value="<?php echo $v1['id']; ?>" <?php if( $data['filegroups-0']['author_default'] == $v1['id'] ): ?> selected="selected" <?php endif; ?> ><?php echo $v1['lname']; ?> <?php echo $v1['fname']; ?></option>
                        <?php endforeach; ?>
                    </optgroup>
                    <?php endif; ?>

                    <optgroup label="Autres">
                        <option value="1">Auteur inconnu</option>
                    </optgroup>

                    <?php foreach($data['authors'] as $k1=>$v1): ?>
                        <?php if(     ($k1 == 0) ): ?>
                        <optgroup label="Professeurs">
                        <?php elseif( ($k1 == 1) ): ?>
                        <optgroup label="Étudiants">
                        <?php endif; ?>

                        <?php foreach($v1 as $k2=>$v2): ?>
                        <option value="<?php echo $v2['id']; ?>" <?php if( $data['filegroups-0']['author_default'] == $v2['id'] && count_nofail($data['filegroups-0']['suggest_authors']) == 0 ): ?> selected="selected" <?php endif; ?> ><?php echo $v2['lname']; ?> <?php echo $v2['fname']; ?></option>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    
        <div class="control-group">
            <label class="control-label">Semaines concernées<label>
            <div class="controls">
                <div class="accordion" id="week-accordion">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseOne">
                                Sélectionner les semaines <i id="chevron-week" class="icon-chevron-down chevron-week"></i>
                            </a>
                        </div>
            
                        <div id="collapseOne" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <?php foreach($data['fg0-semester_weeks'] as $k1=>$v1): ?>
                                <label class="checkbox">
                                    <span class="week-tooltip" data-placement="right" data-toggle="tooltip" title ="<?php echo $v1['text']; ?>">
                                        <input name="fg0-weeks[]" class="weeks-0" type="checkbox" value="<?php echo $v1['id']; ?>" <?php if( in_array($v1['id'], $data['filegroups-0']['weeks_defaults']) ): ?> checked="checked" <?php endif; ?> /><?php echo 'Semaine ' . $v1['id'] ?>
                                    </span>
                                </label>
                                
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
<?php endif; ?>

    <?php foreach($data['files-entries-0'] as $k0=>$v0): ?>
    <fieldset>
        <legend>Informations sur <?php echo $v0['filename']; ?></legend>

        <div class="control-group">
            <label class="control-label">Contenu</label>
            <div class="controls">
                <select name="fg0-<?php echo $k0; ?>-corrections">
                    <?php foreach(Globals::$file_corrections as $k1=>$v1): ?>
                    <option value="<?php echo $k1; ?>" <?php if( $k1 == $v0['corrections'] ): ?> selected="selected" <?php endif; ?> ><?php echo $v1; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Provenance</label>
            <div class="controls">
                <select name="fg0-<?php echo $k0; ?>-origin">
                    <?php foreach(Globals::$file_origins as $k1=>$v1): ?>
                    <option value="<?php echo $k1; ?>" <?php if( $k1 == $v0['origin'] ): ?> selected="selected" <?php endif; ?> ><?php echo $v1; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Date</label>
            <div class="controls">
                <input name="fg0-<?php echo $k0; ?>-date" type="text" value="<?php echo $v0['date']; ?>" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Commentaires</label>
            <div class="controls">
                <textarea rows="10" name="fg0-<?php echo $k0; ?>-comments"><?php echo $v0['comments']; ?></textarea>
            </div>
        </div>
    </fieldset>
    <?php endforeach; ?>

    <div class="control-group">
            <div class="controls">
                 <button class="btn btn-large btn-primary" type="submit">Soumettre les fichiers</button>  
            </div>
        </div>
</form> 

<?php endif; ?>
