<p>
Dans cette partie, vous pouvez naviguer dans les cours que vous enseignez ou dont les droits vous ont été donnés.
</p>

<h3>List des cours:</h3>

<p>
    <?php $this->render('block-table', $data['table_courses'], $this); ?>
</p>
