<div id="page-accueil">
    <div id="welcome">
        <h2><a href="http://i0.kym-cdn.com/photos/images/original/000/048/783/a_winner_is_you20110724-22047-1nd3wif.jpg?1311564834" target="_blank">Bienvenue sur PolyHelp!</a></h2>
        <h3>L'équipe travaille pour t'aider à réussir. Voici les actualités.</h3>
        <table id="newsroll" class="table table-hover">
            <tr class="info"><th>31 mai</th><td>Beaucoup plus facile de partager tes cours avec les autres: maintenant, appuie sur Ctrl et upload plusieurs fichiers en même temps!</td></tr>
            <tr><th>22 avril</th><td>La connexion sécurisée (HTTPS) est activée sur le site entier, et on a pas <a href="http://heartbleed.com/">Heartbleed</a> :)</td></tr>
            <tr><th>30 mars</th><td>Le login passe maintenant par <a href="http://tequila.epfl.ch/" target="_blank">Tequila</a>, un système sûr et sécurisé qu'on connaît tous.</td></tr>
            <tr><th>14 mars</th><td>Les fichiers uploadés étaient mal classés ou ne sauvegardaient pas le titre et les autres informations que tu entrais. C'est fixé!</td></tr>
            <tr><th>28 février</th><td>On a un problème avec HTTPS, toutes les connexions ne sont plus sécurisées. On essaie de résoudre ça au plus vite.</td></tr>
            <tr><th>5 janvier</th><td>L'explorateur à gauche affiche tes cours par défaut: ton compte PolyHelp se synchronise maintenant avec gaspar pour avoir les informations à jour.</td></tr>
            <tr><th>28 décembre</th><td>La connexion sécurisée HTTPS est activée par défaut. Cherchez le petit cadenas vert!</td></tr>
            <tr><th>27 décembre</th><td>Les documents PDF s'affichent maintenant avec le visionneur natif ou le plugin PDF du navigateur, plus fiable qu'<a href="https://www.facebook.com/photo.php?fbid=10151634982779166" target="_blank">avant</a> et plus agréable à utiliser.</td></tr>
        </table>

        <img src="global/img/little-wave.jpg"></img>
        
        <p>Bug, critique ou proposition, écris un mail à <a class="" href="mailto:polyhelp@agepoly.ch">polyhelp@agepoly.ch</a> !</p>

    
        <div id="footer-beta">© <?php echo date('Y'); ?> Equipe Informatique de l'<a href="http://agepoly.epfl.ch">AGEPoly</a> | <a href="mailto:informatique@agepoly.ch?subject=Je+veux+rejoindre+l'agepinfo">on recrute!</a> | <a href="mailto:polyhelp@agepoly.ch">contact</a></div>
	</div>
</div>
