<div class="beta editor">
	<h1>Beta Editor</h1>
	<form action="#" method="post">
		<table>
			<tr>
				<td>
					<label for="name">Name</label>
				</td>
				<td>
					<input type="text" id="name" name="name" value="<?=$data['name']?>" />
				</td>
			</tr>

			<tr>
				<td>
					<label for="description">Description</label>
				</td>
				<td>
					<input type="text" id="description" name="description" value="<?=$data['description']?>" />
				</td>
			</tr>

			<tr>
				<td></td>
				<td><button type="submit" id="submit" name="submit">Submit</button></td>
			</tr>
		</table>
	</form>
</div>
