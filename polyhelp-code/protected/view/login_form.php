<!DOCTYPE HTML>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>PolyHelp Accès</title>
    <link type="text/css" rel="stylesheet" media="all" href="/global/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/global/css/style.css" />
    <script type="text/javascript" src="/global/js/force-https.js"></script>
    <script type="text/javascript" src="/global/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/global/js/bootstrap.js"></script>

</head>
<body id="login-beta">
 <div id="page-login">   

    <!--<div id="ribbon">
        <img src="global/img/beta-corner-ribbon.png"/>
    </div>-->
    <div id="login">
        
    <div class="login-header-img"></div>

        <!-- Button to trigger modal -->
        <a href="/login"><button id="login-modal-trigger" class="btn btn-danger btn-large">Connexion</button></a>
        
        <!-- Modal for login-->
        <div id="login-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
            <div class="cornerboat"></div>

            <!-- Modal header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class = "controls">
                    <h3 id="myModalLabel">Connexion</h3>
                </div>
            </div>

            <!-- Modal Body & Footer-->

            <?php if (count_nofail($data['errors'])): ?>
            <script type="text/javascript">
                $(function(){
                    $('#login-modal').modal('show');
                });
            </script>
                
            <?php foreach($data['errors'] as $v): ?>
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <?=$v['text'];?>
                </div>
            <?php endforeach; ?>
            <?php endif; ?>
            
            <form method="post" class="form-horizontal">
                <div class="modal-body">

                    <!-- Username input-->
                    <!--<div class="control-group">
                        <label for="inputUsername" class="control-label">Identifiant</label>
                        <div class="controls">
                            <input id="inputUsername" type="text" name="username" placeholder="Gaspar" required/>
                        </div>
                    </div>-->

                    <!-- Password input-->
                    <!-- <div class="control-group">
                        <label for="inputPassword" class="control-label">Mot de passe</label>    
                        <div class="controls">
                            <input id="inputPassword"type="password" name="pass" placeholder="Mot de passe" required/>
                        </div>
                    </div> -->

                    <!-- Fine print for terms of use -->
                    <!-- <p>
                        <small>En vous connectant, vous acceptez les 
                            <a class="modal_launcher" href="#">conditions d'utilisation</a>.
                        </small>
                    </p> -->
                </div>

                <div class="modal-footer">

                    <!-- Submit button-->
                    <div class="control-group">
                        <div class="controls">
                            <button id="login-submit" type="submit" class="btn btn-primary login-submit">Login</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
    </div>

    <div id="footer-beta">© <?php echo date('Y'); ?> Equipe Informatique de l'<a href="http://agepoly.epfl.ch">AGEPoly</a> | <a href="mailto:informatique@agepoly.ch?subject=Je+veux+rejoindre+l'agepinfo">on recrute!</a> | <a href="mailto:polyhelp@agepoly.ch">contact</a></a>
        
    </div>
 </div>
 <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . 'view//condition_modal.php';?>

 <script type="text/javascript">
    $(function(){
        $('#login-modal').on(
            'shown', 
            function() {$('#inputUsername').select();}
            );
    }
    );
</script>


                    <!-- G-Analytics -->
<script type="text/javascript" src="/global/js/google-analytics.js"></script>
<!-- Condition modale -->
<script type="text/javascript" src="/global/js/condition-modal.js"></script>
</body>
</html>
