<div class="user_cond modal fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Conditions d'utilisation</h3>
    </div>
    <div class="modal-body">
        <p>
            <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . 'legal//user-condition.php';?>
        </p>
    </div>
</div>