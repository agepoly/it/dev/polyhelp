<div id="page-header">
	<div id="left-menu">
        <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "view//left-menu.php"; ?>
	</div>
	<div class="left-slide-button"><button id="left-menu-button" class="btn btn-inverse" title="Panneau latéral"><i class="glyphicons-icon white show_lines"></i></button></div>
	<div id="top-logo-final"></div>

	<div class="btn-toolbar">
		<?php if ($this->user_display_admin()): ?>
		<div class="btn-group">
			<button class="btn btn-inverse" title="Section d'administration" id="admin" href="/admin"><i class="glyphicons-icon white keys"></i></button>
		</div>
		<?php endif; ?>
		<div class="btn-group">
			<button class="btn btn-inverse upload-button" id="upload" title="Uploader un fichier" href="/upload/new"><i class="glyphicons-icon white cloud-upload upload-button"></i></button>
				<div class="upload-menu">
					<form id="upload-form" action="/upload/new" method="post" enctype="multipart/form-data">
					<input type="hidden" id="add-to-group" name="preferred_filegroup" value="" />

					<h4>Uploader des fichiers</h4>
					<div class="upload-group" id="upload-group-0">
						<div class="group-files">
							<p>Tu peux choisir plusieurs fichiers en même temps.</p>
							<input type="button" id="upload-select-files" value="Choisir les fichiers..." />
							<input type="file" name="files-0[]" required multiple="multiple" style="display:none">
						</div>
					</div>
					<div class="upload-footer">
						<p>
						Upload ensemble les fichiers d'une même série ou TP
						pour les classer dans le même groupe.
						</p>
					</div>
					<!--<div class="upload-footer"><button type="submit" class="btn btn-primary">Envoyer</button></div>-->
					</form>
				</div>
		</div>
		<!-- <div class="btn-group">
			<button id="chat-pane-button" class="btn btn-inverse" rel="popover"><i class="glyphicons-icon white conversation"></i></button>
		</div> -->
	</div>      
</div>
