    <div id='container-error'>

        <?php if( count_nofail($data['errors']) > 0 ): ?>
            <?php foreach($data['errors'] as $k0=>$v0): ?>
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?=$v0['text'];?>
            </div>
        
            <!--<? //php if (isset($v0['img'])): ?>
            <p>
            <img src="<? //=$v0['img'];?>" />
            </p>
            <? //php endif; ?>-->

            <?php endforeach; ?>
        <?php else: ?>
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Une erreur inconnue sans message de détail s'est produite, veuillez reporter celle-ci avec le plus de détail possible aux responsables. Excusez du désagrément occasionné.
            </div>
        <?php endif; ?>
    </div>
