<div>
	<div id="user-id">
		<div class="row-fluid">
			<div class="user_avatar span4">
	        	<img src="" alt="User's avatar" id="user-avatar">
			</div>
			<div class="span8">
				<div class="user_name"></div>
				<div class="user_faculty_section"></div>
				<div class="row-fluid user_score">
					<div class="badge badge-inverse row4 user_score_stars"></div>
				</div>
			</div>
		</div>
	</div>

		<ul class="link-list">
			<li>
				<a href='/prefs'>
					<i class="glyphicons-icon white settings"></i>
					<span>Paramètres</span>
				</a>
			</li>
			<li>
				<a class="modal_launcher" href="#">
					<i class="glyphicons-icon white book"></i>
					<span>Conditions d'utilisation</span>
				</a>
			</li>
<!--		<li>
				<a href='/logout'>
					<i class="glyphicons-icon white power"></i>
					<span>Se déconnecter</span>
				</a>
			</li>
-->		</ul>	
</div>
