<?php if (count_nofail($data['errors']) > 0 ): ?>

<?php $this->render('error', $data); ?>

<?php else: ?>

<p>
Voici la partie forum, vous pouvez discuter ici de tout ce qui concerne la section de ce forum. Vous pouvez parler de vos problèmes, de vos questions ou remarques et partager vos points de vue. Certains posts sont liés à un fichier, une icône de fichier se trouve à côté des posts en question.
</p>

<h2>Liste des posts</h2>

<div class="right_float_action">
    <a href="/support/post/new/<?=$data['postgroup_id'];?>">
        <img src="/global/img/new.png" class="icon" />
        Créer un nouveau fil de discussion
    </a>
</div>

<table id="posts" class="support-table">
    <thead>
        <tr class="support-header">
            <th>
                Titre
                &nbsp; &#x25BC;
            </th>

            <th>
                Auteur
                &nbsp; &#x25BC;
            </th>

            <th>
                Date de création
                &nbsp; &#x25BC;
            </th>

            <th>
                Fichier
            </th>
        </tr>
    </thead>

    <tbody>
        <?php foreach($data['posts'] as $k1 => $v1): ?>
        <tr class="support-table-tr">
            <td>
                <?php if ($v1['file_related']): ?>
                <a href="/support/files/<?=$v1['fgid'];?>">
                    <img src="/global/img/post-file.png" />
                    &nbsp;
                    <?=$v1['title'];?>
                </a>
                <?php else: ?>
                <a href="/support/post/view/<?=$v1['id'];?>">
                    <img src="/global/img/post-regular.png" />
                    &nbsp;
                    <?=$v1['title'];?>
                </a>
                <?php endif; ?>
            </td>

            <td>
                <a href="/profile/<?=$v1['user_id'];?>">
                    <?=$v1['last_name'];?> <?=$v1['first_name'];?>
                </a>
            </td>

            <td data="<?=$v1['ctime'];?>">
                <?=$v1['cdate'];?>
            </td>

            <td>
                <?=$v1['file_related']?"Oui":"Non";?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php endif; ?>
