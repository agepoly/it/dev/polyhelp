<ul>
    <li><a title="Informations générales" href="/">Accueil</a></li>
    <li><a title="Permet d'envoyer de nouveaux fichiers" href="/upload/new">Upload</a></li>
    <li><a title="Partie contenant les fichiers par section/cours (et les discussions/forums)" href="/support">Support</a></li>
    <li><a title="Informations personnelles et options" href="/prefs">Préférences</a></li>
    <li><a title="Permet de rechercher des fichiers ou des discussions" href="/search">Recherche</a></li>
    <?php if ($this->user_display_admin()): ?>
    <li><a title="Section d'administration" href="/admin">Admin</a></li>
    <?php endif; ?>
</ul>
