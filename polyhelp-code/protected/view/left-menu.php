<?php /*
<div>
    <span class="link">Favoris</span>:
    <ul id="lm_favs_list">
        <?php foreach($this->user_favcourses as $k => $v): ?>
        <li index="<?=$v['course_id'];?>">
            <a href="/support/course/<?=$v['course_id'];?>">
                <?=$v['name'];?>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
*/ ?>

<div class="menu-title">
    <div class="search-bar">
        <div class="search-box input-prepend">
            <span class="add-on"><i class="icon-search"></i></span>
            <input id="search-term" type="text" placeholder="Chercher un fichier...">
        </div>
    </div>
</div>
<div class="tabbable tabs-below">
    <div class="tab-content">
        <div class="tab-pane active" id="explorer-tab">
            <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "view//explorer-tab.php"; ?>
        </div>
        <div class="tab-pane" id="user-tab">
            <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "view//user-tab.php"; ?>
        </div>
        <div class="tab-pane" id="search-tab">
            <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "view//search-tab.php"; ?>
        </div>
    </div>
    <ul id="tabbar" class="nav nav-tabs">
        <li class="active"><a href="#explorer-tab" data-toggle="tab" title="Fichiers" id="explorer-tab-link"><i class="glyphicons-icon white file"></i></a></li>
        <li><a href="#user-tab" data-toggle="tab" title="Utilisateur" id="user-tab-link"><i class="glyphicons-icon white user"></i></a></li>
        <li><a href="#search-tab" data-toggle="tab" title="Recherche" id="search-tab-link"><i class="glyphicons-icon white search"></i></a></li>
    </ul>
</div>

<?php /*
<div>
    <span class="link">Connectés</span>:
    <ul id="lm_connected_list">
        <?php foreach ($this->users_connected as $v): ?>
        <li>
            <a href="/profile/<?=$v['id'];?>">
                <?=$v['last_name'];?> <?=$v['first_name'];?>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
*/ ?>