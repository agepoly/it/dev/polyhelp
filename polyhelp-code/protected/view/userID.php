<div class="user-data">
	<div class="row-fluid">

		<!--Insert the avatar of the user in the left up corner of the profile section-->
		<div id="avatar" class="span2">
			<img src="/<?=$data['user_info']['avatar'];?>" />
		</div>

		<!--Insert all the basis information of the user-->
		<div class="span10">
			<div class="row-fluid">

				<!--Insert all the basic information of the user: faculty, section, semester, contact(coming soon)-->
				<div class="span6">
					<table>
						<tr>
							<td class="data-label">Faculté</td>
							<td><?=$data['user_info']['fname'];?></td>
						</tr>
						<tr>
							<td class="data-label">Section</td>
							<td><?=$data['user_info']['sname'];?></td>
						</tr>
						<tr>
							<td class="data-label">Semestre</td>
							<td><?=Globals::$degrees[$data['user_info']['degree']]['name'];?></td>
						</tr>
						<tr>
							<td class="data-label"><img src="/global/img/icons/icon_envelope.png"/>Contact</td>
							<td>Coming Soon</td>
						</tr>
					</table>
				</div>

				<!--Insert all the information of the user according to its activity on the website-->
				<!--Thus information are links to a dedicate webpage showing the evolution (and the list of documents upload, for the documents case)-->
				<div class="span6">
					<div id="stats">

						<!--User's score according to the one given by the community-->
						<div class="span1">
							<ul>
								<li><strong><a href="#"><?=$data['user_info']['score'];?></a></strong></li>
								<li>
									<span class="stats-label">Score</span>
								</li>
							</ul>
						</div>
						<!--Number of documents uploaded. Links to the "My documents" page of the user-->
						<div class="span1">
							<ul>
								<li><strong><a href="#">0</a></strong></li>
								<li>
									<span class="stats-label">Documents</span>
								</li>
							</ul>
						</div>
						<!--Number of messages post to ask or answer any question-->
						<div class="span1">
							<ul>
								<li><strong><a href="#">0</a></strong></li>
								<li>
									<span class="stats-label">Message</span>
								</li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
		</div>


	</div>
</div>