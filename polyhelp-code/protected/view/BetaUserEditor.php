<div class="beta user editor">
	<h1>Beta User Editor</h1>
	<form action="#" method="post">
		<table>
			<tr>
				<td>
					<label for="first_name">First name</label>
				</td>
				<td>
					<input type="text" id="first_name" name="first_name" value="<?=$data['first_name']?>" />
				</td>
			</tr>

			<tr>
				<td>
					<label for="last_name">Last name</label>
				</td>
				<td>
					<input type="text" id="last_name" name="last_name" value="<?=$data['last_name']?>" />
				</td>
			</tr>

			<tr>
				<td>
					<label for="subscribed">Subscribed ?</label>
				</td>
				<td>
					<input type="checkbox" id="subscribed" name="subscribed" value="<?=$data['subscribed']?>" />
				</td>
			</tr>

			<tr>
				<td></td>
				<td><button type="submit" id="submit" name="submit">Submit</button></td>
			</tr>
		</table>
	</form>
</div>