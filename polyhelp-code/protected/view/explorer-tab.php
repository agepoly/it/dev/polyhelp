<div id="explorer" class="carousel slide">
	<div class="carousel-inner">
		<div class="active item">
			<div class="navbar">
				<!-- <a>&lt; Retour</a> -->
				<span class="title">PolyHelp</span>
			</div>
			<ul id="explorer-sections" class="link-list subtle-scroll overthrow"></ul>
		</div>
		<div class="item">
			<div class="navbar">
				<a>&lt; Retour</a>
				<span class="title"></span>
			</div>
			<ul id="explorer-semesters" class="link-list subtle-scroll overthrow"></ul>
		</div>
		<div class="item">
			<div class="navbar">
				<a>&lt; Retour</a>
				<span class="title"></span>
			</div>
			<ul id="explorer-courses" class="link-list subtle-scroll overthrow"></ul>
		</div>
		<div class="item">
			<div class="navbar">
				<a>&lt; Retour</a>
				<span class="title"></span>
			</div>
			<ul id="explorer-categories" class="link-list subtle-scroll overthrow"></ul>
		</div>
		<div class="item">
			<div class="navbar">
				<a>&lt; Retour</a>
				<span class="title"></span>
			</div>
			<ul id="explorer-filegroups" class="link-list subtle-scroll overthrow"></ul>
		</div>
		<div class="item">
			<div class="navbar">
				<a>&lt; Retour</a>
				<span class="title"></span>
			</div>
			<ul id="explorer-files" class="link-list subtle-scroll overthrow"></ul>
		</div>
	</div>
</div>