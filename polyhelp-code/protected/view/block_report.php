<div id="report">
    <form method="post" action="/ajax/report">
    <input type="hidden" name="relid" value="<?=$data['relid'];?>" />
    <input type="hidden" name="reltype" value="<?=$data['reltype'];?>" />
    <p>
    Vous pouvez reporter un problème à propos d'un élément dans la page actuelle afin que les modérateurs le règlent. Sélectionnez la bonne catégorie, et décrivez le problème avec suffisamment de détails en précisant ce qui devrait être changé pour que le problème soit résolu.
    </p>

    <div>
        <label>Type de problème:</label><br/>
        <select name="cat">
            <?php
            $problems_types_reltype = Globals::$problems_types[$data['reltype']]; 
            foreach ($problems_types_reltype['categories'] as $k => $v): ?>
            <option value="<?=$k;?>"><?=$v;?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div>
        <label>Description</label><br/>
        <textarea name="description" class="hint" hint="Description du problème"></textarea>
    </div>

    <div>
        <input class="submit" type="submit" value="Reporter" />
    </div>
    </form>
</div>
