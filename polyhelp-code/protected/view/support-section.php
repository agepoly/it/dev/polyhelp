<?php if (isset($data['default_search'])): ?>
<script type="text/javascript">var searchVal = "<?=$data['default_search'];?>";</script>
<?php endif; ?>

<p>
Vous trouverez ici tous les cours de la section sélectionnée. En cliquant sur une des branches, il vous est possible d'accéder aux différents fichiers et au forum associé. L'étoile à côté de chaque branche permet de mettre ou enlever le cours en accès rapide dans les favoris.
Le moyen le plus rapide d'accéder à la branche de votre choix est de la mettre en favori pour plus tard, vous pouvez aussi chercher une partie du nom du cours, du professeur, etc dans le champs 'Rechercher'. La dernière colonne correspond au nombre de groupes de fichiers (un exercice et son corrigé forme un groupe) pour ce cours.
</p>

<p>
Si JavaScript est activé, le tableau ci-dessous est interactif: il peut-être filtré et trié selon les colonnes de votre choix (pour ajouter une colonne au tri, appuyez sur shift puis cliquez). Par défaut seul les cours de l'année et le semestre actuel sont affichés, videz le champs de recherche pour tout afficher.
</p>

<?php if (count_nofail($data['errors']) > 0 ): ?>
    <?php $this->render('error', $data); ?>
<?php else: ?>

<p class="forum-links">
Actions utiles en relation avec le forum:
<ul class="links">
    <li>
        <a href="/support/postgroup/<?=$data['pg_section'];?>">
        Accéder au forum de la <b><?=$data['section'];?></b>
        </a>
    </li>

    <li>
        <a href="/support/postgroup/<?=$data['pg_faculty'];?>">
        Accéder au forum de la <b><?=$data['faculty'];?></b>
        </a>
    </li>
</ul>
</p>

<table id="courses" class="support-table">
    <caption>Liste des cours</caption>

    <thead>
        <tr class="support-header">
            <th>
                Nom du cours &nbsp; &#x25BC;
            </th>

            <th>
                Professeur &nbsp; &#x25BC;
            </th>

            <th>
                Année &nbsp; &#x25BC;
            </th>

            <th>
                Semestre &nbsp; &#x25BC;
            </th>

            <th>
                # &nbsp; &#x25BC;
            </th>
        </tr>
    </thead>

    <tbody>
        <?php foreach($data['courses'] as $k1 => $v1): ?>
        <tr class="support-table-tr">
            <td>
                <?php if (array_get($data['favcourses'], $v1['id'], false)): ?>
                <a href="/ajax/favcourse/rem/<?=$v1['id'];?>" class="fav_icon favorited" target="favs" index="<?=$v1['id'];?>" cname="<?=$v1['name'];?>">
                    <img src="/global/img/fav.png" />
                </a>
                <?php else: ?>
                <a href="/ajax/favcourse/add/<?=$v1['id'];?>" class="fav_icon notfavorited" target="favs" index="<?=$v1['id'];?>" cname="<?=$v1['name'];?>">
                    <img src="/global/img/notfav.png" />
                </a>
                <?php endif; ?>
                &nbsp;
                <a href="/support/course/<?=$v1['id'];?>">
                    <?=$v1['name'];?>
                </a>
                &nbsp;<span class="isa_key"><?=$v1['isa_key'];?></span>
            </td>

            <td>
                <?php foreach($v1['teachers'] as $k => $t): ?>
                <?php if ($k > 0) { echo ', '; } ?>
                <a href="/profile/<?=$t['id'];?>">
                    <?=$t['lname'];?> <?=$t['fname'];?>
                </a>
                <?php endforeach; ?>
            </td>

            <td data="<?=$v1['degree'];?>">
                <?=$data['degrees'][$v1['degree']]['name'];?>
            </td>

            <td data="<?=$v1['semester'];?>">
                <?=$data['semesters'][$v1['semester']]['name'];?>
            </td>

            <td>
                <?=$v1['fcount'];?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php endif ?>
