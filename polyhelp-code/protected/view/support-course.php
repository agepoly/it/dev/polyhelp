<p>
Sur cette page, vous trouverez la liste des fichiers et des messages concernant le cours sélectionner. En cliquant sur une des entrées ci-dessous, vous entrerez dans le mode visualisation de fichiers.
</p>

<p>
Les coches permettent de sélectionner des fichiers sur lesquels certaines opérations de groupes auront liés, par exemple le téléchargement de ceux-ci.
</p>

<?php if (count_nofail($data['errors']) > 0 ): ?>
    <?php $this->render('error', $data); ?>
<?php else: ?>

<?php if (count_nofail($data['courses_related']) > 0): ?>
<p>
Ce cours est donné (par d'autres professeurs) dans d'autres sections:
    <?php foreach($data['courses_related'] as $course): ?>
    <a href="/support/course/<?=$course['id'];?>">
        [<?=implode(' + ', $course['sections']);?>]
    </a>&nbsp;
    <?php endforeach; ?>
</p>
<?php endif; ?>

<p class="forum-links">
Actions utiles en relation avec le forum:
<ul class="links">
    <li>
        <a href="/support/postgroup/<?=$data['postgroup_id'];?>">
        Accéder au forum de la <b>branche</b>
        </a>
    </li>
</ul>
</p>

<form action="/support/zippack/" method="POST">
<input type="hidden" name="course" value="<?=$data['course_id'];?>" />

<h2>Liste des groupes de fichiers</h2>

<div class="right_float_action">
    <a href="/upload/cnew/<?=$data['section_id_first'];?>/<?=$data['course_id'];?>">
        <img src="/global/img/upload.png" class="icon" />
        Envoyer des fichiers pour ce cours
    </a>
</div>

<table id="filegroups" class="support-table">
    <thead>
        <tr class="support-header">
            <th>
                Nom
                &nbsp; &#x25BC;
            </th>

            <th>
                Type
                &nbsp; &#x25BC;
            </th>

            <th>
                Auteur
                &nbsp; &#x25BC;
            </th>

            <th>
                Uploader
                &nbsp; &#x25BC;
            </th>

            <th>
                Date envoi
                &nbsp; &#x25BC;
            </th>

            <th>
                Correction
                &nbsp; &#x25BC;
            </th>

            <th>
                Note
                &nbsp; &#x25BC;
            </th>

            <th>
                #
                &nbsp; &#x25BC;
            </th>
        </tr>
    </thead>

    <tbody>
        <?php foreach($data['filegroups'] as $k1 => $v1): ?>
        <tr class="support-table-tr">
            <td>
                <input type="checkbox" class="selfile" name="fgids[]" value="<?=$v1['id'];?>" value="1" />&nbsp;
                <a href="/support/files/<?=$v1['id'];?>/<?=$v1['fid'];?>">
                    <?=$v1['name'];?>
                </a>
            </td>

            <td data="<?=$v1['category'];?>">
                <?=$data['categories'][$v1['category']];?>
            </td>

            <td>
                <a href="/profile/<?=$v1['aid'];?>">
                    <?=$v1['afname'];?> <?=$v1['alname'];?>
                </a>
            </td>

            <td>
                <a href="/profile/<?=$v1['uid'];?>">
                    <?=$v1['ufname'];?> <?=$v1['ulname'];?>
                </a>
                <br/><img class="mini user_stars" src="<?=user_score_image(user_score($v1['uscore']));?>" />
            </td>

            <td data="<?=$v1['time'];?>">
                <?=$v1['date'];?>
            </td>

            <td>
                <?=($v1['corrections']) ? "Oui" : "Non";?>
            </td>

            <td>
                <span class="nowrap">
                    <span class="rating"><?=$v1['rating'];?></span>
                    &nbsp;<img class="file_stars"src="<?=file_score_image($v1['best_stars']);?>" />
                </span>
            </td>

            <td>
                <?=$v1['fcount'];?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<p>
<input type="submit" id="zip-download" value="Télécharger" />
<span class="select-checkbox">
    <label>Sélectionner</label>:&nbsp;
    [<a href="#" class="select" onClick="select_all('.selfile'); return false;">tout</a>]
    &nbsp;
    [<a href="#" class="select" onClick="select_none('.selfile'); return false;">rien</a>]
</span>
</p>

</form>
<hr />

<?php dooinclude('forum-postgroup.php', $data); ?>

<?php endif; ?>
