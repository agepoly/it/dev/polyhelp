<?php if (count_nofail($data['errors']) > 0 ): ?>

<?php $this->render('error', $data); ?>

<?php else: ?>

<div id="profile">
    <div class="row-fluid">
        <div class="span8">
                <div class="thumbnail">
                    <div class="span3">
                        <img src="/<?=$data['user_info']['avatar'];?>" alt="">
                    </div>

                    <div class="span7">
                        <h3><?=$data['user_info']['last_name'];?> <?=$data['user_info']['first_name'];?></h3>
                        <table>
                        <tr>
                            <td class="data-label">Section</td>
                            <td><?=$data['user_info']['sname'];?></td>
                        </tr>
                        <tr>
                            <td class="data-label">Année</td>
                            <td><?=Globals::$degrees[$data['user_info']['degree']]['name'];?></td>
                        </tr>
                    </table>
                    </div> 

                    <div class="span2">
                        <div id="badges">
                            <div class="badge badge-inverse">Score: <?=$data['user_info']['score'];?></div>
                            <div class="badge badge-info" style="visibility:hidden;">Fichiers: 0</div>
                            <div class="badge" style="visibility:hidden;">Messages: 0</div>
                        </div>
                        
                        <?php if($data['edit']): ?>
                        <div id="edit-profile">
                            <a href="/profile/edit/<?=$data['user_id'];?>"><i class="icon-pencil"></i> Profil</a>
                        </div>
                        <?php endif;?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->render('block_report', $data['block_report'], $this); ?>

<?php endif; ?>
