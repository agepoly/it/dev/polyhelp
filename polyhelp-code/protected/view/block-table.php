<table id="<?=$data['html_id'];?>">
    <thead>
        <tr class="support-header">
            <?php foreach ($data['headers'] as $v): ?>
            <th>
                <?=$v;?>
                &nbsp; &#x25BC;
            </th>
            <?php endforeach; ?>
        </tr>
    </thead>

    <tbody>
        <?php foreach($data['rows'] as $k => $v): ?>
        <tr class="support-table-tr">
            <?php foreach($v as $c): ?>
            <td data="<?=array_get($c, 'raw', $c['value']);?>" >

                <?php if (isset($c['link'])): ?>
                <a href="<?=$c['link'];?>">
                <?php endif; ?>

                    <?=$c['value'];?>

                <?php if (isset($c['link'])): ?>
                </a>
                <?php endif; ?>

                <?php if (isset($c['buttons'])): ?>
                &nbsp;
                <?php foreach($c['buttons'] as $b): ?>
                <a class="button <?=array_get($b, 'class', '');?>" href="<?=$b['link'];?>">
                    <img src="/global/img/<?=$b['icon'];?>.png" />
                </a>
                <?php endforeach; ?>
                <?php endif; ?>

            </td>
            <?php endforeach; ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
