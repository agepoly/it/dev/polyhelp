<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <title><?php echo $data['section']; ?> | PolyHelp</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" media="all" href="/global/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/global/css/jquery.pagemove.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/global/css/overthrow.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/global/css/style.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/global/css/upload.css" />
    <link type="text/css" rel="stylesheet" media="all" href="/global/css/glyphicons.css" />
    <?php foreach($data['css'] as $k0=>$v0): ?>
    <link type="text/css" rel="stylesheet" media="all" href="/global/css/<?php echo $v0; ?>.css" />
    <?php endforeach; ?>
    <script type="text/javascript" src="/global/js/force-https.js"></script>
    <script type="text/javascript" src="/global/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/global/js/bootstrap.js"></script>
    <script type="text/javascript" src="/global/js/sessionstorage.js"></script>
    <script type="text/javascript" src="/global/js/overthrow.js"></script>
    <script type="text/javascript" src="/global/js/jquery.keyboardnavigable.js"></script>
    <script type="text/javascript" src="/global/js/left-menu.js"></script>
    <script type="text/javascript" src="/global/js/fuse.min.js"></script>
    <script type="text/javascript" src="/global/js/search-tab.js"></script>
    <script type="text/javascript" src="/global/js/explorer-tab.js"></script>
    <script type="text/javascript" src="/global/js/user-tab.js"></script>
    <script type="text/javascript" src="/global/js/upload.js"></script>
   <?php foreach($data['js'] as $k0=>$v0): ?>
    <script type="text/javascript" src="/global/js/<?php echo $v0; ?>.js"></script>
    <?php endforeach; ?>
</head>
