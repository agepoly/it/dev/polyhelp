<div class="hint_box">
    <span class="hint_number">1.</span>
    <span class="hint_center">Spécifiez les fichiers à envoyer.</span>
</div>

<p>
Dans cette partie, il vous est possible de mettre à disposition des autres étudiants des fichiers (PDF, TEX, TXT, ZIP, PNG). Ils seront visibles dans la section et la catégorie (exemple: Exercices) que vous indiquerez à la prochaine étape. Suivez le guide pas à pas pour terminer l'opération.
</p>
<p>
Il vous est possible de grouper plusieurs fichiers dans un même groupe. Cela est utile pour mettre: un exercice avec son corrigé, un PDF de TP avec son archive ZIP ou des scans sous forme d'images d'un même document. Essayez de privilégiez les PDF de bonne qualité.
</p>
<p>
Remarquez qu'il n'est pas possible d'envoyer, seule dans son groupe, une archive ZIP, grouper le d'abord avec un document PDF.
</p>

<form id="upload-form" action="/upload/new" method="post" enctype="multipart/form-data">
    <?php if( isset($data['preferred_section']) && isset($data['preferred_course']) ): ?>
    <input type="hidden" name="preferred_section" value="<?php echo $data['preferred_section']; ?>" />
    <input type="hidden" name="preferred_course" value="<?php echo $data['preferred_course']; ?>" />
    <?php endif; ?>
    <?php if( isset($data['preferred_filegroup']) ): ?>
    <input type="hidden" name="preferred_filegroup" value="<?php echo $data['preferred_filegroup']; ?>" />
    <?php endif; ?>
    <div class="form">
        <div class="upload-group" id="upload-group-0">
            <div class="upload-group-title">Groupe de fichiers #1</div>
            <div class="upload-group-in">
                <ul class="group-files">
                    <li><input type="file" name="files-0[]" /></li>
                </ul>
                <span class="upload-add-link" onclick="addfile(0);">
                    <span class="upload-add"><span class="button-add">+</span> Ajouter un fichier</span>
                </span>
            </div>
        </div>
        <div class="centered">
            <input class="gradient_button" type="submit" value="Envoyer" />
        </div>
    </div>
</form>
