<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "viewc//header.php"; ?>
<body>
    <div id="page-header">
        <div id="top-logo">
            <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "viewc//top-logo.php"; ?>
        </div>
        <div id="top-menu">
            <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "view//top-menu.php"; ?>
        </div>
    </div>
    <div id="page-main">
        <div id="left-menu">
            <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "view//left-menu.php"; ?>
        </div>
        <div id="page-content">
            <h1><?php echo $data['section']; ?></h1>
            <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "view//support.php"; ?>
        </div>
    </div>
    <div id="page-footer">
        <?php include Doo::conf()->SITE_PATH .  Doo::conf()->PROTECTED_FOLDER . "viewc//footer.php"; ?>
    </div>
</body>
</html>
