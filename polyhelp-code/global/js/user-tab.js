$(document).ready(function () {
    
    // Always load user info. You never know if this tab will be shown first on page load.
    load_user_info();
    
    function load_user_info(){
        $.ajax('/ajax/user/info', {
            dataType: 'json',
            
            success: function(data, status){
                $('#user-avatar').attr('src', '/'+data.user_info.avatar);
                $('.user_name').html(data.user_info.first_name+' '+data.user_info.last_name);
                $('.user_faculty_section').html(data.user_info.fname+' - '+data.user_info.sname);
                var stars = '';
                for(var i = 0; i < data.score_stars; i++){
                    stars += '<i class="icon-star icon-white"></i>';
                }
                $('.user_score_stars').html(stars);
            },
            
            error: function(req, status, error){
                console.log("Errors in call to /ajax/user/info. Error: "+error);
            }
        });
    }
    
});