$(document).ready(function () {

	$('#explorer.carousel').carousel({interval: false});

	$('#explorer .navbar a').on('click', function () {
		$('#explorer.carousel').carousel('prev');
	});

	// string path: path to load, starting with an "f"
	// (Boolean) recursive: loads every parent folder into their respective tabs (false by default)
	function load_path (path, recursive) {
		if (recursive) {
			folders = path.split('-');
			var folder_path;
			for (var i = 0; i < folders.length - 1; ++i) {
				// Load parent folders
				folder_path = folders.slice(0, i+1).join('-');
				$.ajax('/ajax/explorer/list/' + folder_path, {
					dataType: 'json', 

					success: function (data, status) {
						render_explorer(data);
					},

					error: function (req, status, error) {
						console.log('Parent folder error: ' + error);
						console.log(req.responseText);
					}
				});
			}
		}

		$.ajax('/ajax/explorer/list/' + path, {
			dataType: 'json',

			success: function (data, status) {
				render_explorer(data, true);
			}, 

			error: function (req, status, error) {
				console.log('Error: ' + error);
			}
		});
	}

	// (Boolean) display: Slide explorer carousel to this path (false by default)
	function render_explorer (data, display) {

		if (!data) return;

		var level = data.level;
		var list = data.list;
		var title = data.title;
		var path = data.parts.path;
		var fgid;

		// Names of CURRENT FOLDERS
		var levels = ['section', 'semester', 'course', 'category', 'filegroup', 'file'];
		var ids = ['#explorer-sections', '#explorer-semesters', '#explorer-courses', 
				'#explorer-categories', '#explorer-filegroups', '#explorer-files'];

		// Render list
		var ul = $(ids[level]).empty();
		for (var i = 0; i < list.length; ++i) {
			ul.append($('<li/>')
				.attr('class', list[i].group)
				.append($('<a/>')
					.html(
					      list[i].name
					      + (list[i].count ? '<span class="count">(' + list[i].count + ')</span>' : '')
					      + (list[i].subtype ? '<span class="count">('+ list[i].subtype + ')</span>': '')
					      + (list[i].year ? ' <span class="count badge">'+ list[i].year+'</span>': '')
					)
					.attr('href', list[i].href)
					.on('click', function (ev) {
						if (this.href.indexOf('#f') == -1) {
							location.href = this.href; // Non-explorer link
							
						} else {
							ev.preventDefault();	// Explorer link
							var path = this.hash.replace('#', '');
							$.ajax('/ajax/t/explorer/navigate/'+path);
							load_path(path);
						}
					})
				)
			);
			//Set the filegroup id, so we can add a link to add a file to the filegroup
			if (list[i].href.indexOf('#f') == -1) {
				fgid = list[i].href.split('/')[3];
			}						
		}
		if(level == 4){
			ul.append($('<p/>')
				.attr('class', 'add-file')
				.append($('<a/>')
					  .html('+ Uploader dans un nouveau groupe')
					  .on('click', function(){
					  	if ($('.upload-menu').is(':hidden')) $.ajax('/ajax/t/upload/panel/'+fgid);
						//$('.upload-menu').show();
						$('#upload-form input[type="file"]').click();
						//$('.upload-menu').find('h4').html('Ajouter un fichier dans le groupe courant');
					  })
				)
			);
		}
		if(level == 5){
			ul.append($('<p/>')
				.attr('class', 'add-file')
				.append($('<a/>')
					  .attr('id', 'add-to-group-button')
					  .html('+ Uploader des fichiers ici...')
					  .on('click', function(){
					  	if ($('.upload-menu').is(':hidden')) $.ajax('/ajax/t/upload/panel/'+fgid);
						//$('.upload-menu').show();
						$('#add-to-group').attr('value', fgid);
						$('#upload-form input[type="file"]').click();
						//$('.upload-menu').find('h4').html('Ajouter un fichier dans le groupe courant');
					  })
				)
			);
		}

		if (list.length == 0) {
			ul.append($('<li/>').attr('class', 'no-results').html('Plus rien. <br>Uploadez un fichier!'));
		}

		// Store path in div & show current folder title
		$($('#explorer .item')[level]).data('path', path).find('.title').text(title);

		if (display) {
			$('#explorer').carousel(level);
		}
	};

	// Load path & instantly jump to its "slide" (level)
	function insta_load(path) {
		if (path != "f") {
			// Instant jump to level (instead of sliding through entire carousel)
			$('#explorer').hide().removeClass('slide').one('slid', function () {
				$('#explorer').show().addClass('slide');
			});
		}
		load_path(path, true);
	}

	// Page just loaded; load default path
	if (window.sessionStorage.explorer_path) {
		insta_load(window.sessionStorage.explorer_path);
	} else {
		$.ajax('/ajax/explorer/default_path', {
			dataType: 'json', 

			success: function (data, status) {
				insta_load(data.path);
			},

			error: function (req, status, error) {
				console.log('Default folder path error: ' + error);
				console.log(req.responseText);
				insta_load('f');	// Fallback path
			}
		});
	}

	// Track and store explorer location
	$('#explorer').on('slid', function () {
		window.sessionStorage.explorer_path = $('#explorer .active').data('path');
	});

});
