$(document).ready(function () {
    /* hide/show left menu */
    
    $pagemove = $('#pagemove');
    var toggleLeftMenu = function () {
        if (!$pagemove.is(':hidden')) {
            $.pagemove.close();
            $pagemove.data('mode', '');
            window.sessionStorage.left_menu = false;
        } else {
            $.pagemove({href: '#left-menu', width: '22%', duration: leftMenuDuration, behavior: 'squish', edge: 'left'});
            window.sessionStorage.left_menu = true;
        }
    };
    $('#left-menu-button').click(toggleLeftMenu);


    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
    var leftMenuDuration = !(isChrome || isSafari) ? 0 : 250;

    if (!window.sessionStorage.hasOwnProperty('left_menu') && location.href.indexOf('/support/files/') == -1) {
        window.sessionStorage.left_menu = true;   // Default value (if a document is not open right now)
    }
    
    if (window.sessionStorage.left_menu == 'true') {
        $.pagemove({instant: true, href: '#left-menu', width: '22%', duration: leftMenuDuration, behavior: 'squish', edge: 'left'});
    }

    var tab_ids = ['explorer-tab', 'user-tab', 'search-tab'];
    if (!window.sessionStorage.hasOwnProperty('left_tab')) {
        window.sessionStorage.left_tab = 0;
    }
    // Show whatever tab was last open
    $('#tabbar li:eq(' + window.sessionStorage.left_tab + ') a').tab('show');

    // Remember new tab whenever tab changes
    $('#tabbar a[data-toggle="tab"]').on('shown', function (e) {
        window.sessionStorage.left_tab = $(e.target).parent().index();
    });

    /*$(document).bind('keyup', function (e) {
        if (e.keyCode == 83) toggleLeftMenu();   // 's' key
        if (e.keyCode != 65) return;    // 'a' key
        
        if ($pagemove.data('mode') != 'ext') {
            $.pagemove({href: '#left-menu', width: '80%', behavior: 'push', edge: 'left'});
            $pagemove.data('mode', 'ext');
        } else {    // Extended sidebar is open
            $.pagemove({href: '#left-menu', width: '22%', behavior: 'push', edge: 'left'});
            $pagemove.data('mode', '');
        }
    });*/
    
    /* add action on fav/courses list */
    $('#lm_courses_list li').each(function () {
        left_menu_decorate($(this), false);
    });
    $('#lm_favs_list li').each(function () {
        left_menu_decorate($(this), true);
    });
    
});
