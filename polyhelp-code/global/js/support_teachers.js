$(document).ready(function () {
    $('#courses').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 0, 'asc' ] ],
        'bPaginate': false,
        'bInfo': false,
    }));
});
