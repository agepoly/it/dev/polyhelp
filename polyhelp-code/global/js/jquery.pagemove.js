/*
 * jQuery pageMove
 * Patrick Yupeng Zhao
 *
 * Based on the jQuery pageSlide plugin
 * Copyright (c) 2011 Scott Robbin (srobbin.com)
 * Dual licensed under the MIT and GPL licenses.
*/

;(function($){
    // Convenience vars for accessing elements
    var $page = $('#page'),
        $pagemove = $('#pagemove');
    
    var _moving = false;   // Mutex to assist closing only once
    var _dontclose = false;    // Prevents panel closing when clicked inside
    
	// If the pagemove element doesn't exist, create it
    if( $pagemove.length == 0 ) {
         $pagemove = $('<div />').attr( 'id', 'pagemove' )
                                  .css( 'display', 'none' )
                                  .appendTo( $('body') );
    }
    if($('#pagemove-inactive').length == 0) {
        $('<div />').attr('id', 'pagemove-inactive')
                    .css('display', 'none')
                    .appendTo($('body'));
    }
    $('body').css('overflow-x', 'hidden');
    
    /*
     * Private methods 
     */
    
    function _load( url, useIframe ) {
        // Are we loading an element from the page or a URL?
        if ( url.indexOf("#") === 0 ) {   
            // Load a page element                
            $pagemove.children().appendTo($('#pagemove-inactive')).hide();
            $(url).appendTo( $pagemove ).show();
        } else {
            // Load a URL. Into an iframe?
            if( useIframe ) {
                var iframe = $("<iframe />").attr({
                                                src: url,
                                                frameborder: 0,
                                                hspace: 0
                                            })
                                            .css({
                                                width: "100%",
                                                height: "100%"
                                            });
                
                $pagemove.html( iframe );
            } else {
                $pagemove.load( url );
            }
            
            $pagemove.data( 'localEl', false );
            
        }
    }
    
    // Function that controls opening of the pagemove
    function _start( edge, duration, width, behavior ) {
        var slideWidth = width,
            pageAnimation = {},
            slideAnimation = {};
        
        // If the slide is open or opening, just ignore the call
        //if( $pagemove.is(':visible') || _moving ) return;	        
        _moving = true;
                       
        // Change page width if squishing
        if (behavior == 'squish') {
            pageAnimation.width = '-=' + slideWidth;
        }

        switch( edge ) {
            case 'left':
                $pagemove.css({ left: '-' + slideWidth, right: 'auto', width: slideWidth });
                pageAnimation.left = '+=' + slideWidth;
                slideAnimation.left = '+=' + slideWidth;
                break;
            default:
                $pagemove.css({ left: 'auto', right: '-' + slideWidth, width: slideWidth});
                if (behavior != 'squish') pageAnimation.left = '-=' + slideWidth;
                slideAnimation.right = '+=' + slideWidth;
                break;
        }
                    
        // Animate the slide, and attach this slide's settings to the element
        $page.animate(pageAnimation, duration).css('overflow', '');
        $pagemove.show()
                  .animate(slideAnimation, duration, function() {
                      _moving = false;
                      $page.addClass('pagemoved');
                  });
    }

    // Slides an already open slide
    // (uses only fromWidth's unit; cannot mix % and px)
    // (positive direction expands the slide & shrinks the page)
    function _slide(edge, duration, fromWidth, toWidth, behavior) {
        var delta = parseInt(toWidth, 10) - parseInt(fromWidth, 10), 
            unit = fromWidth.replace(/[\d\-]/g, ''),
            pageAnimation = {}, 
            slideAnimation = {};
        
        if (behavior == 'squish') {
            pageAnimation.width = ((delta > 0) ? '-=' : '+=') + Math.abs(delta) + unit;
        }
        slideAnimation.width = ((delta > 0) ? '+=' : '-=') + Math.abs(delta) + unit;
        switch (edge) {
            case 'left' :
                pageAnimation.left = ((delta > 0) ? '+=' : '-=') + Math.abs(delta) + unit;
                break;
            default :
                if (behavior != 'squish') pageAnimation.left = ((delta > 0) ? '-=' : '+=') + Math.abs(delta) + unit;
                break;
        }

        $page.animate(pageAnimation, duration).css('overflow', '');
        $pagemove.animate(slideAnimation, duration, function () {
            _moving = false;
        });
    }
      
    /*
     * Public methods 
     */
    
    // Open the pagemove
    $.pagemove = function( options ) {      
        // Extend the settings with those the user has provided
        var settings = $.extend({}, $.pagemove.defaults, options);

        // Allow for instantaneous pagemove
        var animSpeed = settings.instant ? 0 : settings.duration;
        delete settings.instant;
        
        // Are we trying to open in different edge?
        if( $pagemove.is(':visible') && $pagemove.data( 'edge' ) != settings.edge) {
            $.pagemove.close(function(){
                _load( settings.href, settings.iframe );
                _start( settings.edge, animSpeed, settings.width, settings.behavior );
            });
        } else if ($pagemove.is(':hidden')) {                
            // Same edge, first panel opening
            _load( settings.href, settings.iframe );
            _start( settings.edge, animSpeed, settings.width, settings.behavior );
        } else {
            // Same edge, panel already open
            _load( settings.href, settings.iframe );
            _slide(settings.edge, animSpeed, $pagemove.data('width'), settings.width, settings.behavior);
        }
        
        $pagemove.data( settings );
    }
    
    // Close the pagemove
    $.pagemove.close = function( callback ) {
        var $pagemove = $('#pagemove'),
            slideWidth = $pagemove.data('width'),
            duration = $pagemove.data('duration'),
            behavior = $pagemove.data('behavior'), 
            pageAnimation = {},
            slideAnimation = {};
        
        // If the slide isn't open, just ignore the call
        if( $pagemove.is(':hidden') || _moving ) return;
        _moving = true;
        
        pageAnimation.width = '100%';
        pageAnimation.left = '0%';

        switch( $pagemove.data( 'edge' ) ) {
            case 'left':
                slideAnimation.left = '-=' + slideWidth;
                break;
            default:
                slideAnimation.right = '-=' + slideWidth;
                break;
        }
        
        $pagemove.animate(slideAnimation, duration);
        $page.animate(pageAnimation, duration, function() {
            $page.removeClass('pagemoved');
            $pagemove.hide();
            _moving = false;
            if( typeof callback != 'undefined' ) callback();
        }).css('overflow', '');
    }
    
	/*
     * Default settings 
     */
    $.pagemove.defaults = {
        duration: 'normal', // Accepts standard jQuery effects durations (i.e. fast, normal or milliseconds)
        edge:     'left',   // Accepts 'left' or 'right'
        modal:    true,     // If set to true, you must explicitly close pagemove using $.pagemove.close();
        iframe:   true,     // By default, linked pages are loaded into an iframe. Set this to false if you don't want an iframe.
        href:     null,     // Override the source of the content. Optional in most cases, but required when opening pagemove programmatically.
        width:    '260px',  // Accepts any CSS width value and unit
        behavior: 'push'    // Accepts 'push' or 'squish'
    };
	
    /* Events */
    
    // Don't let clicks to the pagemove close the window
    $pagemove.click(function(e) {
        _dontclose = true;
    });

    // Close the pagemove if the document is clicked or the users presses the ESC key, unless the pagemove is modal
	$(document).bind('click keyup', function(e) {
        // If this is a keyup event, let's see if it's an ESC key
        if( e.type == "keyup" && e.keyCode != 27) return;
	    
	    // Make sure it's visible, and we're not modal	    
	    if( $pagemove.is( ':visible' ) && !$pagemove.data( 'modal' )) {
            if (_dontclose) {
                _dontclose = false;
            } else {
                $.pagemove.close();
           }
	    }
	});
	
})(jQuery);