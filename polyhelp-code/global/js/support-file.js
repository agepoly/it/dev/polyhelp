var scale = 1;
var pdf = null;
var page = 1;
var urlStore;
var actualScrollTop = 0;

$(document).ready(function () {

    /* manage the ajax to vote any file */
    $('i.vote_sym').click(function (e) {
        var that = $(this);
        var rate = that.attr('rate');
        var rate_int = ((rate == 'upvote') ? 1 : -1);
        var link = $(this).closest('a').attr('href');
    
        $.post(link, { 'useajax': 1 },
            function (data) {
                var json = $.parseJSON(data);
    
                that.closest('.btn-group')
                    .find('a')
                    .removeClass('vote_sym_on')
                    .addClass('vote_sym_off')
                    .unbind('mouseenter')
                    .unbind('click')
                    .click(function (e) { e.preventDefault() });
            }
        );
    
        e.preventDefault();
    });
    
    // $('#page-main').keydown(function(e){
    //     if(e.which == 39){
    //        nextPage();
    //     }else if(e.which == 37){
    //         prevPage();
    //     }else if(e.which == 38){
    //         zoomPlus();
    //     }else if(e.which == 40){
    //         zoomMinus();
    //     }
    // });
    
    
    $('#share').click(function(){
        $('.popover-wrapper').toggle();
    });
    
    
    // $('#plus').on('click', zoomPlus);  
    // $('#minus').on('click', zoomMinus);
    // $('#nextPage').on('click', nextPage);    
    // $('#prevPage').on('click', prevPage);
    
    $('#print').on('click', printPDF);

    // $('#page-main').scroll(function(){
    //     if (isScrolledDown()) { 
    //         switchHeaderToScrollDown();
    //     }
    // }); 

    // $('#view-parent').scroll(function(){
    //     var newScrollTop = $('#view-parent').scrollTop();

    //     if(newScrollTop == 0 && newScrollTop < actualScrollTop) {
    //         switchHeaderToScrollUp();
    //     }

    //     actualScrollTop = newScrollTop;
    // });
});

// function loadPage(pageno, scale, url){
//     PDFJS.disableWorker = true;
//     PDFJS.getDocument(url).then(function getPdfHelloWorld(_pdf) {
//     pdf = _pdf;
//     pdf.getPage(pageno).then(function getPageHelloWorld(page) {
//       var viewport = page.getViewport(scale);
//       //
//       // Prepare canvas using PDF page dimensions
//       //
//       var canvas = document.getElementById('canvas0');
//       var context = canvas.getContext('2d');

//       canvas.height = viewport.height;
//       canvas.width = viewport.width;
//       //
//       // Render PDF page into canvas context
//       //
//       page.render({canvasContext: context, viewport: viewport});
//     });
//   });
// }

// function wholePDF(scale, url){

//     PDFJS.disableWorker = true;

//     PDFJS.getDocument(url).then(function getPdfHelloWorld(_pdf){
//         urlStore = url;
//         pdf = _pdf;
//         for(var i = 0; i < pdf.numPages; i++){
//             pdf.getPage(i+1).then(function getPageHelloWorld(page){
//                 var viewport = page.getViewport(scale);
//                 if(i > 0 && $('#canvas'+i).length == 0){
//                     $('#canvas'+(i-1)).after('<canvas id="canvas'+i+'" class="canvas"></canvas>');
//                 }
//                 var canvas = document.getElementById('canvas'+i);
//                 canvas.height = viewport.height;
//                 canvas.width = viewport.width;
//                 var context = canvas.getContext('2d');
//                 page.render({canvasContext: context, viewport: viewport});
//             });
//         }
//     })
// }

// function nextPage(){
//     if(page >= pdf.numPages)
//         return ;
//     page++;
//     window.location.hash = 'canvas'+(page-1);
// }

// function prevPage(){
//     if(page <= 1)
//         return ;
//     page--;
//     window.location.hash = 'canvas'+(page-1);
// }

// function zoomPlus(){
//     if(scale > 2) {
//         return;
//     }

//     else if(scale > 1) {
//         scale *= 1.15;
//     }

//     else if(scale > 0.5) {
//         scale *= 1.35;
//     }

//     else {
//         scale *= 1.5;
//     }

//     wholePDF(scale, urlStore);
// }
    
// function zoomMinus(){
//     if(scale < 0.2) {
//         return;
//     }

//     else if(scale < 0.5) {
//         scale /= 1.5;
//     }

//     else if(scale < 1) {
//         scale /= 1.35;
//     }

//     else {
//         scale /= 1.15;
//     }

//     wholePDF(scale, urlStore);
// }

function printPDF(){ 
    var print_doc = window.open(urlStore, "Print");
    print_doc.focus();
   //print_doc.print();
}

// function isScrolledDown() {
//     var topOfViewParent = $('#view-parent').offset().top;
//     var limitHeight = $('#page-header').height() + $('#view-header-small').height();

//     return topOfViewParent < limitHeight;
// }

// function isScrolledUp() {
//     var topOfViewParent = $('#view-parent').offset().top;
//     var limitHeight = $('#page-header').height() + $('#view-header-small').height();

//     return topOfViewParent >= limitHeight;
// }

// function switchHeaderToScrollDown() {
//     $('#view-header').addClass('view-header-scrolldown');
//     $('#view-header-small').removeClass('view-header-small-scrollup');
//     $('#view-parent').addClass('view-parent-scrolldown');
//     $('#view-parent').removeClass('view-parent-scrollup');
// }

// function switchHeaderToScrollUp() {
//     $('#view-header').removeClass('view-header-scrolldown');
//     $('#view-header-small').addClass('view-header-small-scrollup');
//     $('#view-parent').removeClass('view-parent-scrolldown');
//     $('#view-parent').addClass('view-parent-scrollup');
// }


