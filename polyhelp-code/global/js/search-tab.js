$(function () {
	
	// Only search again when search term is different
	var last_term = "";

	$('#search-term').on("keyup click", function () {
		var term = $('#search-term').val();
		window.sessionStorage.search_term = term;
		if (!term) {
			// No search query
			last_term = term;
			$('#search-results').hide();
			$('#search-no-query').show();
		} else if (term == last_term) {
			// Nothing changed
			$('#search-tab-link').tab('show');
		} else {
			last_term = term;
			$('#search-no-query').hide();
			do_search(term, function () {
				$('#search-results').scrollTop(0);
				$('#search-tab-link').tab('show');
				$('#search-results a:first').mousemove();
				$.ajax('/ajax/t/search/'+term);
			});
		}
	});

	function do_search(term, success_callback) {

		$.ajax('/ajax/search/' + encodeURIComponent(term), {
			dataType: 'json', 

			success: function (data, status) {
				var item, item_html;

				// Sort list again, client-side, using better similarity algorithm
				var fuse, results;
				$.each(data.results, function () {
					// Preprocess results
					
					// Don't show badge if year is already shown in file name
					if (this.name.indexOf(this.year) !== -1) this.year = null;
				});
				fuse = new Fuse(data.results, {keys: ['name', 'course', 'category'], caseSensitive: false, threshold: 1});
				results = fuse.search(term);
				
				// Display sorted search results
				$('#search-results').html('').show();
				$('#search-no-query').hide();
				for (var i in results) {
					item = results[i];
					item_html = '<li><a href="' + item.href + '">' + (item.name || '(Fichier sans nom)');
					item_html += item.year ? '<span class="count badge">' + item.year + '</span>' : '';
					item_html += '<br/><span class="description">' + item.course + ' | ' + item.category;
					item_html += item.subtype ? ' | ' + item.subtype : '';
					item_html += '</span></a></li>';
					$('#search-results').append(item_html);
				}

				if (success_callback) success_callback();
			}, 

			error: function (req, status, error) {
				console.log('Search error: ' + error);
				console.log(req.responseText);
			}
		})
	};

	// Make search results keyboard navigable (only when focus in search field)
	$('#search-results').keyboardNavigable('a', {
		metaKey: null, 
		disableInputs: false, 
		mouseNavigation: true, 
		onPreSelect: function (e, curr, next) {
			if (e.type == 'keydown') {
				// Cancel navigation if not in search box or not hovering search
				if (!$('#search-term:focus').length) return false;

				var $next = $(next);
				var $results = $('#search-results');
				
				// Scroll down
				if ($next.position().top + $next.outerHeight() > $results.outerHeight()) {
					$results.scrollTop($results.scrollTop() + $next.outerHeight());
				}

				// Scroll up
				if ($next.position().top < 0) {
					$results.scrollTop($results.scrollTop() - $next.outerHeight());
				}
			}
		},
		onAccess: function (e) {
			$.ajax('/ajax/t/search/access/' + $('#search-results .active').parent().index(), {async: false});
			if (e.type != 'click') {
				e.target.click();
			}
		}
	});
	
	// Restore previous search tab state (if available)
	if (window.sessionStorage.hasOwnProperty('search_term') && window.sessionStorage.search_term) {
		last_term = window.sessionStorage.search_term;
		$('#search-term').val(window.sessionStorage.search_term);
		do_search($('#search-term').val(), function () {
			$('#search-results').scrollTop(window.sessionStorage.search_scrollTop);
		});
	}

	// Remember vertical scroll position
	$('#search-results').scroll(function () {
		window.sessionStorage.search_scrollTop = $(this).scrollTop();
	});
});
