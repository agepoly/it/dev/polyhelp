$(document).ready(function () {
    $('a.right.remove').click(function (e) {
        var that = $(this);

        $.post($(this).attr('href'), { 'useajax': 1 },
            function (data) {
                var json = $.parseJSON(data);

                if (json.code == 0) {
                    that.closest('li').slideUp(function () {
                        $(this).remove();
                    });
                }
                else {
                    alert("Des erreurs sont arrivées:\n"
                        + json.errors.join('\n'));
                }
            }
        );

        e.preventDefault();
    });
});
