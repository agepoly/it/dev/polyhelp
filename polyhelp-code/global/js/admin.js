$(document).ready(function () {
    $('#files').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 3, 'desc' ] ],
        'bInfo': false,
        'aoColumns': [
            null,
            null,
            null,
            { 'sSortDataType': 'dom-data', 'sType': 'numeric' },
            null,
            null,
        ],
    }));

    $('#reports').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 4, 'desc' ] ],
        'bInfo': false,
        'aoColumns': [
            null,
            null,
            null,
            null,
            { 'sSortDataType': 'dom-data', 'sType': 'numeric' },
            null,
            null,
        ],
    }));

    $('#posts').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 3, 'desc' ] ],
        'bInfo': false,
        'aoColumns': [
            null,
            null,
            null,
            { 'sSortDataType': 'dom-data', 'sType': 'numeric' },
        ],
    }));

    $('#users').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 7, 'desc' ], [ 8, 'desc' ], [ 2, 'desc' ] ],
        'bInfo': false,
        'aoColumns': [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ],
    }));

    /* make a hidden panel, click on the h3 to toggle */
    $('.slider').each(function () {
        var that = $(this);
        that.find('.panel').hide();

        that.find('h3').click(function () {
            that.find('.panel').toggle();
        }).append(" &nbsp; &#x25BC;");
    });

    /* override report remove button click */
    $('#reports .button.remove').click(function (e) {
        var that = $(this);

        $.post($(this).attr('href'), { 'useajax': 1 },
            function (data) {
                var json = $.parseJSON(data);

                if (json.code == 0) {
                    that.closest('tr').fadeOut(function () {
                        $(this).remove();
                    });
                }
                else {
                    alert("Des erreurs se sont produites:\n"
                        + json.errors.join("\n"));
                }
            });
        e.preventDefault();
    });
});
