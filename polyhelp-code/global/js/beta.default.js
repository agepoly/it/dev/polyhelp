$(function(){

	$.extend({
		getUrlVars: function(){
		    var vars = [], hash;
		    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		    for(var i = 0; i < hashes.length; i++)
		    {
				hash = hashes[i].split('=');
				vars.push(hash[0]);
				vars[hash[0]] = hash[1];
		    }
	    return vars;
		},
		getUrlVar: function(name){
			return $.getUrlVars()[name];
		}
    });

	$('.beta.form').submit(function(event){
		event.preventDefault();
		var value = $(this).find('input[name=search_value]').val();
		
		var data = {'name': value};
		//$('.beta.list.pane')
		$.get('/beta/admin/list', data, function(response){
			
		});
	});
});