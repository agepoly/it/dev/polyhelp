// Redirects anything else to https://polyhelp.epfl.ch

(function () {
		var froms = ['epfhelp.ch', 'epfhelp.epfl.ch', 'epfhelp.agepoly.ch', 'polyhelp.ch', 'polyhelp.epfl.ch', 'polyhelp.agepoly.ch'];
		if (froms.indexOf(location.hostname) == -1) {
			// Unrecognized host, do nothing
			return;
		}

		var from;
		if (location.protocol !== 'https:') {
				from = 'http-' + location.hostname;
		} else {
				if (location.hostname !== 'polyhelp.epfl.ch') {
						from = 'https-' + location.hostname;
				}
		}
		if (from) {
				document.write('<pre>T&eacute;l&eacute;portation vers https://polyhelp.epfl.ch/...<br>');
				document.write('(Ajoute-le dans tes bookmarks!)</pre><div style="display: none">');
				from = location.search.length ? '&from=' + from : '?from=' + from;
				var newhref = 'https://polyhelp.epfl.ch' + location.pathname + location.search + from + location.hash;
				location.href = newhref;
				setInterval(function () {
						location.href = newhref;
				}, 7777); // Sorry, this is ugly.
				console.log('Redirecting to ' + newhref + ' ...');
		}
})();
