$(document).ready(function () {
    jQuery.fn.popBalloon = function (opts) {
        for (var i = 0; i < $(this).length; ++i) {
            var el = $(this[i]);

            var opts = $.extend({
                'position': null,
                'showDuration': 500,
                'hideDuration': 2000,
                'timeout': 2000,
                'hideAnimation': function (e) {
                    this.fadeOut(e);
                },
                'showAnimation': function (e) {
                    this.fadeIn(e);
                },
            }, opts);

            el.showBalloon(opts);
            setTimeout(function () {
                el.hideBalloon();
            }, opts['timeout']);
        }

        return $(this);
    }
});

function balloon_opts_fast(opts) {
    return $.extend({}, {
        'position': 'right',
        'hideDuration': 0,
        'hideAnimation': null,
        'showDuration': 0,
        'showAnimation': null,
        'minLifetime': 0,
    }, opts);
}
