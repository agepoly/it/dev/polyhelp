$(document).ready(function () {
        $(document).click(function(e) {
            var container = $('.upload-menu');
            if (!$(e.target).is(container)
                && !$(e.target).is($('.upload-button'))
                && !$(e.target).is($('#add-to-group-button'))
                && container.has(e.target).length === 0){
                    container.hide();
            }
        });
    
        $('#upload').on('click', function(){
            $('.upload-menu').toggle();
            $('#add-to-group').attr('value', '');
            if ($('.upload-menu').is(':visible')) {
                $.ajax('/ajax/t/upload/panel');
            }
	    return false;
        });

	$('#upload-select-files').on('click', function () {
		$('#upload-form input[type="file"]').click();
		return false;
	});
	$('#upload-form input[type="file"]').on('change', function (ev) {
		$('#upload-form').submit();
	});
        
	$('.week-tooltip').tooltip();

	$('#week-accordion').on('show', function () {
  		$('#chevron-week').toggleClass('icon-chevron-down icon-chevron-up');
	});

	$('#week-accordion').on('hide', function () {
  		$('#chevron-week').toggleClass('icon-chevron-up icon-chevron-down');
	});

});

function addfile(i) {
    $('#upload-group-'+i+' ul.group-files #add-button')
        .before('<li><input type="file" name="files-0[]"><a onclick="$(this).parent().remove()"><i class="glyphicons-icon circle_minus"></i></a></li>');
}

function weeks_selectall(i) {
    $('.weeks-0').each(function() { $(this).attr('checked', 'checked'); } );
}

function weeks_selectnone(i) {
    $('.weeks-0').each(function() { $(this).removeAttr('checked'); } );
}
