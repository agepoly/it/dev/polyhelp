/**
 * Usage:
 * menu.keyboardNavigable('> li', <options>);
 *
 * The plugins works with delegation, so you don't have to run it again
 * if the element's dom changes in the future.
 *
 * The keydown handler will be unbinded if the elements are removed from the page.
 */
(function ($) {
    $.fn.keyboardNavigable = function (selector, opts) {
        /**
         * Main useful keys
         */
        $.fn.keyboardNavigable.KEY = {
            TOP: 38,
            DOWN: 40,
            LEFT: 37,
            RIGHT: 39,
            ENTER: 13,
            ESCAPE: 27
        };
        var KEY = $.fn.keyboardNavigable.KEY;

        var standardKeys = [KEY.TOP, KEY.DOWN, KEY.ENTER];

        var defaults = {
            // the keys we listen to
            enabledKeys: standardKeys,

            // Set this to true if you don't want the plugin to select and activate the previous/next
            // item when pressing UP/DOWN arrows.
            // You should then implement the onPreSelect option to handle this yourself.
            preventStandard: false,

            // the class name for the active element
            activeClassName: 'active',

            // The meta key to hold. shift, ctrl, alt or null for none
            metaKey: 'ctrl',

            // don't need to press meta key for these ones
            noMeta: [],

            // disable the plugin when an input element has the focus on the page
            disableInputs: true,

            // whether to make items active on a mouseover too
            mouseNavigation: true,

            // called when an item will be made active. You can prevent this
            // by returning false
            onPreSelect: jQuery.noop,

            // called when an item is made active
            onPostSelect: jQuery.noop,

            // called when an item is clicked or enter-pressed
            onAccess: jQuery.noop,

            // a function which should return whether the plugin
            // is active (i.e. is tracking keyboard and mouse events)
            // When not overriden, the plugin is active.
            isEnabled: jQuery.noop
        };
        var options = $.extend(defaults, $.fn.keyboardNavigable.defaultOptions, opts);

        return this.each(function () {
            var el = $(this);
            var activeItem = null;
            var elements = $(selector, el);
            var lastMouseCoords = null;

            /**
             * we regularly refresh the elements list because the dom may have changed
             * TODO: improve this, bad for performance
             */
            function refreshElements() {
                elements = $(selector, el);
            }

            /**
             * Called when no item is highlighted. Find the first one
             * and make it active
             */
            function setFirstActiveItem (e) {
                if (!(getActiveItem() instanceof jQuery)) {
                    refreshElements();
                    makeItemActive(elements[0], e);
                }
                return activeItem;
            }

            /**
             * Make the given item active and de-active the current one.
             * The onPreSelect option can prevent the given item to be made active by
             * returning false, or can make another element active by returning it.
             */
            function makeItemActive (item, e) {
                var preSelectRes = options.onPreSelect.apply(activeItem, [e, activeItem, item, elements]);
                if (preSelectRes === false) {
                    return;
                } else if ($(preSelectRes).length > 0) {
                    item = $(preSelectRes);
                }

                if (activeItem instanceof jQuery) {
                    activeItem.removeClass(options.activeClassName);
                }
                activeItem = $(item);
                activeItem.addClass(options.activeClassName);

                options.onPostSelect.apply(activeItem, [e, activeItem]);
            }

            /**
             * Move to the previous sibling and make it active.
             */
            function moveToPrevious (e) {
                var prev, i;
                if (getActiveItem() === null) {
                    setFirstActiveItem(e);
                    return;
                }
                refreshElements();
                for (i = 1; i < elements.length; i++) {
                    if ($(elements[i]).hasClass(options.activeClassName)) {
                        makeItemActive(elements[i-1], e);
                        break;
                    }
                }
            }

            /**
             * Move to the next sibling and make it active.
             */
            function moveToNext (e) {
                var next, i;

                if (getActiveItem() === null) {
                    setFirstActiveItem(e);
                    return;
                }
                refreshElements();
                for (i = 0; i < elements.length-1; i++) {
                    if ($(elements[i]).hasClass(options.activeClassName)) {
                        makeItemActive(elements[i+1], e);
                        break;
                    }
                }
            }

            function getActiveItem() {
                if (activeItem === null) {
                    return null;
                } else if (activeItem.closest('html').length === 0) {
                    return null;
                }
                return activeItem;
            }

            function onEnterPressed(e) {
                if (activeItem) {
                    activeItem.trigger('enter-pressed-click');
                }
            }

            /**
             * When the mouse enters a link
             */
            function mouseMove (e) {console.log('mousemove');
                if (!isValidEvent(e)) {
                    return;
                }
                if (lastMouseCoords != null) {
                    if (lastMouseCoords.x == e.clientX && lastMouseCoords.y == e.clientY) {
                        // the mouse did not move at the document level, a scroll probably triggered the event
                        //return;
                        // Pat commented this line so that links can be activated with just .mousemove()
                    }
                }

                makeItemActive($(this), e);
            }

            /**
             * We track the last position of the mouse to be able to define if a
             * the mousemove event was triggered by a real mouse move or if
             * something else like scrolling triggered it.
             * see mouseMove function.
             */
            function saveLastMousePosition(e) {
                lastMouseCoords = {
                    x: e.clientX,
                    y: e.clientY
                }
            }

            /**
             * Called when the active item is clicked or enter-pressed
             */
            function clickHandler (e) {
                if (activeItem) {
                    options.onAccess.apply(activeItem, [e, activeItem]);
                }
            }

            function getDelegateString() {
                var delegateString = selector;
                // because of delegation, we have to remove any ">" if
                // present at the beginning of the selector string
                if (delegateString.substr(0,1) === '>') {
                    delegateString = delegateString.substr(1);
                }
                return delegateString;
            }

            function activateEvent(e) {
                makeItemActive($(this), e);
            }

            /**
             * Set a mousemove listener on the container element. Use delegation.
             */
            function registerMouseEvents () {
                var delegateString = getDelegateString();
                el.on('mousemove', delegateString, mouseMove);
                el.on('click', delegateString, clickHandler);
                el.on('activate', delegateString, activateEvent);
                $(document).on('mousemove', delegateString, saveLastMousePosition);
            }

            function unregisterMouseEvents() {
                el.off('mousemove', mouseMove);
                el.off('click', clickHandler);
                el.off('activate', activateEvent);
                $(document).off('mousemove', saveLastMousePosition);
            }

            /**
             * Check if the event is valid regarding which key was pressed, if
             * a meta key was needed, if it was the right one, ...
             * Return a boolean
             */
            function isValidEvent(e) {
                var needMeta = true;

                if (options.isEnabled !== jQuery.noop && options.isEnabled() !== true) {
                    return false;
                }

                if (e.type == 'keydown') {
                    // do we track this key?
                    if (jQuery.inArray(e.which, options.enabledKeys) === -1) {
                        return false;
                    }
                    // does this key needs metakey being pressed at the same time?
                    if (options.metaKey == null || jQuery.inArray(e.which, options.noMeta) > -1) {
                        needMeta = false;
                    }
                    // did we hold the right metakey?
                    if (needMeta && !eval('e.' + options.metaKey + 'Key')) {
                        return false;
                    }
                    // check that we are not in an input field
                    if (options.disableInputs && jQuery.inArray(document.activeElement.nodeName, ['INPUT', 'TEXTAREA', 'SELECT']) > -1) {
                        return false;
                    }
                }

                return true;
            }

            /**
             * Dispatch the event to the listeners if the pressed keys match
             * the ones from the config
             */
            function onKeydown (e) {
                // check that the dom is still in the page. If not, unbind the event handler.
                if (el.closest('html').length === 0) {
                    $(window).off('keydown', onKeydown);
                    return;
                }

                if (!isValidEvent(e)) {
                    return;
                }

                function compareArrays (a, b) {
                    if (a.length != b.length) { return false; }
                    var aa = a.sort(),
                        bb = b.sort();
                    for (var i = 0; aa[i]; i++) {
                        if (aa[i] !== bb[i]) {
                            return false;
                        }
                    }
                    return true;
                }

                if (!options.preventStandard && compareArrays(options.enabledKeys, standardKeys)) {
                    e.preventDefault();
                    switch(e.which) {
                        case KEY.TOP: moveToPrevious(e); break;
                        case KEY.DOWN: moveToNext(e); break;
                        case KEY.ENTER: onEnterPressed(e); break;
                    }
                } else {
                    options.onPreSelect.apply(activeItem, [e, activeItem, null, elements]);
                }
            }

            $(window).on('keydown', onKeydown);

            var delegateString = getDelegateString();
            el.on('enter-pressed-click', delegateString, clickHandler);

            if (options.mouseNavigation) {
                registerMouseEvents();
            }
        });
    };
})( jQuery );