$(document).ready(function () {
    $('#courses').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 4, 'desc' ], [ 0, 'asc' ] ],
        'aoColumns': [
            null,
            null,
            { 'sSortDataType': 'dom-data', 'sType': 'numeric' },
            { 'sSortDataType': 'dom-data', 'sType': 'numeric' },
            null,
        ],
        'oSearch': {
            'sSearch': (typeof(searchVal) !== 'undefined') ? searchVal : '',
        },
    }));

    $('.fav_icon').balloon(balloon_opts_fast({
        'contents': "Permet de mettre en favoris cette branche",
        'position': 'top-right',
    }));

    $('.fav_icon.notfavorited').click(fav_click);
    $('.fav_icon.favorited').click(notfav_click);
});

function fav_click() {
    add_fav_course($(this).attr('index'), $(this).attr('cname'));
    return false;
}

function notfav_click() {
    rem_fav_course($(this).attr('index'), $(this).attr('cname'));
    return false;
}

function set_fav_id(id, added) {
    var icon = (added) ? 'fav' : 'notfav';

    $('.fav_icon[index="'+id+'"]').unbind('click')
        .click( (added) ? notfav_click : fav_click )
        .find('img')
        .attr('src', '/global/img/'+icon+'.png');
}
