$(document).ready(function () {
    $('#posts').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 2, 'desc' ] ],
        'aoColumns': [
            null,
            null,
            { 'sSortDataType': 'dom-data', 'sType': 'numeric' },
            null,
        ],
    }));
});

