$(document).ready(function () {
    /* replace delete links by an ajax action */
    $('div.post_header a.delete').click(function (e) {
        var url = $(this).attr('href');
        var p = $(this);

        $.post(url, { 'useajax': 1 }, function (data) {
            var json = $.parseJSON(data);
            if (json.code != 0) {
                alert("Des erreurs sont arrivées:\n"+ json.errors.join('\n'));
            }
            else {
                p.closest('div.post').slideUp(function () {
                    p.remove();
                })
            }
        });

        e.preventDefault();
    });

    /* add report links to each post */
    $('div.post div.header.right').each(function () {
        var id = $(this).closest('div.post').attr('dbid');

        $(this).prepend(
        $('<span>[<a class="report">Signaler</a>]</span>').click(function () {
            report_show(id);
        }));
    });
});
