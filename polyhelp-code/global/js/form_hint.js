$(document).ready(function () {
    /* clean values if it is the hint */
    function clear_if_hint(that) {
        if (that.val() == that.attr('hint')) {
            that.val('');
        }
    }

    /* remove label because there is already an hint */
    $('.js_remove').remove();

    /* add a nice hint to our form inputs */
    $('form .hint').each(function () {
        var that = $(this);
        function display_hint () {
            if (that.val().length == 0) {
                that.val(that.attr('hint'));
                that.addClass('field_hint');
            }
        };
        $(this).blur(display_hint);
        $(this).focus(function () {
            clear_if_hint($(this));
            that.removeClass('field_hint');
        });
        display_hint();
    });

    /* prevent user posting values with hint */
    $('form input[type="submit"]').click(function () {
        $('form .hint').each(function () {
            clear_if_hint($(this));
        });
    });
});
