$(document).ready(function () {
    $('#files').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 4, 'desc' ] ],
        'aoColumns': [
            null,
            null,
            null,
            null,
            { 'sSortDataType': 'dom-data', 'sType': 'numeric' },
        ],
    }));

    $('#posts').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 3, 'asc' ] ],
        'aoColumns': [
            null,
            null,
            null,
            { 'sSortDataType': 'dom-data', 'sType': 'numeric' },
        ],
    }));
});
