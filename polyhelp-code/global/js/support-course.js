$(document).ready(function () {
    var balloon_opts = {
        'position': 'right',
        'hideDuration': 0,
        'hideAnimation': null,
        'showDuration': 0,
        'showAnimation': null,
        'minLifetime': 0,
    };

    $('#filegroups').dataTable($.extend({}, tables_custom, {
        'aaSorting': [ [ 6, 'desc' ], [ 0, 'asc' ] ],
        'aoColumns': [
            null,
            null,
            null,
            null,
            { 'sSortDataType': 'dom-data', 'sType': 'numeric' },
            null,
            null,
            null,
        ],
    }));

    $('.selfile').balloon(balloon_opts_fast({
        'contents': "Inclure dans le ZIP, voir en bas télécharger",
        'position': 'top-right',
    }));

    $('#zip-download').balloon(balloon_opts_fast({
        'contents': "Télécharger un zip des fichiers séléctionnés",
        'position': 'bottom-right',
    }));
});
