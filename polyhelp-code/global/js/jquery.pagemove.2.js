/*
 * jQuery pageMove
 * Patrick Yupeng Zhao
 *
 * Based on the jQuery pageSlide plugin
 * Copyright (c) 2011 Scott Robbin (srobbin.com)
 * Dual licensed under the MIT and GPL licenses.
*/

;(function($){
    // Convenience vars for accessing elements
    var $page = $('#page'),
        $pagemove = $('#pagemove');
    
    var _dontclose = false;    // Prevents panel closing when clicked inside
    
    // If the pagemove element doesn't exist, create it
    if( $pagemove.length == 0 ) {
         $pagemove = $('<div />').attr( 'id', 'pagemove' )
                                  .css( 'display', 'block' ) // To avoid modifying the CSS. It should be there though.
                                  .hide()
                                  .appendTo( $('body') );
    }
    if($('#pagemove-inactive').length == 0) {
        $('<div />').attr('id', 'pagemove-inactive')
                    .css('display', 'none')
                    .appendTo($('body'));
    }
    $('body').css('overflow-x', 'hidden');
    
    /*
     * Private methods 
     */
    
    function _load( url, useIframe ) {
        // Are we loading an element from the page or a URL?
        if ( url.indexOf("#") === 0 ) {   
            // Load a page element                
            $pagemove.children().appendTo($('#pagemove-inactive')).hide();
            $(url).appendTo( $pagemove ).show();
        } else {
            // Load a URL. Into an iframe?
            if( useIframe ) {
                var iframe = $("<iframe />").attr({
                                                src: url,
                                                frameborder: 0,
                                                hspace: 0
                                            })
                                            .css({
                                                width: "100%",
                                                height: "100%"
                                            });
                
                $pagemove.html( iframe );
            } else {
                $pagemove.load( url );
            }
            
            $pagemove.data( 'localEl', false );
            
        }
    }
    
    // Add two percent strings together, returns a percent (string)
    function _add(a, b) {
        return parseFloat(a) + parseFloat(b) + "%";
    }

    // Subtract two percent strings, returns a percent (string)
    function _minus(a, b) {
        return (parseFloat(a) - parseFloat(b)) + "%";
    }

    // Parse duration, then convert to CSS duration
    function _duration(d) {
        var i = $.inArray(d, ['instant', 'fast', 'normal', 'slow']);
        return (i == -1) ? d * 0.001 : i * 0.2;
    }

    // Function that controls opening of the pagemove
    function _start(edge, duration, width, behavior) {
        var pageAnimation = {},
            slideAnimation = {};
        var page = $page.get()[0],      // HTMLDivElements.
            pagemove = $pagemove.get()[0];
        var pageWidth = page.style.width || 100,
            slideWidth = width;
        var pageLeft = page.style.left || 0;

        duration = _duration(duration);
        console.log(['_start', edge, duration, width, behavior]);

        // Setup correct transitions
        $page.add($pagemove).css('transition', 'left ease-in-out ' + duration + 's, right ease-in-out ' + duration + 's, width ease-in-out ' + duration + 's');

        // Put pagemove into armed position
        switch (edge) {
            case 'left' :
                $pagemove.css('left', '-' + slideWidth);
                $pagemove.css('right', 'auto');
                break;
            case 'right' :
                $pagemove.css('right', '-' + slideWidth);
                $pagemove.css('left', 'auto');
                break;
        }
        $pagemove.css('width', slideWidth);
        $pagemove.css('transition-duration', '0s');
        $pagemove.show();

        // Wait until pagemove is out of "display: none" mode & ready to transition
        setTimeout(function () {
            $pagemove.css(edge, '0%');
            $pagemove.css('transition-duration', duration + 's');

            if (behavior == 'squish') $page.css('width', _minus(pageWidth, slideWidth));
            $page.css('overflow', '');
            switch (edge) {
                case 'left' :
                    $page.css('left', _add(pageLeft, slideWidth));
                    break;
                case 'right' :
                    if (behavior == 'squish') {
                        $page.css('left', '0%');
                    } else {
                        $page.css('left', _minus(pageLeft, slideWidth));
                    }
                    break;
            }

            // Restore transition (in case 'instant' was demanded)
            setTimeout(function () {
                duration = _duration($pagemove.data('duration'));
                $page.add($pagemove).css('transition', 'left ease-in-out ' + duration + 's, right ease-in-out ' + duration + 's, width ease-in-out ' + duration + 's');
                $page.addClass('pagemoved');
            }, 0);
        }, 0);

    }
    
    // Slides an already open slide
    // (uses only fromWidth's unit; cannot mix % and px)
    // (positive direction expands the slide & shrinks the page)
    function _slide(edge, duration, fromWidth, toWidth, behavior) {
        console.log(['_slide', edge, duration, fromWidth, toWidth, behavior]);
        var delta = _minus(toWidth, fromWidth);
        var page = $page.get()[0],
            pagemove = $pagemove.get()[0];
        var pageLeft = page.style.left || alert('Noes, missing page.style.left'),
            pageWidth = page.style.width || 100,
            slideWidth = pagemove.style.width || alert('Noes, missing pagemove.style.width');

        $pagemove.css('width', _add(slideWidth, delta));
        switch (edge) {
            case 'left' :
                $page.css('left', _add(pageLeft, delta));
                break;
            case 'right' :
                $page.css('left', _minus(pageLeft, delta));
                break;
        }
        if (behavior == 'squish') $page.css('width', _minus(pageWidth, delta));

        return;

        var unit = fromWidth.replace(/[\d\-]/g, '');
        var pageAnimation = {}, 
            slideAnimation = {};
        if (behavior == 'squish') {
            pageAnimation.width = ((delta > 0) ? '-=' : '+=') + Math.abs(delta) + unit;
        }
        slideAnimation.width = ((delta > 0) ? '+=' : '-=') + Math.abs(delta) + unit;
        switch (edge) {
            case 'left' :
                pageAnimation.left = ((delta > 0) ? '+=' : '-=') + Math.abs(delta) + unit;
                break;
            default :
                if (behavior != 'squish') pageAnimation.left = ((delta > 0) ? '-=' : '+=') + Math.abs(delta) + unit;
                break;
        }

        $page.animate(pageAnimation, duration).css('overflow', '');
        $pagemove.animate(slideAnimation, duration);
    }
      
    /*
     * Public methods 
     */
    
    // Open the pagemove
    $.pagemove = function( options ) {
        // Extend the settings with those the user has provided
        var settings = $.extend({}, $.pagemove.defaults, options);

        // Allow for instantaneous pagemove
        var duration = settings.instant ? 'instant' : settings.duration;
        delete settings.instant;
        
        // Are we trying to open in different edge?
        if( $pagemove.is(':visible') && $pagemove.data( 'edge' ) != settings.edge) {
            $.pagemove.close(function(){
                _load( settings.href, settings.iframe );
                _start( settings.edge, duration, settings.width, settings.behavior );
            });
        } else if ($pagemove.is(':hidden')) {                
            // Same edge, first panel opening
            _load( settings.href, settings.iframe );
            _start( settings.edge, duration, settings.width, settings.behavior );
        } else {
            // Same edge, panel already open
            _load( settings.href, settings.iframe );
            _slide(settings.edge, duration, $pagemove.data('width'), settings.width, settings.behavior);
        }
        
        $pagemove.data( settings );
    }
    
    // Close the pagemove
    $.pagemove.close = function ( callback ) {
        var $pagemove = $('#pagemove'),
            slideWidth = $pagemove.data('width'),
            duration = _duration($pagemove.data('duration')),
            edge = $pagemove.data('edge'),
            behavior = $pagemove.data('behavior');
        var page = $page.get()[0],
            pagemove = $pagemove.get()[0];
        console.log(['close', edge, duration, slideWidth, behavior]);
        
        // Setup transition speed
        $page.add($pagemove).css('transition', 'left ease-in-out ' + duration + 's, right ease-in-out ' + duration + 's, width ease-in-out ' + duration + 's');

        $pagemove.css(edge, '-' + slideWidth);
        $page.css('left', '0%');
        $page.css('width', '100%');
        $page.removeClass('pagemoved');

        // Wrap-up when done
        var endFunc = function () {
            $page.off('webkitTransitionEnd', endFunc);
            $pagemove.hide();
            if (typeof callback != 'undefined') callback();
        };
        setTimeout(endFunc, duration * 1000);
    }
    
    /*
     * Default settings 
     */
    $.pagemove.defaults = {
        duration: 'normal', // Accepts standard jQuery effects durations (i.e. fast, normal or milliseconds)
        edge:     'left',   // Accepts 'left' or 'right'
        modal:    true,     // If set to true, you must explicitly close pagemove using $.pagemove.close();
        iframe:   true,     // By default, linked pages are loaded into an iframe. Set this to false if you don't want an iframe.
        href:     null,     // Override the source of the content. Optional in most cases, but required when opening pagemove programmatically.
        width:    '260px',  // Accepts any CSS width value and unit
        behavior: 'push'    // Accepts 'push' or 'squish'
    };
    
    /* Events */
    
    // Don't let clicks to the pagemove close the window
    $pagemove.click(function(e) {
        _dontclose = true;
    });

    // Close the pagemove if the document is clicked or the users presses the ESC key, unless the pagemove is modal
    $(document).bind('click keyup', function(e) {
        // If this is a keyup event, let's see if it's an ESC key
        if( e.type == "keyup" && e.keyCode != 27) return;
        
        // Make sure it's visible, and we're not modal      
        if( $pagemove.is( ':visible' ) && !$pagemove.data( 'modal' )) {
            if (_dontclose) {
                _dontclose = false;
            } else {
                $.pagemove.close();
           }
        }
    });
    
})(jQuery);