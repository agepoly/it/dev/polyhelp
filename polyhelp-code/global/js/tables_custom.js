/**
 * Default tables attribute, can be overriden.
 */
var tables_custom = {
    'oLanguage' : { 'sUrl': '/global/txt/datatables_fr.json' },
    'sDom': '<"dataTables_top"f>t<"dataTables_bottom"iprl>',
    'aLengthMenu': [ 10, 25, 50, 100, 1000 ],
    'iDisplayLength': 25,
    'bSort': true,
    'fnInitComplete': function () {
        var wrapper = $(this).closest('.dataTables_wrapper')

        wrapper.find('.dataTables_filter')
               .append('<span class="icon clear_filter">&#x2297;</span>');

        wrapper.find('.clear_filter').click(function () {
            $(this).closest('.dataTables_filter')
                   .find('input')
                   .attr('value',  '')
                   .focus()
                   .keyup();
        });

        if ($().balloon) {
            wrapper.find('.dataTables_filter label')
                .balloon(balloon_opts_fast({
                    'contents': "Permet de filtrer par n'importe quelle "
                    +"colonne du tableau ci-dessous",
                    'position': 'top',
                }));

            wrapper.find('.dataTables_filter span').balloon(balloon_opts_fast({
                'contents': "Vider le champs ci-contre",
                'position': 'right',
            }));
        }

        $(this).find('th').balloon(balloon_opts_fast({
            'contents': "Trier par cette colonne",
            'position': 'top',
        }));
    },
}

/*
 * A function to get the data attribute in td to sort the table.
 */
$(document).ready(function () {
    $.fn.dataTableExt.afnSortData['dom-data'] = function (set, col) {
        var aData = [ ];
        $('td:eq('+col+')', set.oApi._fnGetTrNodes(set)).each(function () {
            aData.push($(this).attr('data'));
        });
        return aData;
    };
});

function select_all(selector) {
    $(selector).each(function() {
        $(this).attr('checked', 'checked');
    } );
}

function select_none(selector) {
    $(selector).each(function() {
        $(this).removeAttr('checked');
    } );
}
