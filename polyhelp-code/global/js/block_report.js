$(document).ready(function () {
    /* hide report problems because we have icons for that, and when we are at
     * it, add a close button in the overlay for later. */
    $('#report')
        .hide()
        .prepend($('<b id="report_close">Fermer</b><br/>').click(function () {
            $('#report').animate({
                    'opacity': 0.0,
                    'top': '-=170',
            }, 220, function () {
                $(this).hide();
            });
        }).css({ 'float': 'right' }))
        
    /* change the form submit trigger, to use ajax */
    $('#report form').submit(function () {
        $.ajax({
            'url': $(this).attr('action'),
            'type': 'POST',
            'data': $(this).serialize() + '&useajax=1',
            'success': function (data) {
                var json = $.parseJSON(data)
                var split = json.errors;
                var code = json.code;

                if (!isNaN(code) && code == 0) {
                    $('#report form').find('.submit')
                        .attr('value', "Envoyé!")
                        .attr('disabled', 'disabled');

                    setTimeout(function () {
                        $('#report_close').click();
                    }, 1500);
                }
                else {
                    alert("Certaines erreurs se sont produites:\n"
                        + split.join("\n"));
                }
            },
        });
        return false;
    });
    
    $(document).click(function(e){
       var container =  $('#report');
       if(!$(e.target).is(container)
          && $('#report-button-bigheader').has(e.target).length === 0
          && $('#report-button-smallheader').has(e.target).length === 0
          && container.has(e.target).length === 0){
            container.hide();
       }
    });

    /* used to center our window divs */
    jQuery.fn.center = function () {
        this.css("position","absolute");
        this.css("top", Math.max(0, (($(window).height()
                - this.outerHeight()) / 2)
                + $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, (($(window).width()
                - this.outerWidth()) / 2)
                + $(window).scrollLeft()) + "px");

        return this;
    }

});

/* function to display the report window */
function report_show(relid) {
    console.log('report_show '+ relid);
    $('#report')
        .addClass('floating_window')
        .center()
        .css('opacity', 0.95)
        .show()
        .find('input[name="relid"]')
        .attr('value', relid);

    $('#report .submit').attr('value', "Reporter")
        .removeAttr('disabled');
}

